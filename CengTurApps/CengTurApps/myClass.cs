using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using Microsoft.Win32;

namespace CengTurApp
{
    class myClass
    {
        public static SqlConnection SQLServer()
        {
            SqlConnection Baglanti = new SqlConnection();
            string IPAdres, User, Passwd;
            IPAdres = Registry.CurrentUser.OpenSubKey("mCayirLi.net").OpenSubKey("CengTur").GetValue("ServerIp").ToString();
            User = Registry.CurrentUser.OpenSubKey("mCayirLi.net").OpenSubKey("CengTur").GetValue("User").ToString();
            Passwd = Registry.CurrentUser.OpenSubKey("mCayirLi.net").OpenSubKey("CengTur").GetValue("Passwd").ToString();
            Baglanti.ConnectionString = "Data Source=" + IPAdres + "; Initial Catalog=CengTur; User ID=" + User + "; Password=" + Passwd + ";";
            return Baglanti;
        }

        public static void ExecQuery(string SQL)
        {
            SqlConnection Baglanti = SQLServer();
            try
            {
                Baglanti.Open();
                SqlCommand Komut = new SqlCommand(SQL, Baglanti);
                Komut.ExecuteNonQuery();
            }
            finally
            {
                Baglanti.Close();
            }
        }
        public static int ExecScalarQuery(string SQL)
        {
            SqlConnection Baglanti = SQLServer();
            try
            {
                Baglanti.Open();
                SqlCommand Komut = new SqlCommand(SQL, Baglanti);
                return (Int32)Komut.ExecuteScalar();
            }
            finally
            {
                Baglanti.Close();
            }
        }

        public static Guid ExecScalarQueryGuid(string SQL)
        {
            SqlConnection Baglanti = SQLServer();
            try
            {
                Baglanti.Open();
                SqlCommand Komut = new SqlCommand(SQL, Baglanti);
                SqlDataReader dr = Komut.ExecuteReader();
                dr.Read();
                return dr.GetGuid(0);
            }
            finally
            {
                Baglanti.Close();
            }
        }
        public static int RecCount(string SQL)
        {
            SqlConnection Baglanti = SQLServer();
            SqlCommand cmdRecCount = new SqlCommand(SQL, Baglanti);
            cmdRecCount.CommandType = CommandType.Text;
            Baglanti.Open();
            SqlDataReader DR = cmdRecCount.ExecuteReader(CommandBehavior.CloseConnection);
            if (DR.Read())
                return DR.GetInt32(0);
            else
                return 0;
        }
    }
}