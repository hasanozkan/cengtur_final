using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.Sql;
using System.Data.SqlClient;
using Microsoft.Win32;
using System.Security.Cryptography;

namespace CengTurApp
{
    public partial class ChangeServer : Form
    {
        public ChangeServer()
        {
            InitializeComponent();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            errorProvider1.Dispose();
            if (mtxtIPAdres.Text.Replace(" ", "") == "...")
                errorProvider1.SetError(mtxtIPAdres, "Bo� olamaz!");
            else if (txtUser.Text == "")
                errorProvider1.SetError(txtUser, "Bo� olamaz!");
            else if (txtPasswd.Text == "")
                errorProvider1.SetError(txtPasswd, "Bo� olamaz!");
            else if (txtPasswd.Text != txtPasswdConf.Text)
                MessageBox.Show("Girilen �ifre bilgileri e�le�miyor!", "Hata", MessageBoxButtons.OK, MessageBoxIcon.Error);
            else
            {
                SqlConnection Baglanti = new SqlConnection();
                Baglanti.ConnectionString = "Data Source=" + mtxtIPAdres.Text.Replace(" ", "") + "; Initial Catalog=CengTur; User ID=" + txtUser.Text + "; Password=" + txtPasswd.Text + ";";
                try
                {
                    Cursor.Current = Cursors.WaitCursor;
                    Baglanti.Open();
                    SaveLogin2Reg(mtxtIPAdres.Text.Replace(" ", ""), txtUser.Text, txtPasswd.Text);
                    this.Hide();
                    Form1 fr1 = new Form1();
                    fr1.Show();
                }
                catch (Exception Hata)
                {
                    MessageBox.Show(Hata.Message, "Hata", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Cursor.Current = Cursor.Current;
                }
                finally
                {
                    Baglanti.Close();
                }
            }
        }
        private void SaveLogin2Reg(string IPAdres, string User, string Passwd)
        {
            Registry.CurrentUser.CreateSubKey("mCayirLi.net").CreateSubKey("CengTur").SetValue("ServerIp", IPAdres);
            Registry.CurrentUser.CreateSubKey("mCayirLi.net").CreateSubKey("CengTur").SetValue("User", User);
            Registry.CurrentUser.CreateSubKey("mCayirLi.net").CreateSubKey("CengTur").SetValue("Passwd", Passwd);
        }
        private void btnIptal_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}