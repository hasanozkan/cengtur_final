using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.Sql;
using System.Data.SqlClient;

namespace CengTurApp
{
    public partial class Bolge : Form
    {
        public Bolge()
        {
            InitializeComponent();
        }
        
        string[] arrayTKodu;
        short[] arraySKodu;

        private void FillCmbBoxTurlar()
        {
            SqlConnection Baglanti = myClass.SQLServer();
            cmbTurlar.Items.Clear();
            try
            {
                arrayTKodu = new string[myClass.RecCount("SELECT Count(*) FROM Turlar WHERE TurKodu <> '0'")];
                SqlCommand cmdTurlar = new SqlCommand("SELECT TurKodu, TurAdi FROM Turlar WHERE TurKodu <> '0'", Baglanti);
                cmdTurlar.CommandType = CommandType.Text;
                Baglanti.Open();
                SqlDataReader DR = cmdTurlar.ExecuteReader(CommandBehavior.CloseConnection);
                int i = new int();
                while (DR.Read())
                {
                    arrayTKodu[i] = DR.GetString(0);
                    cmbTurlar.Items.Add(DR.GetString(1));
                    i++;
                }
            }
            catch (Exception Hata)
            {
                MessageBox.Show(Hata.Message, "Hata", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Baglanti.Close();
            }
        }

        private void FillCmbBoxSehir()
        {
            SqlConnection Baglanti = myClass.SQLServer();
            cmbSehir.Items.Clear();
            try
            {
                arraySKodu = new short[myClass.RecCount("SELECT Count(*) FROM Sehir WHERE TurKodu = '" + arrayTKodu[cmbTurlar.SelectedIndex] + "'")];
                SqlCommand cmdSehir = new SqlCommand("SELECT SKodu, SAdi FROM Sehir WHERE TurKodu = '" + arrayTKodu[cmbTurlar.SelectedIndex] + "' ORDER BY SAdi", Baglanti);
                cmdSehir.CommandType = CommandType.Text;
                Baglanti.Open();
                SqlDataReader DR = cmdSehir.ExecuteReader(CommandBehavior.CloseConnection);
                int i = new int();
                while (DR.Read())
                {
                    arraySKodu[i] = DR.GetInt16(0);
                    cmbSehir.Items.Add(DR.GetString(1));
                    i++;
                }
            }
            catch (Exception Hata)
            {
                MessageBox.Show(Hata.Message, "Hata", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Baglanti.Close();
            }
        }

        private void FillLstBox()
        {
            SqlConnection Baglanti = myClass.SQLServer();
            lstBoxBolge.Items.Clear();
            try
            {
                short SKodu = arraySKodu[cmbSehir.SelectedIndex];
                SqlCommand cmdBolge = new SqlCommand("SELECT BAdi FROM Bolge WHERE SKodu = " + SKodu.ToString() + " ORDER BY BAdi", Baglanti);
                cmdBolge.CommandType = CommandType.Text;
                Baglanti.Open();
                SqlDataReader DR = cmdBolge.ExecuteReader(CommandBehavior.CloseConnection);
                while (DR.Read())
                    lstBoxBolge.Items.Add(DR.GetString(0));
            }
            catch (Exception Hata)
            {
                MessageBox.Show(Hata.Message, "Hata", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Baglanti.Close();
            }
        }

        private void Bolge_Load(object sender, EventArgs e)
        {
            FillCmbBoxTurlar();
        }

        private void cmbTurlar_SelectedIndexChanged(object sender, EventArgs e)
        {
            errorProvider1.Dispose();
            FillCmbBoxSehir();
            lstBoxBolge.Items.Clear();
            txtYeniBolge.Clear();
            txtYeniBolge.BackColor = Color.White;
        }

        private void cmbSehir_SelectedIndexChanged(object sender, EventArgs e)
        {
            errorProvider1.Dispose();
            FillLstBox();
            txtYeniBolge.Clear();
            txtYeniBolge.BackColor = Color.White;
            txtYeniBolge.Focus();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            errorProvider1.Dispose();
            if (cmbTurlar.SelectedIndex == -1)
                errorProvider1.SetError(cmbTurlar, "�ncelikle Tur Tan�m�n� se�melisiniz!");
            else if (cmbSehir.SelectedIndex == -1)
                errorProvider1.SetError(cmbSehir, "�st B�lgeyi se�melisiniz!");
            else if (txtYeniBolge.Text == string.Empty)
                errorProvider1.SetError(txtYeniBolge, "Bo� olamaz!");
            else
            {
                try
                {
                    string TurKodu = arrayTKodu[cmbTurlar.SelectedIndex];
                    string TurAdi = cmbTurlar.SelectedItem.ToString();
                    short SKodu = arraySKodu[cmbSehir.SelectedIndex];
                    string SAdi = cmbSehir.SelectedItem.ToString();
                    txtYeniBolge.Text = txtYeniBolge.Text.Trim();

                    bool Kontrol = new bool();
                    string[] FromCheckBolge = CheckBolge(txtYeniBolge.Lines, SKodu, ref Kontrol);

                    if (!Kontrol)
                    {
                        txtYeniBolge.BackColor = Color.White;
                        DialogResult cvp = MessageBox.Show("Toplam " + txtYeniBolge.Lines.Length + " alt b�lge, " + TurAdi + " tan�ml� turun " + SAdi + " �st b�lgesine eklenecektir.\n\nDevam etmek istedi�inize emin misiniz?", "Uyar�", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2);
                        if (cvp == DialogResult.Yes)
                        {
                            for (int i = 0; i < txtYeniBolge.Lines.Length; i++)
                            {
                                myClass.ExecQuery("INSERT INTO Bolge (BAdi, SKodu) VALUES ('" + txtYeniBolge.Lines.GetValue(i).ToString().ToUpper() + "', " + SKodu.ToString() + ")");
                                myClass.ExecQuery("INSERT INTO Konum (TurKodu, SKodu, BKodu) VALUES ('" + TurKodu + "', " + SKodu.ToString() + ", " + GetBKodu(txtYeniBolge.Lines.GetValue(i).ToString().ToUpper(), SKodu).ToString() + ")");
                            }
                            FillLstBox();
                            txtYeniBolge.Clear();
                        }
                    }
                    else
                    {
                        MessageBox.Show("Girdi�iniz bilgilerde daha �nceden " + TurAdi + " tan�ml� turun "+ SAdi +" �st b�lgesine kaydedilmi� alt b�lge bulundu!\n\n��aretlenmi� bu bilgileri ��kartarak l�tfen tekrar deneyiniz.", "Hata", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        txtYeniBolge.Clear();
                        txtYeniBolge.BackColor = ColorTranslator.FromHtml("#FFDCDC");
                        for (int i = 0; i < FromCheckBolge.Length; i++)
                            txtYeniBolge.Text += FromCheckBolge[i] + Environment.NewLine;
                    }
                }
                catch (Exception Hata)
                {
                    MessageBox.Show(Hata.Message, "Hata", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private string[] CheckBolge(string[] GelenBolgeler, short SKodu, ref bool Result)
        {
            string[] BulunanBolgeler = GelenBolgeler;
            for (int i = 0; i < GelenBolgeler.Length; i++)
            {
                SqlConnection Baglanti = myClass.SQLServer();
                SqlCommand cmdBolge = new SqlCommand("SELECT * FROM Bolge WHERE SKodu = " + SKodu.ToString() + " AND BAdi = '" + GelenBolgeler[i].ToUpper() + "'", Baglanti);
                cmdBolge.CommandType = CommandType.Text;
                Baglanti.Open();
                SqlDataReader DR = cmdBolge.ExecuteReader(CommandBehavior.CloseConnection);
                if (DR.Read())
                {
                    Result = true;
                    BulunanBolgeler[i] = "--> " + GelenBolgeler[i];
                }
            }
            return BulunanBolgeler;
        }

        private int GetBKodu(string BAdi, short SKodu)
        {
            SqlConnection Baglanti = myClass.SQLServer();
            SqlCommand cmdBolge = new SqlCommand("SELECT BKodu FROM Bolge WHERE BAdi='" + BAdi + "' AND SKodu = " + SKodu.ToString(), Baglanti);
            cmdBolge.CommandType = CommandType.Text;
            Baglanti.Open();
            SqlDataReader DR = cmdBolge.ExecuteReader(CommandBehavior.CloseConnection);
            DR.Read();
            int BKodu = DR.GetInt32(0);
            Baglanti.Close();
            return BKodu;
        }

        private void btnIptal_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}