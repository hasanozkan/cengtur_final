namespace CengTurApp
{
    partial class OtelBilgileri
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OtelBilgileri));
            this.tabControlOtel = new System.Windows.Forms.TabControl();
            this.tabPageOtel = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.tabControlDetay = new System.Windows.Forms.TabControl();
            this.tabPageOtelEkle = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.lblHata = new System.Windows.Forms.Label();
            this.btnOtelEkle = new System.Windows.Forms.Button();
            this.txtOtelAdi = new System.Windows.Forms.TextBox();
            this.tabPageOtelDuzenle = new System.Windows.Forms.TabPage();
            this.lblHit = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.btnDuzenIptal = new System.Windows.Forms.Button();
            this.btnFotos = new System.Windows.Forms.Button();
            this.btnThumb = new System.Windows.Forms.Button();
            this.btnKonumBilgisi = new System.Windows.Forms.Button();
            this.pBoxThumb = new System.Windows.Forms.PictureBox();
            this.btnTesisBilgileri = new System.Windows.Forms.Button();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lstBoxOtel = new System.Windows.Forms.ListBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbBolge = new System.Windows.Forms.ComboBox();
            this.cmbSehir = new System.Windows.Forms.ComboBox();
            this.cmbTurlar = new System.Windows.Forms.ComboBox();
            this.tabPageTesis = new System.Windows.Forms.TabPage();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.txtTesisBilgileri = new System.Windows.Forms.TextBox();
            this.lblTesisBilgileri_Info = new System.Windows.Forms.Label();
            this.btnTesisBilgisiEkle = new System.Windows.Forms.Button();
            this.tabPageKonum = new System.Windows.Forms.TabPage();
            this.tabPageThumb = new System.Windows.Forms.TabPage();
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            this.tabPageFotos = new System.Windows.Forms.TabPage();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.pnlFormTitle = new System.Windows.Forms.Panel();
            this.lblFormTitle = new System.Windows.Forms.Label();
            this.btnIptal = new System.Windows.Forms.Button();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.webBrowser2 = new System.Windows.Forms.WebBrowser();
            this.tabControlOtel.SuspendLayout();
            this.tabPageOtel.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.tabControlDetay.SuspendLayout();
            this.tabPageOtelEkle.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.tabPageOtelDuzenle.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pBoxThumb)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPageTesis.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.tabPageThumb.SuspendLayout();
            this.tabPageFotos.SuspendLayout();
            this.pnlFormTitle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControlOtel
            // 
            this.tabControlOtel.Controls.Add(this.tabPageOtel);
            this.tabControlOtel.Controls.Add(this.tabPageTesis);
            this.tabControlOtel.Controls.Add(this.tabPageKonum);
            this.tabControlOtel.Controls.Add(this.tabPageThumb);
            this.tabControlOtel.Controls.Add(this.tabPageFotos);
            this.tabControlOtel.ImageList = this.imageList1;
            this.tabControlOtel.Location = new System.Drawing.Point(12, 54);
            this.tabControlOtel.Name = "tabControlOtel";
            this.tabControlOtel.SelectedIndex = 0;
            this.tabControlOtel.Size = new System.Drawing.Size(490, 335);
            this.tabControlOtel.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tabControlOtel.TabIndex = 0;
            this.tabControlOtel.Selecting += new System.Windows.Forms.TabControlCancelEventHandler(this.tabControlOtel_Selecting);
            this.tabControlOtel.SelectedIndexChanged += new System.EventHandler(this.tabControlOtel_SelectedIndexChanged);
            // 
            // tabPageOtel
            // 
            this.tabPageOtel.Controls.Add(this.groupBox3);
            this.tabPageOtel.Controls.Add(this.groupBox2);
            this.tabPageOtel.Controls.Add(this.groupBox1);
            this.tabPageOtel.ImageIndex = 0;
            this.tabPageOtel.Location = new System.Drawing.Point(4, 23);
            this.tabPageOtel.Name = "tabPageOtel";
            this.tabPageOtel.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageOtel.Size = new System.Drawing.Size(482, 308);
            this.tabPageOtel.TabIndex = 0;
            this.tabPageOtel.Text = "Oteller";
            this.tabPageOtel.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.tabControlDetay);
            this.groupBox3.Location = new System.Drawing.Point(6, 122);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(471, 179);
            this.groupBox3.TabIndex = 18;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Otel Bilgisi";
            // 
            // tabControlDetay
            // 
            this.tabControlDetay.Controls.Add(this.tabPageOtelEkle);
            this.tabControlDetay.Controls.Add(this.tabPageOtelDuzenle);
            this.tabControlDetay.ImageList = this.imageList1;
            this.tabControlDetay.Location = new System.Drawing.Point(9, 19);
            this.tabControlDetay.Multiline = true;
            this.tabControlDetay.Name = "tabControlDetay";
            this.tabControlDetay.SelectedIndex = 0;
            this.tabControlDetay.Size = new System.Drawing.Size(453, 154);
            this.tabControlDetay.TabIndex = 19;
            this.tabControlDetay.Selecting += new System.Windows.Forms.TabControlCancelEventHandler(this.tabControlDetay_Selecting);
            this.tabControlDetay.SelectedIndexChanged += new System.EventHandler(this.tabControlDetay_SelectedIndexChanged);
            // 
            // tabPageOtelEkle
            // 
            this.tabPageOtelEkle.BackColor = System.Drawing.Color.Transparent;
            this.tabPageOtelEkle.Controls.Add(this.groupBox5);
            this.tabPageOtelEkle.ImageIndex = 5;
            this.tabPageOtelEkle.Location = new System.Drawing.Point(4, 23);
            this.tabPageOtelEkle.Name = "tabPageOtelEkle";
            this.tabPageOtelEkle.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageOtelEkle.Size = new System.Drawing.Size(445, 127);
            this.tabPageOtelEkle.TabIndex = 0;
            this.tabPageOtelEkle.Text = "Ekle";
            this.tabPageOtelEkle.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.lblHata);
            this.groupBox5.Controls.Add(this.btnOtelEkle);
            this.groupBox5.Controls.Add(this.txtOtelAdi);
            this.groupBox5.Location = new System.Drawing.Point(6, 6);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(433, 112);
            this.groupBox5.TabIndex = 0;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Otel Ad�n� Giriniz";
            // 
            // lblHata
            // 
            this.lblHata.AutoSize = true;
            this.lblHata.ForeColor = System.Drawing.Color.Red;
            this.lblHata.Location = new System.Drawing.Point(22, 42);
            this.lblHata.Name = "lblHata";
            this.lblHata.Size = new System.Drawing.Size(40, 13);
            this.lblHata.TabIndex = 2;
            this.lblHata.Text = "lblHata";
            // 
            // btnOtelEkle
            // 
            this.btnOtelEkle.Image = global::CengTurApps.Properties.Resources.PencilGo;
            this.btnOtelEkle.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnOtelEkle.Location = new System.Drawing.Point(352, 17);
            this.btnOtelEkle.Name = "btnOtelEkle";
            this.btnOtelEkle.Size = new System.Drawing.Size(75, 23);
            this.btnOtelEkle.TabIndex = 1;
            this.btnOtelEkle.Text = "Devam >";
            this.btnOtelEkle.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnOtelEkle.UseVisualStyleBackColor = true;
            this.btnOtelEkle.Click += new System.EventHandler(this.btnOtelEkle_Click);
            // 
            // txtOtelAdi
            // 
            this.txtOtelAdi.Location = new System.Drawing.Point(6, 19);
            this.txtOtelAdi.Name = "txtOtelAdi";
            this.txtOtelAdi.Size = new System.Drawing.Size(340, 20);
            this.txtOtelAdi.TabIndex = 0;
            // 
            // tabPageOtelDuzenle
            // 
            this.tabPageOtelDuzenle.BackColor = System.Drawing.Color.Transparent;
            this.tabPageOtelDuzenle.Controls.Add(this.lblHit);
            this.tabPageOtelDuzenle.Controls.Add(this.groupBox4);
            this.tabPageOtelDuzenle.ImageIndex = 6;
            this.tabPageOtelDuzenle.Location = new System.Drawing.Point(4, 23);
            this.tabPageOtelDuzenle.Name = "tabPageOtelDuzenle";
            this.tabPageOtelDuzenle.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageOtelDuzenle.Size = new System.Drawing.Size(445, 127);
            this.tabPageOtelDuzenle.TabIndex = 1;
            this.tabPageOtelDuzenle.Text = "D�zenle";
            this.tabPageOtelDuzenle.UseVisualStyleBackColor = true;
            // 
            // lblHit
            // 
            this.lblHit.AutoSize = true;
            this.lblHit.Location = new System.Drawing.Point(6, 104);
            this.lblHit.Name = "lblHit";
            this.lblHit.Size = new System.Drawing.Size(30, 13);
            this.lblHit.TabIndex = 23;
            this.lblHit.Text = "lblHit";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.btnDuzenIptal);
            this.groupBox4.Controls.Add(this.btnFotos);
            this.groupBox4.Controls.Add(this.btnThumb);
            this.groupBox4.Controls.Add(this.btnKonumBilgisi);
            this.groupBox4.Controls.Add(this.pBoxThumb);
            this.groupBox4.Controls.Add(this.btnTesisBilgileri);
            this.groupBox4.Location = new System.Drawing.Point(6, 6);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(435, 95);
            this.groupBox4.TabIndex = 22;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Denetim Paneli";
            // 
            // btnDuzenIptal
            // 
            this.btnDuzenIptal.Image = global::CengTurApps.Properties.Resources.DeleteRec;
            this.btnDuzenIptal.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDuzenIptal.Location = new System.Drawing.Point(354, 66);
            this.btnDuzenIptal.Name = "btnDuzenIptal";
            this.btnDuzenIptal.Size = new System.Drawing.Size(75, 23);
            this.btnDuzenIptal.TabIndex = 24;
            this.btnDuzenIptal.Text = "&Vazge�";
            this.btnDuzenIptal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDuzenIptal.UseVisualStyleBackColor = true;
            this.btnDuzenIptal.Click += new System.EventHandler(this.btnDuzenIptal_Click);
            // 
            // btnFotos
            // 
            this.btnFotos.Image = global::CengTurApps.Properties.Resources.Foto;
            this.btnFotos.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFotos.Location = new System.Drawing.Point(218, 57);
            this.btnFotos.Name = "btnFotos";
            this.btnFotos.Size = new System.Drawing.Size(95, 32);
            this.btnFotos.TabIndex = 23;
            this.btnFotos.Text = "  &Foto�raflar";
            this.btnFotos.UseVisualStyleBackColor = true;
            this.btnFotos.Click += new System.EventHandler(this.btnFotos_Click);
            // 
            // btnThumb
            // 
            this.btnThumb.Image = global::CengTurApps.Properties.Resources.Thumb;
            this.btnThumb.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnThumb.Location = new System.Drawing.Point(218, 19);
            this.btnThumb.Name = "btnThumb";
            this.btnThumb.Size = new System.Drawing.Size(95, 32);
            this.btnThumb.TabIndex = 22;
            this.btnThumb.Text = " K���k &Resim";
            this.btnThumb.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnThumb.UseVisualStyleBackColor = true;
            this.btnThumb.Click += new System.EventHandler(this.btnThumb_Click);
            // 
            // btnKonumBilgisi
            // 
            this.btnKonumBilgisi.Image = global::CengTurApps.Properties.Resources.Branch;
            this.btnKonumBilgisi.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnKonumBilgisi.Location = new System.Drawing.Point(117, 57);
            this.btnKonumBilgisi.Name = "btnKonumBilgisi";
            this.btnKonumBilgisi.Size = new System.Drawing.Size(95, 32);
            this.btnKonumBilgisi.TabIndex = 21;
            this.btnKonumBilgisi.Text = "&Konum Bilgisi";
            this.btnKonumBilgisi.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnKonumBilgisi.UseVisualStyleBackColor = true;
            this.btnKonumBilgisi.Click += new System.EventHandler(this.btnKonumBilgisi_Click);
            // 
            // pBoxThumb
            // 
            this.pBoxThumb.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pBoxThumb.Location = new System.Drawing.Point(6, 19);
            this.pBoxThumb.Name = "pBoxThumb";
            this.pBoxThumb.Size = new System.Drawing.Size(105, 70);
            this.pBoxThumb.TabIndex = 19;
            this.pBoxThumb.TabStop = false;
            // 
            // btnTesisBilgileri
            // 
            this.btnTesisBilgileri.Image = global::CengTurApps.Properties.Resources.Tesis;
            this.btnTesisBilgileri.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnTesisBilgileri.Location = new System.Drawing.Point(117, 19);
            this.btnTesisBilgileri.Name = "btnTesisBilgileri";
            this.btnTesisBilgileri.Size = new System.Drawing.Size(95, 32);
            this.btnTesisBilgileri.TabIndex = 20;
            this.btnTesisBilgileri.Text = "&Tesis Bilgileri";
            this.btnTesisBilgileri.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnTesisBilgileri.UseVisualStyleBackColor = true;
            this.btnTesisBilgileri.Click += new System.EventHandler(this.btnTesisBilgileri_Click);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "House.png");
            this.imageList1.Images.SetKeyName(1, "Tesis.png");
            this.imageList1.Images.SetKeyName(2, "Branch.png");
            this.imageList1.Images.SetKeyName(3, "Thumb.png");
            this.imageList1.Images.SetKeyName(4, "Foto.png");
            this.imageList1.Images.SetKeyName(5, "Add.png");
            this.imageList1.Images.SetKeyName(6, "Refresh.png");
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lstBoxOtel);
            this.groupBox2.Location = new System.Drawing.Point(277, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(200, 110);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Mevcut Oteller";
            // 
            // lstBoxOtel
            // 
            this.lstBoxOtel.Font = new System.Drawing.Font("Lucida Console", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lstBoxOtel.FormattingEnabled = true;
            this.lstBoxOtel.ItemHeight = 12;
            this.lstBoxOtel.Location = new System.Drawing.Point(6, 16);
            this.lstBoxOtel.Name = "lstBoxOtel";
            this.lstBoxOtel.Size = new System.Drawing.Size(188, 88);
            this.lstBoxOtel.TabIndex = 0;
            this.lstBoxOtel.DoubleClick += new System.EventHandler(this.lstBoxOtel_DoubleClick);
            this.lstBoxOtel.SelectedIndexChanged += new System.EventHandler(this.lstBoxOtel_SelectedIndexChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.cmbBolge);
            this.groupBox1.Controls.Add(this.cmbSehir);
            this.groupBox1.Controls.Add(this.cmbTurlar);
            this.groupBox1.Location = new System.Drawing.Point(6, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(265, 110);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Konum Bilgileri";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 72);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Alt B�lge:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 46);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "�st B�lge:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Tur Tan�m�:";
            // 
            // cmbBolge
            // 
            this.cmbBolge.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBolge.Font = new System.Drawing.Font("Lucida Console", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.cmbBolge.FormattingEnabled = true;
            this.cmbBolge.Location = new System.Drawing.Point(72, 71);
            this.cmbBolge.Name = "cmbBolge";
            this.cmbBolge.Size = new System.Drawing.Size(175, 20);
            this.cmbBolge.TabIndex = 3;
            this.cmbBolge.SelectedIndexChanged += new System.EventHandler(this.cmbBolge_SelectedIndexChanged);
            // 
            // cmbSehir
            // 
            this.cmbSehir.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSehir.Font = new System.Drawing.Font("Lucida Console", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.cmbSehir.FormattingEnabled = true;
            this.cmbSehir.Location = new System.Drawing.Point(72, 45);
            this.cmbSehir.Name = "cmbSehir";
            this.cmbSehir.Size = new System.Drawing.Size(175, 20);
            this.cmbSehir.TabIndex = 2;
            this.cmbSehir.SelectedIndexChanged += new System.EventHandler(this.cmbSehir_SelectedIndexChanged);
            // 
            // cmbTurlar
            // 
            this.cmbTurlar.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTurlar.Font = new System.Drawing.Font("Lucida Console", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.cmbTurlar.FormattingEnabled = true;
            this.cmbTurlar.Location = new System.Drawing.Point(72, 19);
            this.cmbTurlar.Name = "cmbTurlar";
            this.cmbTurlar.Size = new System.Drawing.Size(175, 20);
            this.cmbTurlar.TabIndex = 1;
            this.cmbTurlar.SelectedIndexChanged += new System.EventHandler(this.cmbTurlar_SelectedIndexChanged);
            // 
            // tabPageTesis
            // 
            this.tabPageTesis.Controls.Add(this.groupBox6);
            this.tabPageTesis.ImageIndex = 1;
            this.tabPageTesis.Location = new System.Drawing.Point(4, 23);
            this.tabPageTesis.Name = "tabPageTesis";
            this.tabPageTesis.Size = new System.Drawing.Size(482, 308);
            this.tabPageTesis.TabIndex = 2;
            this.tabPageTesis.Text = "Tesis Bilgileri";
            this.tabPageTesis.UseVisualStyleBackColor = true;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.txtTesisBilgileri);
            this.groupBox6.Controls.Add(this.lblTesisBilgileri_Info);
            this.groupBox6.Controls.Add(this.btnTesisBilgisiEkle);
            this.groupBox6.Location = new System.Drawing.Point(6, 6);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(470, 296);
            this.groupBox6.TabIndex = 0;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Tesis Bilgileri";
            // 
            // txtTesisBilgileri
            // 
            this.txtTesisBilgileri.Location = new System.Drawing.Point(9, 19);
            this.txtTesisBilgileri.Multiline = true;
            this.txtTesisBilgileri.Name = "txtTesisBilgileri";
            this.txtTesisBilgileri.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtTesisBilgileri.Size = new System.Drawing.Size(455, 242);
            this.txtTesisBilgileri.TabIndex = 4;
            // 
            // lblTesisBilgileri_Info
            // 
            this.lblTesisBilgileri_Info.AutoSize = true;
            this.lblTesisBilgileri_Info.ForeColor = System.Drawing.Color.Red;
            this.lblTesisBilgileri_Info.Location = new System.Drawing.Point(6, 267);
            this.lblTesisBilgileri_Info.Name = "lblTesisBilgileri_Info";
            this.lblTesisBilgileri_Info.Size = new System.Drawing.Size(98, 13);
            this.lblTesisBilgileri_Info.TabIndex = 3;
            this.lblTesisBilgileri_Info.Text = "lblTesisBilgileri_Info";
            // 
            // btnTesisBilgisiEkle
            // 
            this.btnTesisBilgisiEkle.Image = global::CengTurApps.Properties.Resources.Update;
            this.btnTesisBilgisiEkle.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnTesisBilgisiEkle.Location = new System.Drawing.Point(389, 267);
            this.btnTesisBilgisiEkle.Name = "btnTesisBilgisiEkle";
            this.btnTesisBilgisiEkle.Size = new System.Drawing.Size(75, 23);
            this.btnTesisBilgisiEkle.TabIndex = 1;
            this.btnTesisBilgisiEkle.Text = "Ekle";
            this.btnTesisBilgisiEkle.UseVisualStyleBackColor = true;
            this.btnTesisBilgisiEkle.Click += new System.EventHandler(this.btnTesisBilgisiEkle_Click);
            // 
            // tabPageKonum
            // 
            this.tabPageKonum.ImageIndex = 2;
            this.tabPageKonum.Location = new System.Drawing.Point(4, 23);
            this.tabPageKonum.Name = "tabPageKonum";
            this.tabPageKonum.Size = new System.Drawing.Size(482, 308);
            this.tabPageKonum.TabIndex = 3;
            this.tabPageKonum.Text = "Konum Bilgisi";
            this.tabPageKonum.UseVisualStyleBackColor = true;
            // 
            // tabPageThumb
            // 
            this.tabPageThumb.Controls.Add(this.webBrowser1);
            this.tabPageThumb.ImageIndex = 3;
            this.tabPageThumb.Location = new System.Drawing.Point(4, 23);
            this.tabPageThumb.Name = "tabPageThumb";
            this.tabPageThumb.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageThumb.Size = new System.Drawing.Size(482, 308);
            this.tabPageThumb.TabIndex = 1;
            this.tabPageThumb.Text = "K���k Resim";
            this.tabPageThumb.UseVisualStyleBackColor = true;
            // 
            // webBrowser1
            // 
            this.webBrowser1.AllowWebBrowserDrop = false;
            this.webBrowser1.IsWebBrowserContextMenuEnabled = false;
            this.webBrowser1.Location = new System.Drawing.Point(0, 0);
            this.webBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.Size = new System.Drawing.Size(476, 302);
            this.webBrowser1.TabIndex = 0;
            this.webBrowser1.WebBrowserShortcutsEnabled = false;
            // 
            // tabPageFotos
            // 
            this.tabPageFotos.Controls.Add(this.webBrowser2);
            this.tabPageFotos.ImageIndex = 4;
            this.tabPageFotos.Location = new System.Drawing.Point(4, 23);
            this.tabPageFotos.Name = "tabPageFotos";
            this.tabPageFotos.Size = new System.Drawing.Size(482, 308);
            this.tabPageFotos.TabIndex = 4;
            this.tabPageFotos.Text = "Foto�raflar";
            this.tabPageFotos.UseVisualStyleBackColor = true;
            // 
            // toolTip1
            // 
            this.toolTip1.AutoPopDelay = 500;
            this.toolTip1.InitialDelay = 500;
            this.toolTip1.IsBalloon = true;
            this.toolTip1.ReshowDelay = 100;
            this.toolTip1.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            // 
            // pnlFormTitle
            // 
            this.pnlFormTitle.BackgroundImage = global::CengTurApps.Properties.Resources.PanelBack;
            this.pnlFormTitle.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlFormTitle.Controls.Add(this.lblFormTitle);
            this.pnlFormTitle.Location = new System.Drawing.Point(12, 8);
            this.pnlFormTitle.Name = "pnlFormTitle";
            this.pnlFormTitle.Size = new System.Drawing.Size(490, 40);
            this.pnlFormTitle.TabIndex = 16;
            // 
            // lblFormTitle
            // 
            this.lblFormTitle.AutoSize = true;
            this.lblFormTitle.BackColor = System.Drawing.Color.Transparent;
            this.lblFormTitle.Font = new System.Drawing.Font("Trebuchet MS", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblFormTitle.ForeColor = System.Drawing.Color.White;
            this.lblFormTitle.Location = new System.Drawing.Point(0, 6);
            this.lblFormTitle.Name = "lblFormTitle";
            this.lblFormTitle.Size = new System.Drawing.Size(145, 29);
            this.lblFormTitle.TabIndex = 0;
            this.lblFormTitle.Text = "otel bilgileri";
            // 
            // btnIptal
            // 
            this.btnIptal.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnIptal.Image = global::CengTurApps.Properties.Resources.Cancel;
            this.btnIptal.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnIptal.Location = new System.Drawing.Point(427, 395);
            this.btnIptal.Name = "btnIptal";
            this.btnIptal.Size = new System.Drawing.Size(75, 23);
            this.btnIptal.TabIndex = 4;
            this.btnIptal.Text = "Kapat";
            this.btnIptal.UseVisualStyleBackColor = true;
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            this.errorProvider1.RightToLeft = true;
            // 
            // webBrowser2
            // 
            this.webBrowser2.AllowWebBrowserDrop = false;
            this.webBrowser2.IsWebBrowserContextMenuEnabled = false;
            this.webBrowser2.Location = new System.Drawing.Point(0, 0);
            this.webBrowser2.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser2.Name = "webBrowser2";
            this.webBrowser2.Size = new System.Drawing.Size(476, 302);
            this.webBrowser2.TabIndex = 0;
            this.webBrowser2.WebBrowserShortcutsEnabled = false;
            // 
            // OtelBilgileri
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(514, 423);
            this.Controls.Add(this.pnlFormTitle);
            this.Controls.Add(this.btnIptal);
            this.Controls.Add(this.tabControlOtel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "OtelBilgileri";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "OtelBilgileri";
            this.Load += new System.EventHandler(this.OtelBilgileri_Load);
            this.tabControlOtel.ResumeLayout(false);
            this.tabPageOtel.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.tabControlDetay.ResumeLayout(false);
            this.tabPageOtelEkle.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.tabPageOtelDuzenle.ResumeLayout(false);
            this.tabPageOtelDuzenle.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pBoxThumb)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPageTesis.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.tabPageThumb.ResumeLayout(false);
            this.tabPageFotos.ResumeLayout(false);
            this.pnlFormTitle.ResumeLayout(false);
            this.pnlFormTitle.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControlOtel;
        private System.Windows.Forms.TabPage tabPageOtel;
        private System.Windows.Forms.TabPage tabPageThumb;
        private System.Windows.Forms.Button btnIptal;
        private System.Windows.Forms.Panel pnlFormTitle;
        private System.Windows.Forms.Label lblFormTitle;
        private System.Windows.Forms.WebBrowser webBrowser1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox cmbTurlar;
        private System.Windows.Forms.ComboBox cmbBolge;
        private System.Windows.Forms.ComboBox cmbSehir;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ListBox lstBoxOtel;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TabControl tabControlDetay;
        private System.Windows.Forms.TabPage tabPageOtelEkle;
        private System.Windows.Forms.TabPage tabPageOtelDuzenle;
        private System.Windows.Forms.PictureBox pBoxThumb;
        private System.Windows.Forms.Button btnTesisBilgileri;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label lblHit;
        private System.Windows.Forms.Button btnFotos;
        private System.Windows.Forms.Button btnThumb;
        private System.Windows.Forms.Button btnKonumBilgisi;
        private System.Windows.Forms.Button btnDuzenIptal;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox txtOtelAdi;
        private System.Windows.Forms.Button btnOtelEkle;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.Label lblHata;
        private System.Windows.Forms.TabPage tabPageTesis;
        private System.Windows.Forms.TabPage tabPageKonum;
        private System.Windows.Forms.TabPage tabPageFotos;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Button btnTesisBilgisiEkle;
        private System.Windows.Forms.Label lblTesisBilgileri_Info;
        private System.Windows.Forms.TextBox txtTesisBilgileri;
        private System.Windows.Forms.WebBrowser webBrowser2;
    }
}