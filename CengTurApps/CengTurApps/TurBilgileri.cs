using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.Sql;
using System.Data.SqlClient;

namespace CengTurApp
{
    public partial class TurBilgileri : Form
    {
        public TurBilgileri()
        {
            InitializeComponent();
        }

        private void FillLstBox()
        {
            SqlConnection Baglanti = myClass.SQLServer();
            lstBoxTurlar.Items.Clear();
            try
            {
                SqlCommand cmdTurlar = new SqlCommand("SELECT TurKodu, TurAdi FROM Turlar WHERE TurKodu <> '0'", Baglanti);
                cmdTurlar.CommandType = CommandType.Text;
                Baglanti.Open();
                SqlDataReader DR = cmdTurlar.ExecuteReader(CommandBehavior.CloseConnection);
                while (DR.Read())
                    lstBoxTurlar.Items.Add(DR.GetString(0) + "     " + DR.GetString(1));
            }
            catch (Exception Hata)
            {
                MessageBox.Show(Hata.Message, "Hata", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Baglanti.Close();
            }
        }

        private void TurBilgileri_Load(object sender, EventArgs e)
        {
            FillLstBox();
            
        }

        private void lstBoxTurlar_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                if (lstBoxTurlar.SelectedIndex != -1)
                {
                    string TurKodu = lstBoxTurlar.SelectedItem.ToString().Substring(0, 10).Trim();
                    string TurAdi = lstBoxTurlar.SelectedItem.ToString().Remove(0, 10);
                    toolTip1.SetToolTip(lstBoxTurlar, TurAdi);
                    lstBoxTurlar.ContextMenuStrip = contextMenuStrip1;
                }
            }
        }
        
        bool Degisitir = new bool();
        private void lstBoxTurlar_DoubleClick(object sender, EventArgs e)
        {
            if (lstBoxTurlar.SelectedIndex != -1)
            {
                errorProvider1.Dispose();
                Degisitir = true;
                groupBox2.Text = "De�i�tir";
                txtTurKodu.Enabled = false;
                txtTurKodu.Text = lstBoxTurlar.SelectedItem.ToString().Substring(0, 10).Trim();
                txtTurAdi.Text = lstBoxTurlar.SelectedItem.ToString().Remove(0, 10);
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (Degisitir)
                {
                    errorProvider1.Dispose();
                    if (txtTurAdi.Text == "")
                        errorProvider1.SetError(txtTurAdi, "Bo� olamaz!");
                    else
                    {
                        string TurAdi = lstBoxTurlar.SelectedItem.ToString().Remove(0, 10);
                        DialogResult cvp = MessageBox.Show(TurAdi + " olan tur tan�m�;\n" + txtTurAdi.Text.ToUpper() + " olarak de�i�tirilecektir.\n\nDevam etmek istedi�inize emin misiniz?", "Uyar�", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2);
                        if (cvp == DialogResult.Yes)
                        {
                            myClass.ExecQuery("UPDATE Turlar SET TurAdi='" + txtTurAdi.Text.ToUpper() + "' WHERE TurKodu='" + txtTurKodu.Text + "'");
                            Degisitir = false;
                            groupBox2.Text = "Ekle";
                            txtTurKodu.Enabled = true;
                            txtTurKodu.Text = string.Empty;
                            txtTurAdi.Text = string.Empty;
                            FillLstBox();
                        }
                    }
                }
                else
                {
                    errorProvider1.Dispose();
                    if (txtTurKodu.Text == "")
                        errorProvider1.SetError(txtTurKodu, "Bo� olamaz!");
                    else if (txtTurAdi.Text == "")
                        errorProvider1.SetError(txtTurAdi, "Bo� olamaz!");
                    else
                    {
                        DialogResult cvp = MessageBox.Show("A�a��daki tur tan�m� veri taban�na eklenecektir.\n" + txtTurKodu.Text.ToUpper() + " - " + txtTurAdi.Text.ToUpper() + "\n\nDevam etmek istedi�inize emin misiniz?", "Uyar�", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2);
                        if (cvp == DialogResult.Yes)
                        {
                            myClass.ExecQuery("INSERT INTO Turlar (TurKodu,TurAdi) VALUES ('" + txtTurKodu.Text.ToUpper() + "','" + txtTurAdi.Text.ToUpper() + "')");
                            txtTurKodu.Text = string.Empty;
                            txtTurAdi.Text = string.Empty;
                            FillLstBox();
                        }
                    }
                }
            }
            catch (Exception Hata)
            {
                MessageBox.Show(Hata.Message, "Hata", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void SilToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (lstBoxTurlar.SelectedIndex != -1)
            {
                string TurKodu = lstBoxTurlar.SelectedItem.ToString().Substring(0, 10).Trim();
                string TurAdi = lstBoxTurlar.SelectedItem.ToString().Remove(0, 10);

                DialogResult cvp = MessageBox.Show("A�a��daki tur tan�m� kal�c� olarak silinecektir.\n" + TurKodu + " - " + TurAdi + "\n\nDevam etmek istedi�inize emin misiniz?", "Uyar�", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2);
                if (cvp == DialogResult.Yes)
                {
                    myClass.ExecQuery("DELETE FROM Turlar WHERE TurKodu='" + TurKodu + "'");
                    FillLstBox();
                }
            }
        }

        private void EkleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Degisitir = false;
            groupBox2.Text = "Ekle";
            txtTurKodu.Enabled = true;
            txtTurKodu.Text = string.Empty;
            txtTurAdi.Text = string.Empty;
            txtTurKodu.Focus();
        }

        private void btnIptal_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}