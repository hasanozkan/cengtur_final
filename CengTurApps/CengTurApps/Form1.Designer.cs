namespace CengTurApp
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.DosyaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CikisToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rezervasyonToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rezervasyonEkleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.VeriTabaniToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.TurBilgileriToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.BolgeBilgileriToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.UstBolgeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.AltBolgeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.HizmetTurleriToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripSeparator();
            this.OtelBilgileriToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.m��teri�li�kileriToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gelenM��teriMesajlar�ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hakl�M��teriMesajlar�ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.AyarlarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SQLServerBilgileriToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.YardimToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.AboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.DosyaToolStripMenuItem,
            this.rezervasyonToolStripMenuItem,
            this.VeriTabaniToolStripMenuItem,
            this.m��teri�li�kileriToolStripMenuItem,
            this.AyarlarToolStripMenuItem,
            this.YardimToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(634, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // DosyaToolStripMenuItem
            // 
            this.DosyaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CikisToolStripMenuItem});
            this.DosyaToolStripMenuItem.Name = "DosyaToolStripMenuItem";
            this.DosyaToolStripMenuItem.Size = new System.Drawing.Size(51, 20);
            this.DosyaToolStripMenuItem.Text = "Dosya";
            // 
            // CikisToolStripMenuItem
            // 
            this.CikisToolStripMenuItem.Image = global::CengTurApps.Properties.Resources.Cancel;
            this.CikisToolStripMenuItem.Name = "CikisToolStripMenuItem";
            this.CikisToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.X)));
            this.CikisToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.CikisToolStripMenuItem.Text = "��k��";
            this.CikisToolStripMenuItem.Click += new System.EventHandler(this.CikisToolStripMenuItem_Click);
            // 
            // rezervasyonToolStripMenuItem
            // 
            this.rezervasyonToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.rezervasyonEkleToolStripMenuItem});
            this.rezervasyonToolStripMenuItem.Name = "rezervasyonToolStripMenuItem";
            this.rezervasyonToolStripMenuItem.Size = new System.Drawing.Size(84, 20);
            this.rezervasyonToolStripMenuItem.Text = "Rezervasyon";
            // 
            // rezervasyonEkleToolStripMenuItem
            // 
            this.rezervasyonEkleToolStripMenuItem.Name = "rezervasyonEkleToolStripMenuItem";
            this.rezervasyonEkleToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.rezervasyonEkleToolStripMenuItem.Text = "Rezervasyon Ekle";
            // 
            // VeriTabaniToolStripMenuItem
            // 
            this.VeriTabaniToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TurBilgileriToolStripMenuItem,
            this.BolgeBilgileriToolStripMenuItem,
            this.toolStripMenuItem1,
            this.HizmetTurleriToolStripMenuItem,
            this.toolStripMenuItem3,
            this.OtelBilgileriToolStripMenuItem});
            this.VeriTabaniToolStripMenuItem.Name = "VeriTabaniToolStripMenuItem";
            this.VeriTabaniToolStripMenuItem.Size = new System.Drawing.Size(78, 20);
            this.VeriTabaniToolStripMenuItem.Text = "Veri Taban�";
            // 
            // TurBilgileriToolStripMenuItem
            // 
            this.TurBilgileriToolStripMenuItem.Image = global::CengTurApps.Properties.Resources.RenkPaleti;
            this.TurBilgileriToolStripMenuItem.Name = "TurBilgileriToolStripMenuItem";
            this.TurBilgileriToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
            this.TurBilgileriToolStripMenuItem.Text = "Tur Bilgileri";
            this.TurBilgileriToolStripMenuItem.Click += new System.EventHandler(this.TurBilgileriToolStripMenuItem_Click);
            // 
            // BolgeBilgileriToolStripMenuItem
            // 
            this.BolgeBilgileriToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.UstBolgeToolStripMenuItem,
            this.AltBolgeToolStripMenuItem});
            this.BolgeBilgileriToolStripMenuItem.Image = global::CengTurApps.Properties.Resources.Branch;
            this.BolgeBilgileriToolStripMenuItem.Name = "BolgeBilgileriToolStripMenuItem";
            this.BolgeBilgileriToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
            this.BolgeBilgileriToolStripMenuItem.Text = "B�lge Bilgileri";
            // 
            // UstBolgeToolStripMenuItem
            // 
            this.UstBolgeToolStripMenuItem.Image = global::CengTurApps.Properties.Resources.UstBolge;
            this.UstBolgeToolStripMenuItem.Name = "UstBolgeToolStripMenuItem";
            this.UstBolgeToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.UstBolgeToolStripMenuItem.Text = "�st B�lge";
            this.UstBolgeToolStripMenuItem.Click += new System.EventHandler(this.UstBolgeToolStripMenuItem_Click);
            // 
            // AltBolgeToolStripMenuItem
            // 
            this.AltBolgeToolStripMenuItem.Image = global::CengTurApps.Properties.Resources.AltBolge;
            this.AltBolgeToolStripMenuItem.Name = "AltBolgeToolStripMenuItem";
            this.AltBolgeToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.AltBolgeToolStripMenuItem.Text = "Alt B�lge";
            this.AltBolgeToolStripMenuItem.Click += new System.EventHandler(this.AltBolgeToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(146, 6);
            // 
            // HizmetTurleriToolStripMenuItem
            // 
            this.HizmetTurleriToolStripMenuItem.Image = global::CengTurApps.Properties.Resources.RedStar;
            this.HizmetTurleriToolStripMenuItem.Name = "HizmetTurleriToolStripMenuItem";
            this.HizmetTurleriToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
            this.HizmetTurleriToolStripMenuItem.Text = "Hizmet T�rleri";
            this.HizmetTurleriToolStripMenuItem.Click += new System.EventHandler(this.HizmetTurleriToolStripMenuItem_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(146, 6);
            // 
            // OtelBilgileriToolStripMenuItem
            // 
            this.OtelBilgileriToolStripMenuItem.Image = global::CengTurApps.Properties.Resources.House;
            this.OtelBilgileriToolStripMenuItem.Name = "OtelBilgileriToolStripMenuItem";
            this.OtelBilgileriToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
            this.OtelBilgileriToolStripMenuItem.Text = "Otel Bilgileri";
            this.OtelBilgileriToolStripMenuItem.Click += new System.EventHandler(this.OtelBilgileriToolStripMenuItem_Click);
            // 
            // m��teri�li�kileriToolStripMenuItem
            // 
            this.m��teri�li�kileriToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.gelenM��teriMesajlar�ToolStripMenuItem,
            this.hakl�M��teriMesajlar�ToolStripMenuItem});
            this.m��teri�li�kileriToolStripMenuItem.Name = "m��teri�li�kileriToolStripMenuItem";
            this.m��teri�li�kileriToolStripMenuItem.Size = new System.Drawing.Size(101, 20);
            this.m��teri�li�kileriToolStripMenuItem.Text = "M��teri �li�kileri";
            // 
            // gelenM��teriMesajlar�ToolStripMenuItem
            // 
            this.gelenM��teriMesajlar�ToolStripMenuItem.Name = "gelenM��teriMesajlar�ToolStripMenuItem";
            this.gelenM��teriMesajlar�ToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.gelenM��teriMesajlar�ToolStripMenuItem.Text = "Gelen M��teri Mesajlar�";
            this.gelenM��teriMesajlar�ToolStripMenuItem.Click += new System.EventHandler(this.gelenM��teriMesajlar�ToolStripMenuItem_Click);
            // 
            // hakl�M��teriMesajlar�ToolStripMenuItem
            // 
            this.hakl�M��teriMesajlar�ToolStripMenuItem.Name = "hakl�M��teriMesajlar�ToolStripMenuItem";
            this.hakl�M��teriMesajlar�ToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.hakl�M��teriMesajlar�ToolStripMenuItem.Text = "Hakl� M��teri Mesajlar�";
            // 
            // AyarlarToolStripMenuItem
            // 
            this.AyarlarToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.SQLServerBilgileriToolStripMenuItem});
            this.AyarlarToolStripMenuItem.Name = "AyarlarToolStripMenuItem";
            this.AyarlarToolStripMenuItem.Size = new System.Drawing.Size(56, 20);
            this.AyarlarToolStripMenuItem.Text = "Ayarlar";
            // 
            // SQLServerBilgileriToolStripMenuItem
            // 
            this.SQLServerBilgileriToolStripMenuItem.Image = global::CengTurApps.Properties.Resources.DataBaseKey;
            this.SQLServerBilgileriToolStripMenuItem.Name = "SQLServerBilgileriToolStripMenuItem";
            this.SQLServerBilgileriToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.SQLServerBilgileriToolStripMenuItem.Text = "SQL Server Bilgileri";
            this.SQLServerBilgileriToolStripMenuItem.Click += new System.EventHandler(this.SQLServerBilgileriToolStripMenuItem_Click);
            // 
            // YardimToolStripMenuItem
            // 
            this.YardimToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.AboutToolStripMenuItem});
            this.YardimToolStripMenuItem.Name = "YardimToolStripMenuItem";
            this.YardimToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
            this.YardimToolStripMenuItem.Text = "Yard�m";
            // 
            // AboutToolStripMenuItem
            // 
            this.AboutToolStripMenuItem.Image = global::CengTurApps.Properties.Resources.vCard;
            this.AboutToolStripMenuItem.Name = "AboutToolStripMenuItem";
            this.AboutToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.AboutToolStripMenuItem.Text = "Hakk�nda";
            this.AboutToolStripMenuItem.Click += new System.EventHandler(this.AboutToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(634, 448);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CengTur Y�netim Paneli";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem DosyaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem CikisToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem AyarlarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem SQLServerBilgileriToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem VeriTabaniToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem HizmetTurleriToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem TurBilgileriToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem BolgeBilgileriToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem UstBolgeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem AltBolgeToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem OtelBilgileriToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem YardimToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem AboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rezervasyonToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rezervasyonEkleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem m��teri�li�kileriToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gelenM��teriMesajlar�ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem hakl�M��teriMesajlar�ToolStripMenuItem;
    }
}

