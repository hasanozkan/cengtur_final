using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace CengTurApp
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            myClass.SQLServer().Open();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            myClass.SQLServer().Close();
            Application.Exit();
        }

        private void SQLServerBilgileriToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ChangeServer frmChangeServer = new ChangeServer();
            frmChangeServer.ShowDialog();
        }

        private void CikisToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void HizmetTurleriToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Hizmet frmHizmet = new Hizmet();
            frmHizmet.ShowDialog();
        }

        private void TurBilgileriToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TurBilgileri frmTurBilgileri = new TurBilgileri();
            frmTurBilgileri.ShowDialog();
        }

        private void UstBolgeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Sehir frmSehir = new Sehir();
            frmSehir.ShowDialog();
        }

        private void AltBolgeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Bolge frmBolge = new Bolge();
            frmBolge.ShowDialog();
        }

        private void AboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutBox1 frmAbout = new AboutBox1();
            frmAbout.ShowDialog();
        }

        private void OtelBilgileriToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OtelBilgileri frmOtelBilgileri = new OtelBilgileri();
            frmOtelBilgileri.ShowDialog();
        }

        private void gelenM��teriMesajlar�ToolStripMenuItem_Click(object sender, EventArgs e)
        {
           
        }
    }
}