namespace CengTurApp
{
    partial class TurBilgileri
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtTurAdi = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtTurKodu = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lstBoxTurlar = new System.Windows.Forms.ListBox();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnIptal = new System.Windows.Forms.Button();
            this.EkleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.DegistirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SilToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.KapatToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.EkleToolStripMenuItem,
            this.DegistirToolStripMenuItem,
            this.SilToolStripMenuItem,
            this.toolStripMenuItem1,
            this.KapatToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(153, 120);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(118, 6);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtTurAdi);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.txtTurKodu);
            this.groupBox2.Controls.Add(this.btnOk);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Location = new System.Drawing.Point(12, 143);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(262, 98);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Ekle";
            // 
            // txtTurAdi
            // 
            this.txtTurAdi.Location = new System.Drawing.Point(82, 43);
            this.txtTurAdi.MaxLength = 50;
            this.txtTurAdi.Name = "txtTurAdi";
            this.txtTurAdi.Size = new System.Drawing.Size(160, 20);
            this.txtTurAdi.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 46);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Tur Tan�m�:";
            // 
            // txtTurKodu
            // 
            this.txtTurKodu.Location = new System.Drawing.Point(82, 17);
            this.txtTurKodu.MaxLength = 5;
            this.txtTurKodu.Name = "txtTurKodu";
            this.txtTurKodu.Size = new System.Drawing.Size(160, 20);
            this.txtTurKodu.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Tur Kodu:";
            // 
            // toolTip1
            // 
            this.toolTip1.IsBalloon = true;
            this.toolTip1.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.toolTip1.ToolTipTitle = "De�i�tirmek i�in �ift t�klay�n�z.";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lstBoxTurlar);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(262, 125);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Turlar";
            // 
            // lstBoxTurlar
            // 
            this.lstBoxTurlar.Font = new System.Drawing.Font("Lucida Console", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstBoxTurlar.HorizontalScrollbar = true;
            this.lstBoxTurlar.ItemHeight = 12;
            this.lstBoxTurlar.Location = new System.Drawing.Point(6, 19);
            this.lstBoxTurlar.Name = "lstBoxTurlar";
            this.lstBoxTurlar.Size = new System.Drawing.Size(250, 100);
            this.lstBoxTurlar.TabIndex = 0;
            this.lstBoxTurlar.MouseClick += new System.Windows.Forms.MouseEventHandler(this.lstBoxTurlar_MouseClick);
            this.lstBoxTurlar.DoubleClick += new System.EventHandler(this.lstBoxTurlar_DoubleClick);
            // 
            // btnOk
            // 
            this.btnOk.Image = global::CengTurApps.Properties.Resources.AcceptBtn;
            this.btnOk.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnOk.Location = new System.Drawing.Point(167, 69);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 2;
            this.btnOk.Text = "    Tamam";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnIptal
            // 
            this.btnIptal.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnIptal.Image = global::CengTurApps.Properties.Resources.Cancel;
            this.btnIptal.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnIptal.Location = new System.Drawing.Point(199, 247);
            this.btnIptal.Name = "btnIptal";
            this.btnIptal.Size = new System.Drawing.Size(75, 23);
            this.btnIptal.TabIndex = 3;
            this.btnIptal.Text = "Kapat";
            this.btnIptal.UseVisualStyleBackColor = true;
            this.btnIptal.Click += new System.EventHandler(this.btnIptal_Click);
            // 
            // EkleToolStripMenuItem
            // 
            this.EkleToolStripMenuItem.Image = global::CengTurApps.Properties.Resources.AddRec;
            this.EkleToolStripMenuItem.Name = "EkleToolStripMenuItem";
            this.EkleToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.EkleToolStripMenuItem.Text = "Ekle";
            this.EkleToolStripMenuItem.Click += new System.EventHandler(this.EkleToolStripMenuItem_Click);
            // 
            // DegistirToolStripMenuItem
            // 
            this.DegistirToolStripMenuItem.Image = global::CengTurApps.Properties.Resources.Update;
            this.DegistirToolStripMenuItem.Name = "DegistirToolStripMenuItem";
            this.DegistirToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.DegistirToolStripMenuItem.Text = "De�i�tir";
            this.DegistirToolStripMenuItem.Click += new System.EventHandler(this.lstBoxTurlar_DoubleClick);
            // 
            // SilToolStripMenuItem
            // 
            this.SilToolStripMenuItem.Image = global::CengTurApps.Properties.Resources.DeleteRec;
            this.SilToolStripMenuItem.Name = "SilToolStripMenuItem";
            this.SilToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.SilToolStripMenuItem.Text = "Sil";
            this.SilToolStripMenuItem.Click += new System.EventHandler(this.SilToolStripMenuItem_Click);
            // 
            // KapatToolStripMenuItem
            // 
            this.KapatToolStripMenuItem.Image = global::CengTurApps.Properties.Resources.Cancel;
            this.KapatToolStripMenuItem.Name = "KapatToolStripMenuItem";
            this.KapatToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.KapatToolStripMenuItem.Text = "Kapat";
            this.KapatToolStripMenuItem.Click += new System.EventHandler(this.btnIptal_Click);
            // 
            // TurBilgileri
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnIptal;
            this.ClientSize = new System.Drawing.Size(284, 278);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.btnIptal);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TurBilgileri";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Tur Bilgileri";
            this.Load += new System.EventHandler(this.TurBilgileri_Load);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolStripMenuItem EkleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem DegistirToolStripMenuItem;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtTurAdi;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtTurKodu;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ListBox lstBoxTurlar;
        private System.Windows.Forms.Button btnIptal;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem SilToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem KapatToolStripMenuItem;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}