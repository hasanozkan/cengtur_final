using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.Sql;
using System.Data.SqlClient;
using System.IO;
using System.Net;

namespace CengTurApp
{
    public partial class OtelBilgileri : Form
    {
        public OtelBilgileri()
        {
            InitializeComponent();
        }

        #region Global De�i�kenler
        string Url = "http://localhost:53461/CengTur";
        string[] arrayTKodu;
        short[] arraySKodu;
        int[] arrayBKodu;
        int[] arrayOtelKodu;
        bool OtelDuzenle = new bool();
        SqlDataReader DROtelBilgisi;
        int OtelKodu;
        #endregion

        #region Liste i�lemleri
        private void FillCmbBoxTurlar()
        {
            SqlConnection Baglanti = myClass.SQLServer();
            cmbTurlar.Items.Clear();
            try
            {
                arrayTKodu = new string[myClass.RecCount("SELECT Count(*) FROM Turlar WHERE TurKodu <> '0'")];
                SqlCommand cmdTurlar = new SqlCommand("SELECT TurKodu, TurAdi FROM Turlar WHERE TurKodu <> '0'", Baglanti);
                cmdTurlar.CommandType = CommandType.Text;
                Baglanti.Open();
                SqlDataReader DR = cmdTurlar.ExecuteReader(CommandBehavior.CloseConnection);
                int i = new int();
                while (DR.Read())
                {
                    arrayTKodu[i] = DR.GetString(0);
                    cmbTurlar.Items.Add(DR.GetString(1));
                    i++;
                }
            }
            catch (Exception Hata)
            {
                MessageBox.Show(Hata.Message, "Hata", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Baglanti.Close();
            }
        }
        private void FillCmbBoxSehir()
        {
            SqlConnection Baglanti = myClass.SQLServer();
            cmbSehir.Items.Clear();
            try
            {
                arraySKodu = new short[myClass.RecCount("SELECT Count(*) FROM Sehir WHERE TurKodu = '" + arrayTKodu[cmbTurlar.SelectedIndex] + "'")];
                SqlCommand cmdSehir = new SqlCommand("SELECT SKodu, SAdi FROM Sehir WHERE TurKodu = '" + arrayTKodu[cmbTurlar.SelectedIndex] + "' ORDER BY SAdi", Baglanti);
                cmdSehir.CommandType = CommandType.Text;
                Baglanti.Open();
                SqlDataReader DR = cmdSehir.ExecuteReader(CommandBehavior.CloseConnection);
                int i = new int();
                while (DR.Read())
                {
                    arraySKodu[i] = DR.GetInt16(0);
                    cmbSehir.Items.Add(DR.GetString(1));
                    i++;
                }
            }
            catch (Exception Hata)
            {
                MessageBox.Show(Hata.Message, "Hata", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Baglanti.Close();
            }
        }
        private void FillCmbBoxBolge()
        {
            short SKodu = arraySKodu[cmbSehir.SelectedIndex];
            arrayBKodu = new int[myClass.RecCount("SELECT Count(*) FROM Bolge WHERE SKodu = '" + SKodu.ToString() + "'")];
            SqlConnection Baglanti = myClass.SQLServer();
            cmbBolge.Items.Clear();
            try
            {
                SqlCommand cmdBolge = new SqlCommand("SELECT BKodu, BAdi FROM Bolge WHERE SKodu = " + SKodu.ToString() + " ORDER BY BAdi", Baglanti);
                cmdBolge.CommandType = CommandType.Text;
                Baglanti.Open();
                SqlDataReader DR = cmdBolge.ExecuteReader(CommandBehavior.CloseConnection);
                int i = new int();
                while (DR.Read())
                {
                    arrayBKodu[i] = DR.GetInt32(0);
                    cmbBolge.Items.Add(DR.GetString(1));
                    i++;
                }
            }
            catch (Exception Hata)
            {
                MessageBox.Show(Hata.Message, "Hata", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Baglanti.Close();
            }
        }
        private void FillLstBox()
        {
            int KonumId = GetKonumId(arrayTKodu[cmbTurlar.SelectedIndex], arraySKodu[cmbSehir.SelectedIndex], arrayBKodu[cmbBolge.SelectedIndex]);
            arrayOtelKodu = new int[myClass.RecCount("SELECT Count(*) FROM Oteller WHERE KonumId = " + KonumId.ToString())];
            SqlConnection Baglanti = myClass.SQLServer();
            lstBoxOtel.Items.Clear();
            try
            {
                SqlCommand cmdOtel = new SqlCommand("SELECT OtelKodu, Adi FROM Oteller WHERE KonumId = " + KonumId.ToString() + " ORDER BY Adi", Baglanti);
                cmdOtel.CommandType = CommandType.Text;
                Baglanti.Open();
                SqlDataReader DR = cmdOtel.ExecuteReader(CommandBehavior.CloseConnection);
                int i = new int();
                while (DR.Read())
                {
                    arrayOtelKodu[i] = DR.GetInt32(0);
                    lstBoxOtel.Items.Add((i + 1).ToString() + ". " + DR.GetString(1));
                    i++;
                }
                if (i == 0)
                {
                    lstBoxOtel.Items.Add("");
                    lstBoxOtel.Items.Add("");
                    lstBoxOtel.Items.Add(" *** SE��LEN KONUMDA ***");
                    lstBoxOtel.Items.Add(" *** OTEL BULUNAMADI ***");
                    lstBoxOtel.Enabled = false;
                }
                else
                    lstBoxOtel.Enabled = true;
            }
            catch (Exception Hata)
            {
                MessageBox.Show(Hata.Message, "Hata", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Baglanti.Close();
            }
        }

        private void cmbTurlar_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillCmbBoxSehir();
            cmbSehir.Enabled = true;
            cmbBolge.Items.Clear();
            cmbBolge.Enabled = false;
            lstBoxOtel.Items.Clear();
            DuzenIptal();
        }
        private void cmbSehir_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillCmbBoxBolge();
            cmbBolge.Enabled = true;
            lstBoxOtel.Items.Clear();
            DuzenIptal();
        }
        private void cmbBolge_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillLstBox();
            DuzenIptal();
            lblHata.Text = string.Empty;
            errorProvider1.Dispose();
        }
        #endregion

        #region Form_Load
        private void OtelBilgileri_Load(object sender, EventArgs e)
        {
            cmbSehir.Enabled = false;
            cmbBolge.Enabled = false;
            FillCmbBoxTurlar();
            lblHata.Text = string.Empty;
        }
        #endregion

        #region ListBoxOtel
        private int GetKonumId(string TurKodu, short SKodu, int BKodu)
        {
            SqlConnection Baglanti = myClass.SQLServer();
            SqlCommand cmdOtel = new SqlCommand("SELECT KonumId FROM Konum WHERE TurKodu='" + TurKodu + "' AND SKodu=" + SKodu.ToString() + " AND BKodu=" + BKodu.ToString(), Baglanti);
            cmdOtel.CommandType = CommandType.Text;
            Baglanti.Open();
            SqlDataReader DR = cmdOtel.ExecuteReader(CommandBehavior.CloseConnection);
            DR.Read();
            int KonumId = DR.GetInt32(0);
            Baglanti.Close();
            return KonumId;
        }
        private void lstBoxOtel_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lstBoxOtel.SelectedIndex > -1)
            {
                toolTip1.ToolTipTitle = "Otel Bilgileri";
                toolTip1.SetToolTip(lstBoxOtel, "Ayr�nt�lar i�in �ift t�klay�n�z.");
            }
        }
        private void lstBoxOtel_DoubleClick(object sender, EventArgs e)
        {
            if (lstBoxOtel.SelectedIndex > -1)
            {
                DuzenlenecekOtelSec(arrayOtelKodu[lstBoxOtel.SelectedIndex]);
                tabControlDetay.SelectedIndex = 1;
            }
        }
        #endregion

        #region Otel Bilgileri D�zenleme
        private void DuzenlenecekOtelSec(int gelOtelKodu)
        {
            try
            {
                OtelKodu = gelOtelKodu;
                OtelDuzenle = true;
                DROtelBilgisi = GetOtel(OtelKodu);

                lblFormTitle.Text = DROtelBilgisi.GetString(0);
                lblHit.Text = "�zlenme Say�s�: " + DROtelBilgisi.GetInt32(2).ToString();
                Stream ImageStream = new WebClient().OpenRead(Url + "/Images/Otels/Thumbs/" + DROtelBilgisi.GetString(3));
                pBoxThumb.Image = Image.FromStream(ImageStream);
            }
            catch (WebException)
            {
                //MessageBox.Show("Uzak sunucuya ba�lan�lam�yor!\n\nL�tfen daha sonra tekrar deneyiniz.", "Hata", MessageBoxButtons.OK, MessageBoxIcon.Error);
                pBoxThumb.Image = Image.FromFile("errThumb.jpg");
            }
        }
        private SqlDataReader GetOtel(int OtelKodu)
        {
            SqlConnection Baglanti = myClass.SQLServer();
            SqlCommand cmdOtel = new SqlCommand("SELECT Adi, TesisBilgileri, Hit, Thumb FROM Oteller WHERE OtelKodu = " + OtelKodu.ToString(), Baglanti);
            cmdOtel.CommandType = CommandType.Text;
            Baglanti.Open();
            SqlDataReader DR = cmdOtel.ExecuteReader(CommandBehavior.CloseConnection);
            DR.Read();
            return DR;
        }
        #endregion

        #region D�zenleme Kontrolleri
        private void tabControlOtel_Selecting(object sender, TabControlCancelEventArgs e)
        {
            if (!OtelDuzenle)
            {
                e.Cancel = true;
                MessageBox.Show("Bu i�lem i�in �ncelikle mevcut bir otel se�meli veya yeni bir otel eklemelisiniz!", "Hata", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void tabControlDetay_Selecting(object sender, TabControlCancelEventArgs e)
        {
            if (!OtelDuzenle)
            {
                e.Cancel = true;
                MessageBox.Show("Bu i�lem i�in �ncelikle mevcut bir otel se�melisiniz!", "Hata", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void DuzenIptal()
        {
            lblFormTitle.Text = "otel bilgileri";
            tabControlOtel.SelectedIndex = 0;
            tabControlDetay.SelectedIndex = 0;
            OtelDuzenle = false;
        }
        private void btnDuzenIptal_Click(object sender, EventArgs e)
        {
            DuzenIptal();
        }
        #endregion

        #region Tab Ge�i�leri
        private void tabControlOtel_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (tabControlOtel.SelectedIndex)
            {
                case 0: DuzenlenecekOtelSec(OtelKodu); break;
                case 1: btnTesisBilgileri_Click(sender, e); break;
                case 2: btnKonumBilgisi_Click(sender, e); break;
                case 3: btnThumb_Click(sender, e); break;
                case 4: btnFotos_Click(sender, e); break;
            }
        }
        private void tabControlDetay_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tabControlDetay.SelectedIndex == 0)
            {
                DuzenIptal();
                txtOtelAdi.Text = string.Empty;
                txtOtelAdi.Focus();
                lblHata.Text = string.Empty;
                errorProvider1.Dispose();
            }
        }

        private void btnTesisBilgileri_Click(object sender, EventArgs e)
        {
            lblTesisBilgileri_Info.Text = string.Empty;
            try
            {
                txtTesisBilgileri.Text = DROtelBilgisi.GetString(1).Replace("<br />", Environment.NewLine);
            }
            catch (Exception)
            {
                txtTesisBilgileri.Text = string.Empty;
                txtTesisBilgileri.Focus();
            }
            finally
            {
                tabControlOtel.SelectedIndex = 1;
            }
        }

        private void btnKonumBilgisi_Click(object sender, EventArgs e)
        {
            tabControlOtel.SelectedIndex = 2;
        }

        private void btnThumb_Click(object sender, EventArgs e)
        {
            try
            {
                webBrowser1.Url = new Uri(Url + "/Applications/ChangeThumb.aspx?OtelKodu=" + OtelKodu);
                tabControlOtel.SelectedIndex = 3;
            }
            catch (Exception)
            {
                MessageBox.Show("Uzak sunucuya ba�lan�lam�yor!\n\nL�tfen daha sonra tekrar deneyiniz.", "Hata", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnFotos_Click(object sender, EventArgs e)
        {
            try
            {
                webBrowser2.Url = new Uri(Url + "/Applications/AddPhotos.aspx?OtelKodu=" + OtelKodu);
                tabControlOtel.SelectedIndex = 4;
            }
            catch (Exception)
            {
                MessageBox.Show("Uzak sunucuya ba�lan�lam�yor!\n\nL�tfen daha sonra tekrar deneyiniz.", "Hata", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #endregion

        private void btnOtelEkle_Click(object sender, EventArgs e)
        {
            System.Globalization.CultureInfo culture = new System.Globalization.CultureInfo("en-US");
            errorProvider1.Dispose();
            lblHata.Text = string.Empty;

            if (cmbTurlar.SelectedIndex == -1 || cmbSehir.SelectedIndex == -1 || cmbBolge.SelectedIndex == -1)
            {
                errorProvider1.SetError(lblHata, "Devam etmeden �nce Konum Bilgisini se�melisiniz!");
                lblHata.Text = errorProvider1.GetError(lblHata);
            }
            else if (txtOtelAdi.Text == string.Empty)
            {
                errorProvider1.SetError(lblHata, "Devam etmeden �nce Otel Ad�n� girmelisiniz!");
                lblHata.Text = errorProvider1.GetError(lblHata);
                txtOtelAdi.Focus();
            }
            else if (CheckOtelInListBox(txtOtelAdi.Text.ToUpper()) != -1)
            {
                errorProvider1.SetError(lblHata, "Eklemek istedi�iniz otel, daha �nceden bu konuma kaydedilmi�!");
                lblHata.Text = errorProvider1.GetError(lblHata);
                txtOtelAdi.Focus();
            }
            else
            {
                int OtelID;
                Guid OpnId;
                int KonumId = GetKonumId(arrayTKodu[cmbTurlar.SelectedIndex], arraySKodu[cmbSehir.SelectedIndex], arrayBKodu[cmbBolge.SelectedIndex]);
                OtelID=myClass.ExecScalarQuery("INSERT INTO Oteller (KonumId, Adi, Thumb) VALUES (" + KonumId + ", '" + txtOtelAdi.Text.ToUpper() + "', 'noThumb.jpg')"+" SELECT CAST(scope_identity() AS int)");
                //OpenOtelInsert
                myClass.ExecQuery("INSERT INTO OpenOtel(OtelKodu,BasTarih,BitTarih,HKodu) VALUES(" + OtelID + ",'" + Convert.ToDateTime(DateTime.Now).ToString(culture) + "','" + Convert.ToDateTime(DateTime.Now).ToString(culture) + "','UHD')");
                OpnId=myClass.ExecScalarQueryGuid("SELECT DISTINCT OpenOtel.OpenId FROM OpenOtel WHERE OtelKodu="+OtelID);
                myClass.ExecQuery(@"INSERT INTO OpenOtelDetails(OpenId,tipKodu,fiyat1P,fiyat2P,ilaveYatak) VALUES("+OpnId+",1,0,0,0)");
                //Yeni eklenen otelin listelenmesi
                FillLstBox();
                //D�zenleme Modu
                DuzenlenecekOtelSec(arrayOtelKodu[CheckOtelInListBox(txtOtelAdi.Text.ToUpper())]);
                tabControlDetay.SelectedIndex = 1;
            }
        }

        private int CheckOtelInListBox(string OtelAdi)
        {
            for (int i = 0; i < lstBoxOtel.Items.Count; i++)
                if (lstBoxOtel.Items[i].ToString().IndexOf(OtelAdi) != -1)
                    return i;
            return -1;
        }

        private void btnTesisBilgisiEkle_Click(object sender, EventArgs e)
        {
            try
            {
                string strTesisBilgisi = string.Empty;
                foreach (string satir in txtTesisBilgileri.Lines)
                    strTesisBilgisi += satir + "<br />";

                myClass.ExecQuery("UPDATE Oteller SET TesisBilgileri='" + strTesisBilgisi + "' WHERE OtelKodu=" + OtelKodu);
                lblTesisBilgileri_Info.Text = "De�i�iklikler ba�ar�yla uyguland�.";
            }
            catch (Exception Hata)
            {
                MessageBox.Show(Hata.Message, "Hata", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}