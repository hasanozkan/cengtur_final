using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Microsoft.Win32;

namespace CengTurApp
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            try
            {
                if (Registry.CurrentUser.OpenSubKey("mCayirLi.net").OpenSubKey("CengTur").GetValue("ServerIp").ToString() != null)
                    Application.Run(new Form1());
            }
            catch (Exception)
            {
                Application.Run(new ChangeServer());
            }

        }
    }
}