using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.Sql;
using System.Data.SqlClient;

namespace CengTurApp
{
    public partial class Sehir : Form
    {
        public Sehir()
        {
            InitializeComponent();
        }

        string[] arrayTKodu;

        private void FillCmbBox()
        {
            SqlConnection Baglanti = myClass.SQLServer();
            cmbTurlar.Items.Clear();
            try
            {
                arrayTKodu = new string[myClass.RecCount("SELECT Count(*) FROM Turlar WHERE TurKodu <> '0'")];
                SqlCommand cmdTurlar = new SqlCommand("SELECT TurKodu, TurAdi FROM Turlar WHERE TurKodu <> '0'", Baglanti);
                cmdTurlar.CommandType = CommandType.Text;
                Baglanti.Open();
                SqlDataReader DR = cmdTurlar.ExecuteReader(CommandBehavior.CloseConnection);
                int i = new int();
                while (DR.Read())
                {
                    arrayTKodu[i] = DR.GetString(0);
                    cmbTurlar.Items.Add(DR.GetString(1));
                    i++;
                }
            }
            catch (Exception Hata)
            {
                MessageBox.Show(Hata.Message, "Hata", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Baglanti.Close();
            }
        }

        private void FillLstBox()
        {
            SqlConnection Baglanti = myClass.SQLServer();
            lstBoxSehir.Items.Clear();
            try
            {
                string TurKodu = arrayTKodu[cmbTurlar.SelectedIndex];
                SqlCommand cmdSehir = new SqlCommand("SELECT SAdi FROM Sehir WHERE TurKodu = '" + TurKodu + "' ORDER BY SAdi", Baglanti);
                cmdSehir.CommandType = CommandType.Text;
                Baglanti.Open();
                SqlDataReader DR = cmdSehir.ExecuteReader(CommandBehavior.CloseConnection);
                while (DR.Read())
                    lstBoxSehir.Items.Add(DR.GetString(0));
            }
            catch (Exception Hata)
            {
                MessageBox.Show(Hata.Message, "Hata", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Baglanti.Close();
            }
        }

        private void Sehir_Load(object sender, EventArgs e)
        {
            FillCmbBox();
        }

        private void cmbTurlar_SelectedIndexChanged(object sender, EventArgs e)
        {
            errorProvider1.Dispose();
            FillLstBox();
            txtYeniSehir.Clear();
            txtYeniSehir.BackColor = Color.White;
            txtYeniSehir.Focus();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            errorProvider1.Dispose();
            if (cmbTurlar.SelectedIndex == -1)
                errorProvider1.SetError(cmbTurlar, "�ncelikle Tur Tan�m�n� se�melisiniz!");
            else if (txtYeniSehir.Text == string.Empty)
                errorProvider1.SetError(txtYeniSehir, "Bo� olamaz!");
            else
            {
                try
                {
                    string TurKodu = arrayTKodu[cmbTurlar.SelectedIndex];
                    string TurAdi = cmbTurlar.SelectedItem.ToString();
                    txtYeniSehir.Text = txtYeniSehir.Text.Trim();

                    bool Kontrol = new bool();
                    string[] FromCheckSehir = CheckSehir(txtYeniSehir.Lines, TurKodu, ref Kontrol);

                    if (!Kontrol)
                    {
                        txtYeniSehir.BackColor = Color.White;
                        DialogResult cvp = MessageBox.Show("Toplam " + txtYeniSehir.Lines.Length + " �st b�lge, " + TurAdi + " tan�ml� tura eklenecektir.\n\nDevam etmek istedi�inize emin misiniz?", "Uyar�", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2);
                        if (cvp == DialogResult.Yes)
                        {
                            for (int i = 0; i < txtYeniSehir.Lines.Length; i++)
                            {
                                myClass.ExecQuery("INSERT INTO Sehir (SAdi, TurKodu) VALUES ('" + txtYeniSehir.Lines.GetValue(i).ToString().ToUpper() + "','" + TurKodu + "')");
                                if (checkBox1.Checked)
                                {
                                    string SAdi = txtYeniSehir.Lines.GetValue(i).ToString().ToUpper();
                                    short SKodu = GetSKodu(SAdi, TurKodu);
                                    myClass.ExecQuery("INSERT INTO Bolge (BAdi, SKodu) VALUES ('" + SAdi + "', " + SKodu.ToString() + ")");
                                    myClass.ExecQuery("INSERT INTO Konum (TurKodu, SKodu, BKodu) VALUES ('" + TurKodu + "', " + SKodu.ToString() + ", " + GetBKodu(SAdi, SKodu).ToString() + ")");
                                }
                            }
                            FillLstBox();
                            txtYeniSehir.Clear();
                            checkBox1.Checked = false;
                        }
                    }
                    else
                    {
                        MessageBox.Show("Girdi�iniz bilgilerde daha �nceden " + TurAdi + " tan�ml� tura kaydedilmi� �st b�lge bulundu!\n\n��aretlenmi� bu bilgileri ��kartarak l�tfen tekrar deneyiniz.", "Hata", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        txtYeniSehir.Clear();
                        txtYeniSehir.BackColor = ColorTranslator.FromHtml("#FFDCDC");
                        for (int i = 0; i < FromCheckSehir.Length; i++)
                            txtYeniSehir.Text += FromCheckSehir[i] + Environment.NewLine;
                    }
                }
                catch (Exception Hata)
                {
                    MessageBox.Show(Hata.Message, "Hata", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private string[] CheckSehir(string[] GelenSehirler, string TurKodu, ref bool Result)
        {
            string[] BulunanSehirler = GelenSehirler;
            for (int i = 0; i < GelenSehirler.Length; i++)
            {
                SqlConnection Baglanti = myClass.SQLServer();
                SqlCommand cmdSehir = new SqlCommand("SELECT * FROM Sehir WHERE TurKodu = '" + TurKodu + "' AND SAdi = '" + GelenSehirler[i].ToUpper() + "'", Baglanti);
                cmdSehir.CommandType = CommandType.Text;
                Baglanti.Open();
                SqlDataReader DR = cmdSehir.ExecuteReader(CommandBehavior.CloseConnection);
                if (DR.Read())
                {
                    Result = true;
                    BulunanSehirler[i] = "--> " + GelenSehirler[i];
                }
            }
            return BulunanSehirler;
        }

        private short GetSKodu(string SAdi, string TurKodu)
        {
            SqlConnection Baglanti = myClass.SQLServer();
            SqlCommand cmdSehir = new SqlCommand("SELECT SKodu FROM Sehir WHERE SAdi='" + SAdi + "' AND TurKodu='" + TurKodu + "'", Baglanti);
            cmdSehir.CommandType = CommandType.Text;
            Baglanti.Open();
            SqlDataReader DR = cmdSehir.ExecuteReader(CommandBehavior.CloseConnection);
            DR.Read();
            short SKodu = DR.GetInt16(0);
            Baglanti.Close();
            return SKodu;
        }

        private int GetBKodu(string BAdi, short SKodu)
        {
            SqlConnection Baglanti = myClass.SQLServer();
            SqlCommand cmdBolge = new SqlCommand("SELECT BKodu FROM Bolge WHERE BAdi='" + BAdi + "' AND SKodu = " + SKodu.ToString(), Baglanti);
            cmdBolge.CommandType = CommandType.Text;
            Baglanti.Open();
            SqlDataReader DR = cmdBolge.ExecuteReader(CommandBehavior.CloseConnection);
            DR.Read();
            int BKodu = DR.GetInt32(0);
            Baglanti.Close();
            return BKodu;
        }

        private void btnIptal_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}