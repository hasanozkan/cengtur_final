using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.Sql;
using System.Data.SqlClient;

namespace CengTurApp
{
    public partial class Hizmet : Form
    {
        public Hizmet()
        {
            InitializeComponent();
        }

        private void FillLstBox()
        {
            SqlConnection Baglanti = myClass.SQLServer();
            lstBoxHizmet.Items.Clear();
            try
            {
                SqlCommand cmdHizmet = new SqlCommand("SELECT HKodu, HAdi FROM Hizmet", Baglanti);
                cmdHizmet.CommandType = CommandType.Text;
                Baglanti.Open();
                SqlDataReader DR = cmdHizmet.ExecuteReader(CommandBehavior.CloseConnection);
                while (DR.Read())
                    lstBoxHizmet.Items.Add(DR.GetString(0) + DR.GetString(1));
            }
            catch (Exception Hata)
            {
                MessageBox.Show(Hata.Message, "Hata", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Baglanti.Close();
            }
        }

        private void Hizmet_Load(object sender, EventArgs e)
        {
            FillLstBox();
        }

        private void lstBoxHizmet_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                if (lstBoxHizmet.SelectedIndex != -1)
                {
                    string HKodu = lstBoxHizmet.SelectedItem.ToString().Substring(0, 10).Trim();
                    string HAdi = lstBoxHizmet.SelectedItem.ToString().Remove(0, 10);
                    toolTip1.SetToolTip(lstBoxHizmet, HAdi);
                    lstBoxHizmet.ContextMenuStrip = contextMenuStrip1;
                }
            }
        }

        bool Degisitir = new bool();
        private void lstBoxHizmet_DoubleClick(object sender, EventArgs e)
        {
            if (lstBoxHizmet.SelectedIndex != -1)
            {
                errorProvider1.Dispose();
                Degisitir = true;
                groupBox2.Text = "De�i�tir";
                txtHKodu.Enabled = false;
                txtHKodu.Text = lstBoxHizmet.SelectedItem.ToString().Substring(0, 10).Trim();
                txtHAdi.Text = lstBoxHizmet.SelectedItem.ToString().Remove(0, 10);
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (Degisitir)
                {
                    errorProvider1.Dispose();
                    if (txtHAdi.Text == "")
                        errorProvider1.SetError(txtHAdi, "Bo� olamaz!");
                    else
                    {
                        string HAdi = lstBoxHizmet.SelectedItem.ToString().Remove(0, 10);
                        DialogResult cvp = MessageBox.Show(HAdi + " olan hizmet t�r�;\n" + txtHAdi.Text.ToUpper() + " olarak de�i�tirilecektir.\n\nDevam etmek istedi�inize emin misiniz?", "Uyar�", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2);
                        if (cvp == DialogResult.Yes)
                        {
                            myClass.ExecQuery("UPDATE Hizmet SET HAdi='" + txtHAdi.Text.ToUpper() + "' WHERE HKodu='" + txtHKodu.Text + "'");
                            Degisitir = false;
                            groupBox2.Text = "Ekle";
                            txtHKodu.Enabled = true;
                            txtHKodu.Text = string.Empty;
                            txtHAdi.Text = string.Empty;
                            FillLstBox();
                        }
                    }
                }
                else
                {
                    errorProvider1.Dispose();
                    if (txtHKodu.Text == "")
                        errorProvider1.SetError(txtHKodu, "Bo� olamaz!");
                    else if (txtHAdi.Text == "")
                        errorProvider1.SetError(txtHAdi, "Bo� olamaz!");
                    else
                    {
                        DialogResult cvp = MessageBox.Show("A�a��daki hizmet t�r� veri taban�na eklenecektir.\n" + txtHKodu.Text.ToUpper() + " - " + txtHAdi.Text.ToUpper() + "\n\nDevam etmek istedi�inize emin misiniz?", "Uyar�", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2);
                        if (cvp == DialogResult.Yes)
                        {
                            myClass.ExecQuery("INSERT INTO Hizmet VALUES ('" + txtHKodu.Text.ToUpper() + "','" + txtHAdi.Text.ToUpper() + "')");
                            txtHKodu.Text = string.Empty;
                            txtHAdi.Text = string.Empty;
                            FillLstBox();
                        }
                    }
                }
            }
            catch (Exception Hata)
            {
                MessageBox.Show(Hata.Message, "Hata", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void SilToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (lstBoxHizmet.SelectedIndex != -1)
            {
                string HKodu = lstBoxHizmet.SelectedItem.ToString().Substring(0, 10).Trim();
                string HAdi = lstBoxHizmet.SelectedItem.ToString().Remove(0, 10);

                DialogResult cvp = MessageBox.Show("A�a��daki hizmet t�r� kal�c� olarak silinecektir.\n" + HKodu + " - " + HAdi + "\n\nDevam etmek istedi�inize emin misiniz?", "Uyar�", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2);
                if (cvp == DialogResult.Yes)
                {
                    myClass.ExecQuery("DELETE FROM Hizmet WHERE HKodu='" + HKodu + "'");
                    FillLstBox();
                }
            }
        }

        private void EkleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Degisitir = false;
            groupBox2.Text = "Ekle";
            txtHKodu.Enabled = true;
            txtHKodu.Text = string.Empty;
            txtHAdi.Text = string.Empty;
            txtHKodu.Focus();
        }

        private void btnIptal_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}