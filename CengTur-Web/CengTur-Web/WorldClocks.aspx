﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="WorldClocks.aspx.cs" Inherits="Default2" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpDetail" Runat="Server">
 <script src="Scripts/remote_clock.js" type="text/javascript"></script>

 <script language="javascript">
     function FirstView() {
         document.getElementById('mytd').innerHTML = ShowTime('Istanbul', 3);
     }

     function Goster() {
         try {
             
         document.getElementById('Preloader').className = "goster";
         var secim = document.getElementById('ctl00$cpDetail$ListBox1').selectedIndex;
         if (secim == -1) {
             alert('Şehir Seçiniz !');
         }
         else {
             var sehir = document.getElementById('ctl00$cpDetail$ListBox1').options[secim].text;
             var add = document.getElementById('ctl00$cpDetail$ListBox1').options[secim].value;
             document.getElementById('mytd').innerHTML = ShowTime(sehir, add);
             document.getElementById('Preloader').className = "gizle";
         }
         }
         catch(Error) {
             document.getElementById('ErrorPanel').className = "goster";
         }
         finally
         {
         
         }
     }
    
 </script>
<table border="0" cellpadding="0" cellspacing="0" width="510">
<tr class="BaslikSatiri">
		<td align="right" 
            style="height: 40px; padding-right:10px; background-image:url('Images/Tables/title_bar_turuncu_big.jpg');">
            <span style="font-size: 20px;">Dünya Saatleri</span>
		</td>
	</tr>
	</table>
        <fieldset class="tah11" 
        
        style=" border-top: 2px solid #FF6600; padding: 5px 0px 15px 10px; width: 500px; border-left-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-color: #FF6600; border-right-color: #FF6600; border-bottom-color: #FF6600;">
			<legend><img src="Images/Legend/Clock.png" style="border: none;" alt="" /></legend>
            <table style="background-image: url('Images/Tables/bg_box_noeffect_90x509.gif');";>
    <tr>
    <td style="width: 344px">
    <table>
    <tr>
    <td style="text-align: center">
    <h5> Şehir Seçiniz : </h5>
     <asp:ListBox ID="ListBox1" runat="server" Height="207px" Width="196px" 
            CssClass="textBox" DataSourceID="SqlDataSourceSaatFarklari" 
            DataTextField="Ulke" DataValueField="SaatFarki">
        </asp:ListBox>
     
        <cc1:ListSearchExtender ID="ListBox1_ListSearchExtender" runat="server" 
            Enabled="True" PromptText="Arama için kelime yazınız..." 
            TargetControlID="ListBox1">
        </cc1:ListSearchExtender>
     
        <br> 
        <input id="Button1" type="button" value="Saati Göster" onclick="Goster()" class="button" />    
    </td>
    <td style="width: 206px; text-align: center; vertical-align:middle">
        <div id="mytd" style="width: 283px" align="center">
        </div>
        <div id="Preloader" style="width: 305px" class="gizle">
            <img alt="" src="Images/Ajax/ajax_loader_32.gif" /> Yükleniyor...
        </div>
          <div id="ErrorPanel" style="width: 305px" class="gizle">
            Yüklenirken hata oluştu...
        </div>
        <script language="javascript">
            FirstView();
        </script>
    </td>
    
    </tr>
    </table>
    </tr>
	</table>
	     
            </asp:Table>
            </fieldset>                
	  <asp:SqlDataSource ID="SqlDataSourceSaatFarklari" runat="server" 
            ConnectionString="<%$ ConnectionStrings:CengTurConnectionString %>" 
            
        SelectCommand="SELECT [Id], [Ulke], [SaatFarki] FROM [SaatFarklari] ORDER BY [Ulke]">
        </asp:SqlDataSource>
</asp:Content>


