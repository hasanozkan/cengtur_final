#region Using Tan�mlamalar�
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
#endregion

public partial class ReserveCustomerInfo : System.Web.UI.Page
{
   
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            lblCekilecekTutar.Text = Session["ToplamUcret"].ToString() + " TL";
            lblToplam.Text = Session["ToplamUcret"].ToString();
            for (int i = DateTime.Now.Year; i <= DateTime.Now.Year + 10; i++)
            {
                ddlYil.Items.Add(i.ToString());
            }
            imgcc.ImageUrl = Session["ccSecilenImage"].ToString();
            if (Convert.ToInt32(Session["SecilenTaksit"])==0)
            {
                lblToplam.Text = Session["Price"].ToString();
                lblOdemeTuru.Text = "Tek �ekim";
                lblCekilecekTutar.Text = Session["Price"].ToString() + " TL";
            }
            else if (Convert.ToInt32(Session["SecilenTaksit"])==1)
            {
                lblToplam.Text = Session["Taksit1Price"].ToString();
                lblOdemeTuru.Text = Session["Taksit1Text"].ToString();
                lblCekilecekTutar.Text = Session["Taksit1Price"].ToString()+"TL"+ " - " + Session["Taksit1Text"].ToString();
            }
            else if (Convert.ToInt32(Session["SecilenTaksit"]) == 2)
            {
                lblToplam.Text = Session["Taksit2Price"].ToString();
                lblOdemeTuru.Text = Session["Taksit2Text"].ToString();
                lblCekilecekTutar.Text = Session["Taksit2Price"].ToString() + "TL" + " - " + Session["Taksit2Text"].ToString();
            }
        }
    }
    private void AddPersonsToDBase()
    {
        int RezervNo = 0;

        string TCKNo, Ad, Soyad, DTarihi;
        bool Odeme = false;
        byte KisiTur = 0, Cinsiyet = 0;
        DataTable Persons = (DataTable)Session["KisiBilgileri"];

        #region Reserve Ekle
        //Rezervasyon Ekleme
        System.Globalization.CultureInfo culture = new System.Globalization.CultureInfo("en-US");

        

        string InsertRezerve = "INSERT INTO Reserve(oodId,TelNo,ePosta,GirisTarihi,CikisTarihi)"
        + "VALUES("+11+",'" + (Session["TelNo"].ToString()) + "','" + (Session["ePosta"].ToString()) + "','" + Convert.ToDateTime(Session["GirisTarihi"]).ToString(culture) + "','" + Convert.ToDateTime(Session["CikisTarihi"]).ToString(culture) + "') "
        + " SELECT CAST(scope_identity() AS int)";
        RezervNo = CengTur.ExecScalarQuery(InsertRezerve);
        #endregion

        for (int i = 0; i < Persons.Rows.Count; i++)
        {

            TCKNo = (Persons.Rows[i][0].ToString());
            Ad = (Persons.Rows[i][1].ToString());
            Soyad = (Persons.Rows[i][2].ToString());
            DTarihi =(Persons.Rows[i][3].ToString());
            Cinsiyet = Convert.ToByte(Persons.Rows[i][4].ToString());
            KisiTur = Convert.ToByte(Persons.Rows[i][5].ToString());
            Odeme = Convert.ToBoolean(Persons.Rows[i][6].ToString());
            #region KisiEkleme
            string InsertKisiler = "INSERT INTO ReserveKisi VALUES (" + RezervNo + ",'" + TCKNo + "','" + Ad + "',"
                                 + "'" + Soyad + "','" + Convert.ToDateTime(DTarihi).ToString(culture) + "',"
                                 + Cinsiyet + "," + KisiTur + ")";
            CengTur.ExecQuery(InsertKisiler);
            #endregion

            if (Odeme)
            {
                #region Reserve Ekle
                string UpdateReserve = "UPDATE Reserve SET TCKimlikNo='" + TCKNo + "' WHERE ReserveId=" + RezervNo;
                CengTur.ExecQuery(UpdateReserve);
                #endregion
            }
            //Odeme Bilgileri Kaydedilecek...
        }

    }
    protected void imgBtnContinue_Click(object sender, ImageClickEventArgs e)
    {
        bool IsCardInfoTrue=true;
        if (IsCardInfoTrue)
        {
            AddPersonsToDBase();
           
            Response.Redirect("~/ReserveComplete.aspx");
        }
        
        
        
    }
}
