#region Using Tan�mlamalar�
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.Sql;
using System.Data.SqlClient;
#endregion

public partial class _Detail : System.Web.UI.Page
{
    public SqlDataReader DR_HotelDetail;
    string OpenId;

    protected void Page_Load(object sender, EventArgs e)
    {
        //try
        //{
            OpenId = Request.QueryString["OpenId"];
            DR_HotelDetail = CengTur.HotelDetail(OpenId);
            DR_HotelDetail.Read();

            CengTur.ExecQuery("UPDATE Oteller SET Hit=Hit+1 WHERE OtelKodu=" + DR_HotelDetail.GetInt32(8));

            lblTurAdi.Text = DR_HotelDetail.GetString(1);
            lblSehir.Text = DR_HotelDetail.GetString(2);
            lblBolge.Text = DR_HotelDetail.GetString(3);
            lblAdi.Text = DR_HotelDetail.GetString(0);
            SlideShowOtel.ImagesDirectory = "~/Images/Otels/Bigs/" + DR_HotelDetail.GetInt32(8).ToString();
            lblHit.Text = DR_HotelDetail.GetInt32(9).ToString();
            lblTesisBilgileri.Text = DR_HotelDetail.GetString(7);
            lblTarihAraligi.Text = DR_HotelDetail.GetDateTime(4).ToLongDateString() + " - " + DR_HotelDetail.GetDateTime(5).ToLongDateString();
            lblHAdi.Text = DR_HotelDetail.GetString(6);
            lblGirisFiyati.Text = CengTur.OtelEnDusukFiyat(OpenId) + " YTL.'den itibaren,<br />";
            lblGirisDetay.Text = "(1 Gecelik, 2 ki�ilik odada ki�iba�� konaklama fiyat�d�r)";
            
            createSession();
            imgBtnDetail.PostBackUrl = "~/Reserve.aspx";
        //}
        //catch (Exception)
        //{
        //    Response.Redirect("");
        //}
        //finally
        //{
        //    DR_HotelDetail.Close();
        //}
    }
    private void createSession()
    {
        Session["OpenId"] = OpenId;
        Session["Adi"] = lblAdi.Text;
        Session["TurAdi"] = lblTurAdi.Text;
        Session["Sehir"] = lblSehir.Text;
        Session["Bolge"] = lblBolge.Text;
    }
}
