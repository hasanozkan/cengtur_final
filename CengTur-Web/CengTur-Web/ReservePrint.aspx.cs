﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class ReservePrint : System.Web.UI.Page
{
    public string Adres = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        lblOtelAdi.Text = Session["Adi"].ToString();
        lblOdaTipi.Text = Session["OdaTipAdi"].ToString();
        lblKonaklamaTipi.Text = Session["HizmetAdi"].ToString();

        if (Session["cocukSay"].ToString() == "0")
            lblKisiSay.Text = Session["yetiskinSay"].ToString() + " yetişkin";
        else
            lblKisiSay.Text = Session["yetiskinSay"].ToString() + " yetişkin + " + Session["cocukSay"].ToString() + " çocuk";

        lblKonum.Text = Session["Sehir"].ToString() + " / " + Session["Bolge"].ToString();
        lblGirisTarihi.Text = Session["GirisTarihi"].ToString();
        lblCikisTarihi.Text = Session["CikisTarihi"].ToString();
        lblFiyat.Text = Session["ToplamUcret"].ToString() + " YTL";
        lblTelefon.Text = Session["TelNo"].ToString();
        lblEPosta.Text = Session["ePosta"].ToString();
        try
        {
            Adres = Session["OtelAdres"].ToString();
        }
        catch (Exception)
        {
            
            
        }
        //Kişi bilgilerini doldur...
        DataTable Persons = (DataTable)Session["KisiBilgileri"];
        for (int i = 0; i < Persons.Rows.Count; i++)
        {
            //Tablo işlemleri          
            TableRow satir = new TableRow();
            TableCell hucre = new TableCell();

            Label lblKisi = new Label();
            lblKisi.Text = (i + 1).ToString() + ".Kişi";
            

            Label lblTCKNoKisi = new Label();
            lblTCKNoKisi.Text = Persons.Rows[i][0].ToString();

            Label lblAdiKisi = new Label();
            lblAdiKisi.Text = Persons.Rows[i][1].ToString();

            Label lblSoyadiKisi = new Label();
            lblSoyadiKisi.Text = Persons.Rows[i][2].ToString();

            Label lblCinsiyetKisi = new Label();
            if ((Convert.ToByte(Persons.Rows[i][4].ToString())) == 1)
            {
                lblCinsiyetKisi.Text = "E";
            }
            else
            {
                lblCinsiyetKisi.Text = "K";
            }


            Label lblDTarihiKisi = new Label();
            lblDTarihiKisi.Text = Persons.Rows[i][3].ToString();

            Label lblOdemeKisi = new Label();
            if (Convert.ToBoolean(Persons.Rows[i][6].ToString()))
            {
                lblOdemeKisi.Text = "*";
            }
            else
            {
                lblOdemeKisi.Text = "";
            }
            
            hucre.Controls.Add(lblKisi);
            hucre.CssClass = "Tablom";
            satir.Cells.Add(hucre);

            hucre = new TableCell();
            hucre.Controls.Add(lblTCKNoKisi);
            hucre.CssClass = "Tablom";
            
            satir.Cells.Add(hucre);
            

            hucre = new TableCell();
            hucre.Controls.Add(lblAdiKisi);
            hucre.CssClass = "Tablom";
            satir.Cells.Add(hucre);

            hucre = new TableCell();
            hucre.Controls.Add(lblSoyadiKisi);
            hucre.CssClass = "Tablom";
            satir.Cells.Add(hucre);

            hucre = new TableCell();
            hucre.Controls.Add(lblCinsiyetKisi);
            hucre.CssClass = "Tablom";
            satir.Cells.Add(hucre);
            

            hucre = new TableCell();
            hucre.Controls.Add(lblDTarihiKisi);
            hucre.CssClass = "Tablom";
            satir.Cells.Add(hucre);

            hucre = new TableCell();

            hucre.Controls.Add(lblOdemeKisi);
            hucre.CssClass = "Tablom";
            satir.Cells.Add(hucre);

            TblPersonal.Rows.Add(satir);
        }

    }
}
