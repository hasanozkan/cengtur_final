<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ForeignConsulate.aspx.cs" Inherits="Default2" %>

<%@ Register Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI.WebControls" TagPrefix="asp" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpDetail" Runat="Server">
    <table border="0" cellpadding="0" cellspacing="0" width="510">
<tr class="BaslikSatiri">
		<td align="right" 
            style="height: 40px; padding-right:10px; background-image:url('Images/Tables/title_bar_turuncu_big.jpg');">
            <span style="font-size: 20px;">Yurt D��� Konsolosluklar</span>
		</td>
	</tr>
	</table>
        <fieldset class="tah11"     
        style=" border-top: 2px solid #FF6600; padding: 5px 0px 15px 10px; width: 500px; border-left-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-color: #FF6600; border-right-color: #FF6600; border-bottom-color: #FF6600;">
			<legend><img src="Images/Legend/Users.png" style="border: none;" alt="" /></legend>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>  
                    
    <table style=" width: 500px;">
    <tr>
    <td style="width: 344px">
    <table>
    <tr>
    <td style="text-align: center; width: 200px;"><h5> �lke Se�iniz : </h5></td>
    <td width="100">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
    <td width="140">
        &nbsp;</td>
    </tr>
    <tr>
    <td style="text-align: center; width: 190px;" valign="top">
    <a name="Center"></a>
       <asp:ListBox ID="ListBox1" runat="server" Height="207px" Width="196px" 
            CssClass="textBox" DataSourceID="SqlDataSourceKonsolosluklar" 
            DataTextField="UlkeAdi" DataValueField="UlkeId">
        </asp:ListBox>
        <cc1:ListSearchExtender ID="ListBox1_ListSearchExtender" runat="server" 
            Enabled="True" PromptText="Arama i�in kelime yaz�n�z..." 
            TargetControlID="ListBox1">
        </cc1:ListSearchExtender>
        <br />
        <asp:Button ID="btnGoster" runat="server" Text="Bilgileri G�ster" CssClass="button"/>
    </td>
    <td width="100">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
    <td valign="top" align="center" style="text-align: center">
      <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                <ProgressTemplate>
                <img src="Images/Ajax/ajax_loader_32.gif" />
                Y�kleniyor...
                </ProgressTemplate>
                </asp:UpdateProgress>  
        <asp:Repeater ID="Repeater1" runat="server" 
            DataSourceID="SqlDataSourceKonsolosluklarDetail">
        <ItemTemplate>
    <table width="240" class="Tah11" style="background-image:url('Images/Tables/SnippetStart.png'); 
                                         background-position: left top; background-repeat:no-repeat;
                                         margin-bottom: 10px;">
    <tr>
        <td style="padding: 15px 15px 0px 10px;">
            <b><%#DataBinder.Eval(Container.DataItem,"KonumAdi") %></b><br />
            <hr style="width: 99%; border: solid 1px #EBEBEB;" align="left" />
        </td>
    </tr>
    <tr>
        <td style="padding: 0px 15px 0px 10px;">
            <table id="ctl00_ContentPlaceHolder_Left_LeftFrameDefault1_Links1_LinkTable" class="Tah11 LinkTable" border="0" style="width:100%;">
	<tr>
	<td width="17"><img src="Images/Master/Personnel.png" style="border-width:0px;" /></td><td  align="left"><%#DataBinder.Eval(Container.DataItem,"UlkeAdi") %></td>
	</tr>
	<tr>
		<td width="17"><img src="Images/Master/telephone.png" style="border-width:0px;" /></td><td  align="left"><%#DataBinder.Eval(Container.DataItem,"TelNo") %></td>
	</tr><tr>
		<td width="17"><img src="Images/Master/address.png" style="border-width:0px;" /></td><td align="left"><%#DataBinder.Eval(Container.DataItem,"Adres") %></td>
	</tr>
</table>
    </td>
    </tr>
    <tr>
    <td><br /><br /></td>
    </tr>
</table>
</ItemTemplate>
    </asp:Repeater>
    </td>
    </tr>
    </table>
    </tr>
	</table>
	        </ContentTemplate>
           </asp:UpdatePanel>
            </fieldset>                
	  <asp:SqlDataSource ID="SqlDataSourceKonsolosluklarDetail" runat="server" 
                ConnectionString="<%$ ConnectionStrings:CengTurConnectionString %>" 
                
    
        
        
        
        SelectCommand="SELECT KKodu, KonumAdi, TelNo, Adres, FlagUrl, Ulke, YDKonsoloslukUlkeler.UlkeAdi FROM YDKonsolosluklar, YDKonsoloslukUlkeler WHERE (YDKonsolosluklar.Ulke = @Ulke) AND ( YDKonsolosluklar.Ulke=YDKonsoloslukUlkeler.UlkeId)ORDER BY YDKonsolosluklar.KonumAdi">
                <SelectParameters>
                    <asp:ControlParameter ControlID="ListBox1" Name="Ulke" 
                        PropertyName="SelectedValue" Type="Int32" />
                </SelectParameters>
            </asp:SqlDataSource>
            <asp:SqlDataSource ID="SqlDataSourceKonsolosluklar" runat="server" 
            ConnectionString="<%$ ConnectionStrings:CengTurConnectionString %>" 
            
    
        SelectCommand="SELECT [UlkeId], [UlkeAdi] FROM [YDKonsoloslukUlkeler] ORDER BY [UlkeAdi]">
        </asp:SqlDataSource>
</asp:Content>

