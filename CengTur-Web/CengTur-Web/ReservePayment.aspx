<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ReservePayment.aspx.cs" Inherits="ReserveCustomerInfo" %>
<%@ Register assembly="eWorld.UI, Version=2.0.6.2393, Culture=neutral, PublicKeyToken=24d65337282035f2" namespace="eWorld.UI" tagprefix="ew" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpDetail" Runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
<table border="0" cellpadding="0" cellspacing="0" width="510px">
	<tr class="BaslikSatiri">
		<td colspan="2" align="right" style="height: 40px; padding-right:10px; background-image:url('Images/Tables/title_bar_turuncu_big.jpg');">
            <span style="font-size: 20px;">online sat��</span>
		</td>
	</tr>

    <tr style="height: 5px;"><td colspan="2"></td></tr>
    <tr class="BaslikSatiri" style="background-image:url('Images/Tables/title_bg_red.jpg');">
		<td colspan="2">
            �deme ��lemi
		</td>
	</tr>
	<tr>
	<td colspan="2"><br /></td>
	
	</tr>
	
	<tr class="tdDetail" style="background-image: url('Images/Tables/bg_box_noeffect_90x509.gif');";>
	
	<td valign="top" align="center" style="width: 99px">
	<table>
	<tr><td style="width: 100%"><br /><br /></td></tr>
	
	<tr>
	<td>
    <asp:Image ID="imgcc" runat="server" ImageUrl="~/Images/CreditCards/maximum.gif" 
            BorderColor="#FF6600" BorderStyle="Solid" BorderWidth="2px" Height="45px" 
            Width="108px" />
	</td>
	</tr>
	<tr>
	<td style="color: #000000">
        <asp:Label ID="lblToplam" runat="server" Text="250" Font-Size="X-Large" 
            ForeColor="#CC0000"></asp:Label><span>&nbsp;<b>TL</b></span>
            <hr style="color: #000000" />
        <asp:Label ID="lblOdemeTuru" runat="server" Text="6 x 70,00 TL TAKS�T" 
            Font-Size="8pt"></asp:Label>
	</td>
	</tr>
	</table>
	</td>
	
		<td colspan="2" align="left" style="margin-left: 40px; width: 300px;">
		<fieldset class="Tah11" style=" padding:5px 0px 15px 10px; width: 100%; border-color: #FF6600; border-top-style: solid;
			border-left-width: 0px; border-right-width: 0px; border-top-width: 2px; border-bottom-width: 1px">
			<legend><img src="Images/Legend/Users.png" style="border: none;" alt="" /></legend>
		<table>
<tr><td align="left">Kart Sahibinin Ad�:</td><td style="width: 250px">
    <asp:TextBox ID="txtKSAdi" runat="server" 
        Width="160" style="text-align: left"></asp:TextBox>
      <br />
    <asp:RequiredFieldValidator ID="RFVtxtKSAdi" runat="server" 
        ErrorMessage="* Bu alan� bo� b�rakmay�n�z!" ControlToValidate="txtKSAdi" 
        EnableViewState="False" SetFocusOnError="True" Display="Dynamic"></asp:RequiredFieldValidator>
    </td></tr>
    <tr><td align="left" style="width: 1135px">Kart No:</td><td>
        
        <ew:MaskedTextBox ID="txtKartNo" runat="server" Width="160px" style="text-align: left" MaxLength="16" Mask="9999999999999999" />
        <asp:RequiredFieldValidator ID="RFVtxtKartNo" runat="server" EnableViewState="False"
            ErrorMessage="* Bu alan� bo� b�rakmay�n�z!" ControlToValidate="txtKartNo" 
            SetFocusOnError="True" Display="Dynamic"></asp:RequiredFieldValidator>
   
        <asp:RegularExpressionValidator ID="REVtxtKartNo" runat="server" 
            
            ErrorMessage="* Kart numaras� 16 haneli olmal� ve numerik de�erler i�ermelidir!" 
            ControlToValidate="txtKartNo" ValidationExpression="[0-9]{16}" 
            EnableViewState="False" SetFocusOnError="True" Display="Dynamic"></asp:RegularExpressionValidator>
     
        </td></tr>
    <tr><td align="left" style="width: 1135px">G�venlik Kodu:</td><td align="left">
        
                    <ew:MaskedTextBox ID="txtGuvenlik" runat="server" Width="30" 
            style="text-align: left" MaxLength="3" Mask="999" />
                    <br />
        <asp:RequiredFieldValidator ID="RFVtxtGuvenlik" runat="server" 
            ErrorMessage="* Bu alan� bo� b�rakmay�n�z!" 
            ControlToValidate="txtGuvenlik" EnableViewState="False" 
            EnableTheming="False" Display="Dynamic"></asp:RequiredFieldValidator>
   
        <asp:RegularExpressionValidator ID="REVtxtGuvenlik" runat="server" 
            
            ErrorMessage="* G�venlik kodu 3 haneli olmal� ve numerik de�erler i�ermelidir!" 
            ControlToValidate="txtGuvenlik" ValidationExpression="[0-9]{3}" 
            EnableViewState="False" SetFocusOnError="True" Display="Dynamic"></asp:RegularExpressionValidator>
            </td></tr>
    <tr><td align="left" style="width: 1135px">Son Kullanma Tarihi(ay/y�l):</td><td align="left">
    
        <asp:DropDownList ID="ddlAy" runat="server">
            <asp:ListItem>01</asp:ListItem>
            <asp:ListItem>02</asp:ListItem>
            <asp:ListItem>03</asp:ListItem>
            <asp:ListItem>04</asp:ListItem>
            <asp:ListItem>05</asp:ListItem>
            <asp:ListItem>06</asp:ListItem>
            <asp:ListItem>07</asp:ListItem>
            <asp:ListItem>08</asp:ListItem>
            <asp:ListItem>09</asp:ListItem>
            <asp:ListItem>10</asp:ListItem>
            <asp:ListItem>11</asp:ListItem>
            <asp:ListItem>12</asp:ListItem>
        </asp:DropDownList>
        /<asp:DropDownList ID="ddlYil" runat="server">
        </asp:DropDownList>
    </td></tr>
    <tr><td style="text-align: left;">�ekilecek Tutar:</td><td style="text-align: left">
        <asp:Label ID="lblCekilecekTutar" runat="server" CssClass="Baslik" Text=""></asp:Label>
        </td></tr>
</table>
</fieldset>
	          </td>
	          </tr>
	          <tr class="tdDetail" style="padding-top: 15px;">
		<td valign="top" style="width: 99px">
            <asp:CustomValidator ID="cvReserve" runat="server" Font-Bold="true"></asp:CustomValidator>
		</td>
		<td align="right" valign="top">
		<br />
		    <asp:ImageButton ID="imgBtnContinue" runat="server" 
                ImageUrl="~/Images/Buttons/tamamla_button.gif" 
                onclick="imgBtnContinue_Click" />
		</td>
    </tr>
	</table>
	
    </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>


