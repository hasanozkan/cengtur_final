<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="SearchResult.aspx.cs" Inherits="_SearchResult" Debug="true" %>
<asp:Content ID="cpDetail" ContentPlaceHolderID="cpDetail" Runat="Server">

<table border="0" cellpadding="0" cellspacing="0" width="510">
	<tr class="BaslikSatiri" style="background-image:url('Images/Tables/title_bg_red.jpg');">
		<td colspan="4">
            <asp:Label ID="lblTurAdi" runat="server" Text="lblTurAdi"></asp:Label> / 
            <asp:Label ID="lblSehir" runat="server" Text="lblSehir"></asp:Label> /
            <asp:Label ID="lblBolge" runat="server" Text="lblBolge"></asp:Label>
		</td>
	</tr>
	<tr class="BaslikSatiri" style="background-image:url('Images/Tables/title_bg_mavi.jpg');">
		<td colspan="4">
            EN �OK TERC�H ED�LEN OTELLER
		</td>
	</tr>
	<% 
    int ThumbCount = 0;
    while (DR_HitHotel.Read()) {
        ThumbCount++;
        if (ThumbCount > 2) break;//6 Otelin detayl� bilgisi g�sterilecek (3*2)
    %>
    <tr>
        <td colspan="2">
            <table border="0" cellpadding="0" cellspacing="0" class="TableData" style="width:255px; height:128px; background-image: url('Images/Tables/bg_box.gif');">
                <tr>
                    <td rowspan="5" style="width: 105px;" valign="middle">
                        <img src="Images/Otels/Thumbs/<% =DR_HitHotel.GetString(4) %>" alt="" class="imgThumb" />
                    </td>
                    <td style="height: 20px; vertical-align: bottom;">
                        <b style="color: #FF6600;"><% =DR_HitHotel.GetString(0) %></b>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b><% =DR_HitHotel.GetString(2) %> / <% =DR_HitHotel.GetString(3) %></b>
                    </td>                
                </tr>
                <tr>
                    <td>
                        <% =DR_HitHotel.GetString(7) %>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span style="color: #F03307;"><% =CengTur.OtelEnDusukFiyat(DR_HitHotel.GetGuid(8).ToString()) %> YTL.</span>'den itibaren
                    </td>
                </tr>
                <tr>
                    <td>
                        (<% =DR_HitHotel.GetDateTime(5).ToShortDateString() %>-<% =DR_HitHotel.GetDateTime(6).ToShortDateString()%> aras�nda)
                        <a href="Detail.aspx?OpenId=<% =DR_HitHotel.GetGuid(8) %>">Detaylar >></a>
                    </td>
                </tr>
            </table>
		</td>
		<td colspan="2">
        <% if(DR_HitHotel.Read()) { %>
            <table border="0" cellpadding="0" cellspacing="0" class="TableData" style="width:255px; height:128px; background-image: url('Images/Tables/bg_box.gif');">
                <tr>
                    <td rowspan="5" style="width: 105px;" valign="middle">
                        <img src="Images/Otels/Thumbs/<% =DR_HitHotel.GetString(4) %>" alt="" class="imgThumb" />
                    </td>
                    <td style="height: 20px; vertical-align: bottom;">
                        <b style="color: #FF6600;"><% =DR_HitHotel.GetString(0) %></b>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b><% =DR_HitHotel.GetString(2) %> / <% =DR_HitHotel.GetString(3) %></b>
                    </td>                
                </tr>
                <tr>
                    <td>
                        <% =DR_HitHotel.GetString(7) %>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span style="color: #F03307;"><% =CengTur.OtelEnDusukFiyat(DR_HitHotel.GetGuid(8).ToString()) %> YTL.</span>'den itibaren
                    </td>
                </tr>
                <tr>
                    <td>
                        (<% =DR_HitHotel.GetDateTime(5).ToShortDateString() %>-<% =DR_HitHotel.GetDateTime(6).ToShortDateString()%> aras�nda)
                        <a href="Detail.aspx?OpenId=<% =DR_HitHotel.GetGuid(8) %>">Detaylar >></a>
                    </td>
                </tr>
            </table>
        <% } %>    
		</td>
	<% } %>
	</tr>

	<tr style="height: 20px;">
		<td colspan="4"></td>
	</tr>	

	<tr class="BaslikSatiri" style="background-image:url('Images/Tables/title_bg_yesil.jpg');">
		<td colspan="4">
            D��ER OTELLER
		</td>
	</tr>
	<tr class="BaslikSatiri" style="height: 15px; background-color: #FF6600;">
		<td style="width: 25%;">Otel Ad�</td>
		<td style="width: 25%;">�ehir / B�lge</td>
		<td style="width: 25%;">Konaklama</td>
		<td style="width: 25%;">Fiyat</td>
	</tr>
	<% 
    GetResults();
    ThumbCount = 0;   
    while (DR_HitHotel.Read()) {   
        ThumbCount++;
    %>
	<tr class="TableData" style="background-color: #FFFFF5;">
		<td><a href="Detail.aspx?OpenId=<% =DR_HitHotel.GetGuid(8) %>"><% =DR_HitHotel.GetString(0) %></a></td>
		<td><% =DR_HitHotel.GetString(2) %> / <% =DR_HitHotel.GetString(3) %></td>
		<td><% =DR_HitHotel.GetString(7) %></td>
		<td><span style="color: #F03307;"><% =CengTur.OtelEnDusukFiyat(DR_HitHotel.GetGuid(8).ToString()) %> YTL.</span>'den itibaren</td>
	</tr>
	<% } if(ThumbCount == 0) { %>
	<tr class="TableData" style="height: 40px; background-color: #FFFFF5;">
		<td colspan="4">
		    <img src="Images/Notice/Error.png" style="border-width: 0px; vertical-align: middle;" alt="" />
		    Arad���n�z �l��tlere uyan otelimiz bulunmamaktad�r!
		</td>
	</tr>	 
    <% } %>
</table>

</asp:Content>