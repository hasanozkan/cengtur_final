<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="_Detail" Debug="true" %>
<%@ Register Namespace="ImageSlideShow" Assembly="ImageSlideShow" TagPrefix="web" %>
<asp:Content ID="cpDetail" ContentPlaceHolderID="cpDetail" Runat="Server">

<table border="0" cellpadding="0" cellspacing="0" width="510">
	<tr class="BaslikSatiri">
		<td colspan="2" align="right" style="height: 40px; padding-right:10px; background-image:url('Images/Tables/title_bar_turuncu_big.jpg');">
            <span style="font-size: 20px;">otel bilgileri</span>
		</td>
	</tr>

    <tr style="height: 5px;"><td colspan="2"></td></tr>

	<tr class="BaslikSatiri" style="background-image:url('Images/Tables/title_bg_red.jpg');">
		<td colspan="2">
            <asp:Label ID="lblTurAdi" runat="server" Text="lblTurAdi"></asp:Label> / 
            <asp:Label ID="lblSehir" runat="server" Text="lblSehir"></asp:Label> /
            <asp:Label ID="lblBolge" runat="server" Text="lblBolge"></asp:Label>
		</td>
	</tr>

	<tr class="tdDetail" style="height: 40px;">
		<td colspan="2">
            <asp:Label ID="lblAdi" runat="server" Text="lblAdi" Font-Size="24px" ForeColor="#F03307"></asp:Label>
		</td>
	</tr>

    <tr class="tdDetail">
        <td style="width: 65%;">
            <web:ImageSlideShow ID="SlideShowOtel" runat="server" CssClass="SlideShow" />
		</td>
		<td style="width: 35%; vertical-align:top; padding:20px; background-image: url('Images/Tables/detail_bg.jpg'); background-repeat:no-repeat;">
            <asp:ImageButton ID="imgBtnDetail" runat="server" ImageUrl="~/Images/Buttons/detail_button.gif" />
            <p>G�sterim Say�s�: <asp:Label ID="lblHit" runat="server" Text="lblHit" Font-Names="Georgia" Font-Size="20px" ForeColor="#F03307"></asp:Label></p>
		</td>
    </tr>

    <tr style="height: 10px;"><td colspan="2"></td></tr>

    <tr class="tdDetail">
        <td colspan="2">
            <table style="width: 100%; height: 90px; background-image: url('Images/Tables/bg_box_90x509.gif');">
                <tr>
                    <td><b>Tarih Aral���</b></td>
                    <td><b>:</b></td>
                    <td>
                        <asp:Label ID="lblTarihAraligi" runat="server" Text="lblTarihAraligi"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td><b>Konaklama</b></td>
                    <td><b>:</b></td>
                    <td>
                        <asp:Label ID="lblHAdi" runat="server" Text="lblHAdi"></asp:Label>
                    </td>
                </tr>
                <tr valign="top">
                    <td><b>�cret</b></td>
                    <td><b>:</b></td>
                    <td>
                        <asp:Label ID="lblGirisFiyati" runat="server" Text="lblGirisFiyati" Font-Size="14px" ForeColor="#F03307"></asp:Label>
                        <asp:Label ID="lblGirisDetay" runat="server" Text="lblGirisDetay"></asp:Label>
                    </td>
                </tr>
            </table>            
		</td>
	</tr>

	<tr style="height: 10px;"><td colspan="2"></td></tr>

	<tr class="BaslikSatiri" style="background-image:url('Images/Tables/title_bg_mavi.jpg');">
		<td colspan="2">
            TES�S B�LG�LER�
		</td>
	</tr>

	<tr class="tdDetail">
		<td colspan="2">
            <asp:Label ID="lblTesisBilgileri" runat="server" Text="lblTesisBilgileri"></asp:Label>
		</td>
	</tr>
	
	<tr class="tdDetail">
		<td colspan="2">
		    <hr style="color: #FF6600;" />
            Yukar�daki otel bilgileri ve t�m fiyatlar bilgilendirme ama�l� olup, de�i�iklik arz edebilir. 
            Yanl��l�klardan dolay� <b style="color: #F03307;">CengTur</b> sorumlu tutulmaz.<br />
            G�ncel bilgi i�in l�tfen <b style="color: #F03307;">0246 232 3161</b>'den seyahat dan��manlar�m�z ile g�r���n�z.
		</td>
	</tr>
</table>

</asp:Content>