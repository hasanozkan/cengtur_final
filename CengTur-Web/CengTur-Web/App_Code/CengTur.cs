#region Using Tanýmlamalarý
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.Sql;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Security.Cryptography;
#endregion

public class CengTur
{
    public CengTur()
    {

    }
    
    /***********************************************************/
    static SqlConnection Baglanti;
    static SqlTransaction trans;
    public static string[] GetQueryParam(string QueryString)
    {
        char[] DelimiterChars = { ',', '.' };
        return QueryString.Split(DelimiterChars);
    }
    
    public static void dbBaglan()
    {
        Baglanti = new SqlConnection();
        Baglanti.ConnectionString = ConfigurationSettings.AppSettings["SqlServerBaglantisi"];
    }
    /***********************************************************/
  

    /***********************************************************/
    public static void VerileriOnayla()
    {
        trans.Commit();
    }
    public static void VerileriIptalEt()
    {
        trans.Rollback();
    }
    public static SqlDataReader GetOpenHitHotel()
    {
        dbBaglan();
        SqlCommand cmdHitOtels = new SqlCommand("HitOtels", Baglanti);
        cmdHitOtels.CommandType = CommandType.StoredProcedure;
        Baglanti.Open();
        SqlDataReader DR = cmdHitOtels.ExecuteReader(CommandBehavior.CloseConnection);
        return DR;
    }
    public static SqlDataReader GetOpenHitHotelYD()
    {
        dbBaglan();
        SqlCommand cmdHitOtels = new SqlCommand("YDHitOtels", Baglanti);
        cmdHitOtels.CommandType = CommandType.StoredProcedure;
        Baglanti.Open();
        SqlDataReader DR = cmdHitOtels.ExecuteReader(CommandBehavior.CloseConnection);
        return DR;
    }
    public static SqlDataReader GetCustemerComment()
    {
        dbBaglan();
        SqlCommand cmdHitOtels = new SqlCommand("SELECT mId,mMessageHead,mYazan,mMessageFull FROM MusteriTesekkurleri", Baglanti);
        cmdHitOtels.CommandType = CommandType.Text;
        Baglanti.Open();
        SqlDataReader DR = cmdHitOtels.ExecuteReader(CommandBehavior.CloseConnection);
        return DR;
    }
    public static SqlDataReader SearchHotel(string TurKodu, string SKodu, string BKodu)
    {
        dbBaglan();
        SqlCommand cmdSearchHotel = new SqlCommand("SearchOtel", Baglanti);
        cmdSearchHotel.CommandType = CommandType.StoredProcedure;
        cmdSearchHotel.Parameters.Add("@TurKod", SqlDbType.Char);
        cmdSearchHotel.Parameters.Add("@SehKod", SqlDbType.Int);
        cmdSearchHotel.Parameters.Add("@BolKod", SqlDbType.Int);
        cmdSearchHotel.Parameters[0].Value = TurKodu;
        cmdSearchHotel.Parameters[1].Value = SKodu;
        cmdSearchHotel.Parameters[2].Value = BKodu;
        
        Baglanti.Open();
        SqlDataReader DR = cmdSearchHotel.ExecuteReader(CommandBehavior.CloseConnection);
        return DR;
    }
    
    public static SqlDataReader HotelDetail(string OpenId)
    {
        dbBaglan();
        SqlCommand cmdHotelDetail = new SqlCommand("OtelDetail", Baglanti);
        cmdHotelDetail.CommandType = CommandType.StoredProcedure;
        cmdHotelDetail.Parameters.Add("@OpenKod", SqlDbType.VarChar);
        cmdHotelDetail.Parameters[0].Value = OpenId;

        Baglanti.Open();
        SqlDataReader DR = cmdHotelDetail.ExecuteReader(CommandBehavior.CloseConnection);
        return DR;
    }

    public static SqlDataReader OdaKonaklamaTipi(string OpenId)
    {
        dbBaglan();
        SqlCommand cmdOdaKonaklamaTipi = new SqlCommand("OdaKonaklamaTipi", Baglanti);
        cmdOdaKonaklamaTipi.CommandType = CommandType.StoredProcedure;
        cmdOdaKonaklamaTipi.Parameters.Add("@OpenKod", SqlDbType.VarChar);
        cmdOdaKonaklamaTipi.Parameters[0].Value = OpenId;

        Baglanti.Open();
        SqlDataReader DR = cmdOdaKonaklamaTipi.ExecuteReader(CommandBehavior.CloseConnection);
        return DR;
    }
    /***********************************************************/
    

    public static double OtelEnDusukFiyat(string OpenId)
    {
        dbBaglan();
        SqlCommand cmdHitOtels = new SqlCommand("SELECT MIN(fiyat2P) FROM OpenOtelDetails WHERE OpenId = '" + OpenId + "'", Baglanti);
        cmdHitOtels.CommandType = CommandType.Text;
        Baglanti.Open();
        SqlDataReader DR = cmdHitOtels.ExecuteReader(CommandBehavior.CloseConnection);
        if (DR.Read())
            return DR.GetDouble(0);
        else
            return 0;
    }


    /***********************************************************/
    public static DataTable GetDataTable(string SQL)
    {
        dbBaglan();
        SqlDataAdapter DA = new SqlDataAdapter(SQL, Baglanti);
        DataTable DT = new DataTable();
        DA.Fill(DT);
        Baglanti.Close();
        return DT;
    }

    public static void ExecQuery(string SQL)
    {
        dbBaglan();
        try
        {
            Baglanti.Open();
            SqlCommand Komut = new SqlCommand(SQL, Baglanti);
            Komut.ExecuteNonQuery();
        }
        finally
        {
            Baglanti.Close();
        }
    }

    public static int ExecScalarQuery(string SQL)
    {
        dbBaglan();
        try
        {
            Baglanti.Open();
            SqlCommand Komut = new SqlCommand(SQL, Baglanti);
            return (Int32)Komut.ExecuteScalar();
        }
        finally
        {
            Baglanti.Close();
        }
    }
    
    //public static string MD5Encrypt(string toEncrypt)
    //{
    //    byte[] keyArray;
    //    byte[] toEncryptArray = UTF8Encoding.UTF8.GetBytes(toEncrypt);

    //    System.Configuration.AppSettingsReader settingsReader = new AppSettingsReader();
        
    //    string key = (string)settingsReader.GetValue("SecurityKey", typeof(String));
    //    keyArray = UTF8Encoding.UTF8.GetBytes(key);

    //    HMACMD5 hmac = new HMACMD5();
    //    hmac.Key = keyArray;

    //    return BitConverter.ToString(hmac.ComputeHash(toEncryptArray));
    //}
    //public static string MD5Decrypt(string cipherString)
    //{
    //    cipherString=cipherString.Replace('-',' ');
    //    byte[] keyArray;
    //    byte[] toEncryptArray = Convert.FromBase64String(cipherString);

    //    System.Configuration.AppSettingsReader settingsReader = new AppSettingsReader();
    //    //Get your key from config file to open the lock!
    //    string key = (string)settingsReader.GetValue("SecurityKey", typeof(String));
    //        keyArray = UTF8Encoding.UTF8.GetBytes(key);
    //        HMACMD5 hmac = new HMACMD5();
    //        hmac.Key = keyArray;
       
    //    byte[] resultArray = hmac.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);

        
    //    return UTF8Encoding.UTF8.GetString(resultArray);
    //}
    public static int AgeSpan(DateTime start, DateTime end)
    {

        // Compute the difference between start 

        //year and end year. 

        int years = end.Year - start.Year;

        int months = 0;

        int days = 0;

        // Check if the last year was a full year. 

        if (end < start.AddYears(years) && years != 0)
        {

            --years;

        }

        start = start.AddYears(years);

        // Now we know start <= end and the diff between them

        // is < 1 year. 

        if (start.Year == end.Year)
        {

            months = end.Month - start.Month;

        }

        else
        {

            months = (12 - start.Month) + end.Month;

        }

        // Check if the last month was a full month.

        if (end < start.AddMonths(months) && months != 0)
        {

            --months;

        }

        start = start.AddMonths(months);

        // Now we know that start < end and is within 1 month

        // of each other. 

        days = (end - start).Days;

        return years;

    }

/***********************************************************/
}