﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="PassportAndVisaInformations.aspx.cs" Inherits="Default2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpDetail" Runat="Server">
<table border="0" cellpadding="0" cellspacing="0" width="510">
	<tr class="BaslikSatiri">
		<td align="right" 
            style="height: 40px; padding-right:10px; background-image:url('Images/Tables/title_bar_turuncu_big.jpg');">
            <span style="font-size: 20px;">Pasaport ve Vize Bilgileri</span>
		</td>
	</tr>
	<tr class="BaslikSatiri" style="background-image:url('Images/Tables/title_bg_yesil.jpg');"><td>Pasaport ve Vize İşlemleri</td></tr>
	<tr>
	<td><hr />Pasaport ve Vize işlemleriniz hakkında detaylı bilgi almak için <a href="http://www.vizeci.com" target="_blank">www.vizeci.com</a> adresini ziyaret ediniz...<hr /></td>
	</tr>	
	<tr class="BaslikSatiri" style="background-image:url('Images/Tables/title_bg_red.jpg');"><td>Vize İçin Gerekli Evraklar</td></tr>
	<tr>
	<td> 
	
	    <span class="Baslik"><b>İŞVEREN İSENİZ:</b></span>
	    <br />
        <b>1. </b>Şirketin antetli kağıdına 
            ilgili makama hitaben turistik amaçlı vize talebinizi belirten dilekçe. (Yetkili 
            kişi veya kişilerce kaşe ve imza)<b><br />
            2. </b>Ticaret veya sanayi odasından faaliyet belgesi. <b>
            <br />
            3. </b>Vergi levhasının fotokopisi. <b>
            <br />
            4. </b>Firmanızın imza sirküleri. (Fotokopi) <b>
            <br />
            5. </b>Ticari sicil gazetesi. (Ortak iseniz isminizin geçtiği yeri 
            işaretleyiniz) <b>
            <br />
            6. </b>Gayri menkul fotokopileri. (Ev, Arsa, Devre mülk v.b) <b>
            <br />
            7. </b>Şahsi veya firmaya ait banka cüzdanları. <b>
            <br />
            8. </b>Yeni çekilmiş fotoğraf. (Vesikalık)
            <br />
        <br />
        <span class="Baslik"> ÇALIŞAN İSENİZ:</span>
        <br /><b>1. </b>Çalıştığınız firmanın 
            antetli kağıdına ilgili makama hitaben çalıştığınıza dair dilekçe.<b><br />
            2. </b>İlgili makama hitaben turistik gezi amaçlı vize talebinizi belirten 
            dilekçe. <b>
            <br />
            3. </b>S.S.K. işe giriş. (S.S.K. Onaylı fotokopisi)<b> </b>ve son dört aylık SSK 
            primlerinin ödendiğine ilişkin belge<b><br />
            4. </b>Son üç aylık maaş bordronuzun fotokopileri veya asılları. <b>
            <br />
            5. </b>Yukarıdaki 2.3.4.5.6.7.8. nolu şıklardaki evraklar da istenilmektedir.<br />
            <br />
            <span class="Baslik">EV HANIMI İSENİZ:</span>
            <br />
            <b>1.</b> İlgili makama hitaben 
            dilekçe. <b>
            <br />
            2. </b>Gayrimenkullerin fotokopisi. (Ev, Arsa, Devre mülk v.b) <b>
            <br />
            3. </b>Eşinizin iş durumunu belirten belgeler. (İş durumuna göre yukarıdakilerin 
            hepsi) <b>
            <br />
            4. </b>Şahsi banka hesap cüzdanı. <b>
            <br />
            <br />
            <span class="Baslik">EMEKLİ İSENİZ:</span>
            <br />
            <b>1. </b>İlgili makama hitaben 
            dilekçe. (Turistik amaçlı vize talebi) <b>
            <br />
            2. </b>Emekli cüzdanı ve maaşını aldığı bankanın hesap cüzdanı. <b>
            <br />
            3. </b>Gayrimenkuller. (Ev, Arsa, Tapu, v.b fotokopileri)<br />
            <br />
            <span class="Baslik">ÖĞRENCİ İSENİZ:</span>
            <br />
            <b>1. </b>Dilekçe. (İlgili makama 
            turistik vize amaçlı) <b>
            <br />
            2. </b>Okuldan alınmış öğrenci belgesi. (Bu seneye ait) <b>
            <br />
            3. </b>Ebeveynlerin iş durumunu gösteren belgeler. (İşine göre yukarıdakiler)<b><br />
            4.</b>Masrafların ailesi tarafından karşılanacağına ilişkin taahhütname
            <br />
            <br />
            <span class="Baslik">PASAPORTUNUZUN GEÇERLİ OLMASİ İÇİN:</span> 
            <br />
            <b>1. </b>Pasaportun vize başvuru 
            tarihinden itibaren geçerlilik&nbsp; süresinin en az 6 ay olması. <b>
            <br />
            2.</b> Vizenin basılacağı vize sayfalarının boş olması. <b>
            <br />
            3.</b> Pasaport hamili tarafından imzalanmış olması<span class="Baslik"><br />
        18 yaşından küçük çocuklar tek 
            başına gidiyorsa ailesinin noterden muvafakatname vermesi gerekmektedir.</span>
       <br />
       <br />
       <span class="Baslik">VİZE NEDİR?</span>
        <br />
        Bir ülkeden diğer bir ülkeye geçişte 
            ülkeler arasındaki kararlar doğrultusunda şahsın kendine ait pasaportuna yada 
            yerine geçebilecek diğer bir belgeye,gideceği ülkenin temsilciliği (Konsolosluk, 
            Büyükelçilik.. vb.) tarafında verilen izin belgesidir.
            <br />
            
            <span class="Baslik">VİZELER:</span>
            <br />
            Turistik, Ticari veya öğrenim amacı 
            ile alınır ve süresi verilen makam tarafından ziyaretin amacına göre saptanır.<br />
            <span class="Baslik">TRANSİT VİZE:</span>
            <br />
            Kişiler çıkış yaptıkları ülkeden 
            ikinci bir ülke üzerinden üçüncü bir ülkeye geçiş yapacaklarsa ve bu geçiş 
            esnasından ikinci ülkenin gümrük sahasından ayrılacaklarsa, ülkeler arası 
            anlaşmaya göre ikinci ülkeden üçüncü ülkeye geçiş için alınan vize &quot;Transit 
            Vize&quot; dir.<br />
            <span>VİZE ÖZELLİĞİ:</span>
            <br />
      AVUSTURYA, MACARİSTAN, ÇEK 
            CUMHURİYETİ, SLOVAKYA, POLONYA, BELÇİKA, FRANSA, ALMANYA, YUNANİSTAN, İTALYA, 
            LÜKSEMBURG, HOLLANDA, PORTEKİZ, İSPANYA, DANİMARKA, FİNLANDİYA, İZLANDA, NORVEÇ, 
            İSVEÇ
            <br />
            Yukarıdaki <b>schengen</b> ülkelerinin herhangi birinden alınan vize ile diğer 
            schengen ülkelerine seyahat gerçekleşebilir. Sınır polislerinin isteği, yolcunun 
            gideceği ülkenin konsolosluğundan vize alarak schengenin gerçekleştirilmesi 
            doğrultusunda olmaktadır.<br />
            <span class="Baslik">MULTİPLE:</span>
            <br />
            Çok giriş çıkış ( Vizenin bitiş 
            Tarihine kadar geçerli )<br />
            <span class="Baslik">SİNGLE:</span>
            <br />
            Tek giriş çıkış ( Bir kere 
            kullanılabilir. Vizenin bitiş tarihi geçmemiş bile olsa 2.çıkış yapılamaz.)
            <br />
            <ul>
            <li>TRANSİT VİZE ( HAVAYOLU)</li>
            <li>TRANSİT VİZE ( KARAYOLU)</li>
            <li>KISA SÜRELİ KALIŞ ( 3 AY VEYA DAHA AZ )</li>
            <li>UZUN SÜRELİ KALIŞ ( 6 AY VEYA DAHA FAZLA ) </li>
            </ul>
        
        <span class="Baslik">PASAPORT:</span>
            Bir ülkenin vatandaşına veya 
            vatandaşlığına geçmiş yada bir başka bir ülkenin vatandaşıyken sığınma hakkı 
            alan kişilere verilen resmi bir belgedir. Kişiler bir ülkeden başka bir ülkeye 
            seyahatlerinde pasaport yada ülkelerin almış olduğu kararlarla yada pasaport 
            yerine geçen kimliklerle geçebilir. Bazı kişiler milliyetine tabi olduğu ülkede 
            başka bir ülkenin vatandaşlık hakkını kazanıp o ülkelerden pasaport alma hakkı 
            kazanır. Dolayısıyla seyahat eden kişiler değişik ülkelerin kimlikleri 
            bulunabilir.
        <br />
        <br />
        <span class="Baslik">PASAPORT ÇEŞİTLERİ:</span>
            <br />
            Bir ülkenin vatandaşına vermiş 
            olduğu normal pasaportun dışında
            <br />
            <b class="Baslik">1-YABANCILAR PASAPORTU </b>
            <br />
            Başka bir ülkenin milliyetindeki 
            kişilerin farklı bir ülkede vatandaşlık hakkı kazanıp o ülkeden aldıkları 
            pasaporttur.
            <br />
            <b class="Baslik">2-ÇOCUKLARIN PASAPORT YERİNE 
            GEÇEN KİMLİKLERİ </b>
            <br />
            ÖRNEĞİN : KINDERAWEIS 
        <br/>
        <b class="Baslik">3-DİPLOMATİK VE KONSOLOSLUK 
            PASAPORTLARI</b>
            <br />
            Uluslararası gümrük ve kanunlar 
            doğrultusunda kişiler hak etmiş oldukları pasaportlardır. 
        <br />
       <b class="Baslik">4-ULUSLARARASI KIRMIZI KAPLI 
            PASAPORTLAR</b>
       <br />
       ÖRNEĞİN: Birleşmiş milletl, er ve 
            benzeri kuruluşların vermiş olduğu pasaportlar.
        <br />
        <b class="Baslik">5-AİLE PASAPORTLARI</b>
        <br />
        <strong>a- </strong>Karı ve kocanın 
            birlikte bir tek pasaporta sahip olmaları
            <br />
            <strong>b- </strong>Anne ve babanın pasaportuna yaş sınırına göre çocuklarının 
            da kayıtlı olmaları 
            <br />
            <b class="Baslik">6.GÖREVLİ ÖZEL VE SERVİS 
            PASAPORTLARI </b>
            <br />
            Devlet dairelerinden veya özel 
            görevlerde bulunanlara verilen pasaportlardır. 
            <br />
        <b>TÜRKİYE&#39;NİN VERMİŞ OLDUĞU 
            PASAPORT VE KİMLİK TÜRLERİ:</b>
            <br />
            <ul>
            <li style="text-align: left">
            Siyah kaplı 
            normal vatandaşların kullandığı pasaport,
            </li>
            <li>
            Kırmızı kaplı diplomatik pasaport (Cumhurbaşkanı, Başbakan, TBMM Bakanlarının 
            kullandığı pasaportlar),
            </li>
            <li>
            Yeşil kaplı hususi pasaport (Devlet dairesinde 1.2.3 derecedeki memurların 
            kullandığı &nbsp;&nbsp;pasaportlar),
            </li>
            <li>
            Gri kaplı pasaportlar (öğretmen,doktor,subay vb görevlilere verilen 
            pasaportlar),
            </li>
            <li>
            Yabancılara ait pasaportla, Dışişleri Bakanlığının onayı ile Türkiye&#39;de 3 ila 
            6 ay arasında kalma hakkına sahip olan kişilere verilen pasaportlar,
            </li>
            <li>
            Gemici belgeleri,gemici memur ve mürettebatının pasaport olarak 
            kullanabileceği belgelerdir,
            </li>
            <li>
            Landing Card,Türk vatandaşı olup yurt dışına çıkacak görevli uçucu ekibine 
            verilen belgeler</span>
            </li>
            </ul>
	<hr />
	Pasaport ve Vize işlemleriniz hakkında detaylı bilgi almak için <a href="http://www.vizeci.com" target="_blank">www.vizeci.com</a> adresini ziyaret ediniz...
	<br />
	</td>
	</tr>
	
	</table>
</asp:Content>

