﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="AboutUs.aspx.cs" Inherits="Default2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpDetail" Runat="Server">
<table border="0" cellpadding="0" cellspacing="0" width="510">
	<tr class="BaslikSatiri">
		<td align="right" 
            style="height: 40px; padding-right:10px; background-image:url('Images/Tables/title_bar_turuncu_big.jpg');">
            <span style="font-size: 20px;">Hakkımızda</span>
		</td>
	</tr>
	<tr>
	<td>
	<br>
Türkiye'nin iç pazardaki en büyük turizm grubu, 1991 yılında, Türk gezginlere tatil alternatifleri üretmek üzere Mehmet ve Murat Ersoy kardeşler tarafından kurulan, Türkiye'nin lider tur operatörü Etstur (Ersoy Turistik Servisleri A.Ş) ile iş hayatına başladı. Etsgroup bünyesinde şu an Etstur, Didimtur, Atlasjet, Club Voyage Otelleri, <a href="http://www.ucuzabilet.com" target="_blank" class="def11b2">ucuzabilet.com</a> ve Jetset şirketleri bulunuyor. Grup yapısına son olarak, oteller hakkında daha önce bu otellerde konaklamış kişilerce yapılan değerlendirmeleri, puanlamaları ve fotoğrafları barındıran objektif bir paylaşım ve bilgilendirme platformu olan  <a href="http://www.otelpuan.com" target="_blank" class="def11b2">Otelpuan.com</a> katıldı. Etsgroup, 3255 çalışanı ve 346 acentesi ile misafirlerine Türkiye'nin dört bir yanında en kaliteli hizmeti, sürekli sunmayı hedefliyor. <br><br>

<span class="Baslik">HİZMET KALİTEMİZ 2. KEZ ONAYLANDI</span>
<br>
Misafirlerine sağladığı hizmetlerin sürekli iyileştirilmesi ve geliştirilmesi amacı ile yapılan çalışmalara bir yenisini daha eklemenin haklı gururunu yaşıyoruz. <br>
T.C. Sanayi ve Ticaret Bakanlığı Tüketici Memnuniyetini İlke Edinen Firma Ödülünün ardından etstur'un hizmet kalitesi 2. kez tescillendi. ISO 9001:2000 Kalite Belgesini almaya hak kazandık.<br>
Etstur, tüketici haklarına saygılı, memnuniyet odaklı  hizmet kalitesini sürekli  geliştirerek, güveninize layık olmaya ve huzur dolu güvenli tatiller sunmaya devam edecektir.
<br><br>

<span class="Baslik">Kalite Politikamız </span>
<br>
Sunduğumuz Hizmet kalitesi ile <strong>MÜŞTERİLERİMİZİN</strong>, <br>
Çalışma ortamı ve koşulları ile <strong>ÇALIŞANLARIMIZIN</strong>, <br>
Verimlilik ve rekabet gücü ile <strong>TEDARİKÇİLERİMİZİN</strong>,<br>
Yasal düzenlemelere uyma ve sosyal sorumluluğumuz ile <strong>TOPLUMUN</strong> <br>
beklentilerinin üzerinde hizmet sağlamak amacıyla, belirlenmiş kalite standartlarımızı uygulamak, çalışanlarımızı eğitmek, hizmet ve süreçlerimizi sürekli iyileştirmektir. 
<br><br>
<span class="Baslik">Vizyonumuz </span> 
<br>
Koşulsuz müşteri ve çalışan memnuniyeti felsefesini benimsemiş, sektör lideri ve itibarlı tanınmış bir marka olmak. 
<br><br>
<span class="Baslik">Misyonumuz </span> 
<ul>
<li>Sektördeki yeniliklerde lider olmak, 
<li>Bireysel ve kurumsal anlamda sürekli gelişmek, 
<li>Teknolojinin tüm imkanlarını çalışanlarına ve müşterilerine sunmak, 
<li>Toplumsal değerlere sahip çalışma yaşamını sürdürmek, 
<li>Çalışılması tercih edilen şirket olmak 
</ul>
<br><br>
<span class="Baslik">Bireysel Hizmetler </span> 
<br>
Türk Turizm Sektörü'nde mevcut en geniş ürün ve hizmet yelpazesine sahip olan <span class="red_txt-s">Etstur</span>, her gün bünyesine yeni ürün ve hizmetler katmaya devam etmektedir.
<br><br>
<ul>
<li>Otel ve Tatil Köyleri 
<li>Havayolu, Karayolu, Denizyolu Ulaşım Hizmetleri 
<li>Kültür Turları 
<li>Mavi Yolculuk 
<li>Gençlik Turları 
<li>Yurt Dışı Tur Paketleri 
<li>Kayak Turları 
<li>Gemi Seyahatleri 
<li>Sağlık (Termal Oteller) Tesisleri 
<li>Kiralık Araç Hizmeti 
<li>Balayı (Yurt İçi ve Yurt Dışı Seçenekler) Paketleri 
<li>Vize Asistans Hizmetleri 
</ul>
<span class="Baslik">Kurumsal Hizmetler</span> 
<br>
<span class="red_txt-s">Etstur</span>, büyük, orta ve küçük boy işletmelerin her tür kurumsal seyahat ihtiyacına üstün teknolojisi, cazip fiyat ve koşulları, deneyimi, benzersiz kaynakları ve yüksek hizmet kalitesi ile toplu çözümler sunmaktadır. 
<br><br>
<ul>
<li>Dış Hat Uçak Biletleri 
<li>İç Hat Uçak Biletleri 
<li>Yurt İçi ve Yurt Dışı Otel Rezervasyonları 
<li>Yurt İçi ve Yurt Dışı Rent A Car Rezervasyonları 
<li>Seyahat Sigortası ve Asistans Hizmetleri 
<li>Fuar Organizasyonları 
<li>Motivasyon ve Teşvik Gezileri 
<li>Kapalı Grup Organizasyonları 
<li>Açılış, Kutlama ve Benzeri Şirket ve Birlik Organizasyonlar 
<li>Kültür, Sanat, Eğlence ve Benzeri Etkinlik Organizasyonları 
<li>Eğitim ve Araştırma Gezileri 
<li>Vize Asistans Hizmetleri 
<li>Puan ve Ödül Kataloğu Hizmetleri 
</ul>
<br><br>

Etstur Seyahat Hattı, Müşteri Hizmetleri, Çağrı Merkezi, İnternet Hizmetleri ve kurumsal modülü ile şirketlere kurumsal seyahat işlemlerinde birçok avantaj sunmaktadır; 
<br><br>
<ul>
<li>Kolay kullanım 
<li>Uygun fiyatlar 
<li>Kullanıcının belirlediği kriterlere göre en uygun seyahat alternatiflerine erişim 
<li>Otomatik fiyatlandırma sayesinde hesaplama hatalarının önlenmesi 
<li>Şirket çalışanlarının pozisyonlarına göre yetki düzeyleri tanımlama (örnek; belli pozisyonda çalışan insanlar ekonomi sınıfı bileti, 4 yıldızlı otelde rezervasyon v.b hakkına sahiptir). 
<li>Gerçek zamanlı raporlama sayesinde, seyahat harcamalarının kontrol 
edilmesi</ul>                                
    </td>
	</tr>
	</table>
</asp:Content>

