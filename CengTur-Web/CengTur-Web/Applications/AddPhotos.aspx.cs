using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;

public partial class Applications_AddPhotos : System.Web.UI.Page
{
    public string OtelKodu;
    public ArrayList files = new ArrayList();
    static public ArrayList hif = new ArrayList();

    protected void Page_Load(object sender, EventArgs e)
    {
        OtelKodu = Request.QueryString["OtelKodu"];
    }
    protected void AddFile_Click(object sender, EventArgs e)
    {
        if (Page.IsPostBack == true)
        {
            if (FindFile.PostedFile.FileName == string.Empty)
            {
                imgError.Visible = true;
                lblError.Text = " Öncelikle bir fotograf seçmelisiniz!";
                return;
            }
            else if (FindFile.PostedFile.ContentLength <= 0)
            {
                imgError.Visible = true;
                lblError.Text = " Fotograf Boyutu 0 Byte'tan büyük olmalıdır!";
                return;
            }
            else if ((FindFile.PostedFile.ContentType != "image/pjpeg") && (FindFile.PostedFile.ContentType != "image/gif") &&
                    (FindFile.PostedFile.ContentType != "image/bmp") && (FindFile.PostedFile.ContentType != "image/x-png"))
            {
                imgError.Visible = true;
                lblError.Text = " Sadece JPG, GIF, BMP veya PNG uzantılı Fotografları yükleyebilirsiniz!";
                return;
            }
            else if (FindFile.PostedFile.ContentLength > 1024 * 1024 * 2)
            {
                imgError.Visible = true;
                lblError.Text = " Fotograf Boyutu 2 MByte (2048 Kbyte)'tan küçük olmalıdır!";
                return;
            }
            else
            {
                hif.Add(FindFile);
                ListBox1.Items.Add(FindFile.PostedFile.FileName);
                imgError.Visible = false;
                lblError.Text = string.Empty;
            }
        }
    }
    protected void RemvFile_Click(object sender, EventArgs e)
    {
        if ((ListBox1.Items.Count != 0) && (ListBox1.SelectedIndex != -1))
        {
            hif.RemoveAt(ListBox1.SelectedIndex);
            ListBox1.Items.Remove(ListBox1.SelectedItem.Text);
        }
    }
    protected void Upload_ServerClick(object sender, EventArgs e)
    {
        string status = string.Empty;

        if (ListBox1.Items.Count == 0)
        {
            imgError.Visible = true;
            lblError.Text = " Öncelikle fotograf seçmelisiniz!";
            return;
        }
        else
        {
            System.IO.Directory.CreateDirectory(Server.MapPath("../Images/Otels/Bigs/") + OtelKodu);
            string FotoYolu = "../Images/Otels/Bigs/" + OtelKodu + "/";
            string TempFotoYolu = "../Images/Otels/Bigs/Temp/";

            foreach (System.Web.UI.HtmlControls.HtmlInputFile HIF in hif)
            {
                string fn = "";
                try
                {
                    fn = System.IO.Path.GetFileName(HIF.PostedFile.FileName);
                    HIF.PostedFile.SaveAs(Server.MapPath(TempFotoYolu + fn));

                    Bitmap Bmp = new Bitmap(ResizeImage(System.Drawing.Image.FromFile(Server.MapPath(TempFotoYolu + fn))));
                    Bmp.Save(Server.MapPath(FotoYolu + fn));

                    status += fn + "<br>";
                }
                catch (Exception err)
                {
                    imgError.Visible = true;
                    lblError.Text = " Yükleme sırasında hata oluştu: " + FotoYolu + fn + "<br>" + err.ToString();
                }
            }

            hif.Clear();

            FormAddPhotos.Visible = false;
            lblInfo.Text = "Fotograflarınız başarıyla yüklenmiştir.<br />" + status;
            divShowPhotos.Visible = true;
        }
    }
    private static System.Drawing.Image ResizeImage(System.Drawing.Image imgPhoto)
    {
        int sourceWidth = imgPhoto.Width;
        int sourceHeight = imgPhoto.Height;
        int sourceX = 0;
        int sourceY = 0;

        int destX = 0;
        int destY = 0;
        int destWidth = 300;
        float Oran = (destWidth / (float)sourceWidth);
        int destHeight = (int)(sourceHeight * Oran);

        Bitmap bmPhoto = new Bitmap(destWidth, destHeight, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
        bmPhoto.SetResolution(imgPhoto.HorizontalResolution, imgPhoto.VerticalResolution);

        Graphics grPhoto = Graphics.FromImage(bmPhoto);
        grPhoto.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;

        grPhoto.DrawImage(imgPhoto,
            new Rectangle(destX, destY, destWidth, destHeight),
            new Rectangle(sourceX, sourceY, sourceWidth, sourceHeight),
            GraphicsUnit.Pixel);

        grPhoto.Dispose();
        return bmPhoto;
    }
}