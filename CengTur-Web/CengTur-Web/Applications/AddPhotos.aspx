<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AddPhotos.aspx.cs" Inherits="Applications_AddPhotos" Debug="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
    <link href="../Style/CengTur.css" type="text/css" rel="stylesheet"/>
    <script type="text/javascript">
        //Bloklama i�lemini iptal eder...
        window.document.onselectstart=new Function ("return false")
    </script>    
</head>
<body>

<form id="FormAddPhotos" runat="server" encType="multipart/form-data">
    <div>
    <fieldset class="text" style="padding-left: 20; padding-bottom: 10; width: 100%; border-left-width: 0px; 
        border-right-width: 0px; border-top-style: solid; border-top-width: 2px; border-bottom-style: solid; 
        border-bottom-width: 2px; border-color: #FF6600">
        <legend><img src="Images/Foto.png" alt="" style="border-width: 0px;"/></legend>
        <p></p>
            <table border="0" id="tblAddPhotos">
            <tr>
                <td valign="top" colspan="2" style="width: 100%">
                    <input id="FindFile" type="file" runat="server" style="width:250px;"/>
                </td>
            </tr>
            <tr>
                <td valign="top" colspan="2" style="width: 100%">
                    <asp:listbox id="ListBox1" runat="server" Height="100px" Width="250px" CssClass="text" Font-Size="X-Small"></asp:listbox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:ImageButton ID="AddFile" runat="server" ImageUrl="~/Applications/Images/Add.png" OnClick="AddFile_Click" ToolTip="Se�ilen fotograf� y�klemek �zere listeye ekle..." />
                    <asp:ImageButton ID="RemvFile" runat="server" ImageUrl="~/Applications/Images/Delete.png" OnClick="RemvFile_Click" ToolTip="Se�ilen fotograf� listeden ��kar..." />
                </td>
                <td align="right">
                    <asp:ImageButton ID="imgUpload" runat="server" ImageUrl="~/Applications/Images/yukle_button.gif" OnClick="Upload_ServerClick" />
                </td>
            </tr>
            </table>
        <asp:Image ID="imgError" runat="server" ImageUrl="~/Applications/Images/Exclamation.png" ImageAlign="AbsMiddle" Visible="false" />
        <asp:label id="lblError" runat="server" Width="400px" Font-Bold="True" ForeColor="#F03307"></asp:label>
    </fieldset>
    </div>

    <p class="text">L�tfen y�kleyece�iniz Fotograf�n;</p>
    <ul class="text">
        <li><b>JPG</b>, <b>GIF</b>, <b>BMP</b> veya <b>PNG</b> dosya uzant�s�nda,</li>
        <li>Boyutunun <b>2 MByte</b> (2048 Kbyte)'tan k���k olmas�na dikkat ediniz.</li>
    </ul>
</form>

<div id="divShowPhotos" runat="server" visible="false">
    <fieldset class="text" style="padding-left: 20; padding-bottom: 10; width: 100%; border-left-width: 0px; 
        border-right-width: 0px; border-top-style: solid; border-top-width: 2px; border-bottom-style: solid; 
        border-bottom-width: 2px; border-color: #FF6600">
		<legend><img src="Images/Foto.png" alt="" style="border-width: 0px;"/></legend>
		<p></p>
	    <table border="0" class="text">
	        <tr>
	            <td colspan="2"><img src="Images/Ok.png" alt="" style="vertical-align:middle; border-width: 0px;" /> <asp:Label ID="lblInfo" runat="server" Text="lblInfo"></asp:Label></td>
	        </tr>
	    </table>
        <p></p>
    </fieldset>
    <a href="AddPhotos.aspx?OtelKodu=<% =OtelKodu %>" class="text">&lt;&lt; Yeni fotograf ekle</a>
</div>

</body>
</html>