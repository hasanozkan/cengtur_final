using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;

public partial class Applications_ChangeThumb : System.Web.UI.Page
{
    public string OtelKodu;
    protected void Page_Load(object sender, EventArgs e)
    {
        OtelKodu = Request.QueryString["OtelKodu"];

        switch (Request.QueryString["ErrorCode"])
        {
            case "1":
                {
                    this.FormHataYakala.Visible = true;
                    this.FormAddThumb.Visible = false;
                    lblHata.Text = "Sadece JPG, GIF, BMP veya PNG uzant�l� Fotograflar� y�kleyebilirsiniz!";
                    break;
                }
            case "2":
                {
                    this.FormHataYakala.Visible = true;
                    this.FormAddThumb.Visible = false;
                    lblHata.Text = "Fotograf Boyutu 2 MByte (2048 Kbyte)'tan k���k olmal�d�r!";
                    break;
                }
            case "3":
                {
                    this.FormHataYakala.Visible = true;
                    this.FormAddThumb.Visible = false;
                    lblHata.Text = "Fotograf Boyutu 0 Byte'tan b�y�k olmal�d�r!";
                    break;
                }
            default: this.FormHataYakala.Visible = false; break;
        }
    }
    protected void btnThumbYukle_Click(object sender, EventArgs e)
    {
        if (FileUpload1.HasFile)
            if ((FileUpload1.PostedFile.ContentType != "image/pjpeg") && (FileUpload1.PostedFile.ContentType != "image/gif") &&
                (FileUpload1.PostedFile.ContentType != "image/bmp") && (FileUpload1.PostedFile.ContentType != "image/x-png"))
                Response.Redirect("ChangeThumb.aspx?ErrorCode=1&OtelKodu=" + OtelKodu);
            else if (FileUpload1.PostedFile.ContentLength > 1024 * 1024 * 2)
                Response.Redirect("ChangeThumb.aspx?ErrorCode=2&OtelKodu=" + OtelKodu);
            else
                Upload_Insert_Thumb(FileUpload1.FileName);
        else if (FileUpload1.PostedFile.ContentLength <= 0)
            Response.Redirect("ChangeThumb.aspx?ErrorCode=3&OtelKodu=" + OtelKodu);
    }

    private void Upload_Insert_Thumb(string FileName)
    {
        string FotoYolu = "../Images/Otels/Thumbs/Temp/" + FileName;
        FileUpload1.SaveAs(Server.MapPath(FotoYolu));
        Bitmap Bmp = new Bitmap(ResizeImage(System.Drawing.Image.FromFile(Server.MapPath(FotoYolu))));
        Bmp.Save(Server.MapPath("../Images/Otels/Thumbs/" + FileName));

        CengTur.ExecQuery("UPDATE Oteller SET Thumb='" + FileName + "' WHERE OtelKodu=" + OtelKodu);

        FormAddThumb.Visible = false;
        addedThumb.ImageUrl = "../Images/Otels/Thumbs/" + FileName;
        lblFotoInfo0.Text = "Geni�lik: " + Bmp.Width.ToString() + "px";
        lblFotoInfo1.Text = "Y�kseklik: " + Bmp.Height.ToString() + "px";
        Bmp.Dispose();
        divShowThumb.Visible = true;
    }
    private static System.Drawing.Image ResizeImage(System.Drawing.Image imgPhoto)
    {
        int sourceWidth = imgPhoto.Width;
        int sourceHeight = imgPhoto.Height;
        int sourceX = 0;
        int sourceY = 0;

        int destX = 0;
        int destY = 0;
        int destWidth = 105;
        int destHeight = 70; 
        //float Oran = (destWidth / (float)sourceWidth);
        //int destHeight = (int)(sourceHeight * Oran);

        Bitmap bmPhoto = new Bitmap(destWidth, destHeight, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
        bmPhoto.SetResolution(imgPhoto.HorizontalResolution, imgPhoto.VerticalResolution);

        Graphics grPhoto = Graphics.FromImage(bmPhoto);
        grPhoto.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;

        grPhoto.DrawImage(imgPhoto,
            new Rectangle(destX, destY, destWidth, destHeight),
            new Rectangle(sourceX, sourceY, sourceWidth, sourceHeight),
            GraphicsUnit.Pixel);

        grPhoto.Dispose();
        return bmPhoto;
    }
}
