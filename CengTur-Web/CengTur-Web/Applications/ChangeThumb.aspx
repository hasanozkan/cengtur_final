<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ChangeThumb.aspx.cs" Inherits="Applications_ChangeThumb" Debug="true" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
    <link href="../Style/CengTur.css" type="text/css" rel="stylesheet"/>
    <script type="text/javascript">
        //Bloklama i�lemini iptal eder...
        window.document.onselectstart=new Function ("return false")
    </script>
</head>
<body>

<form id="FormHataYakala" runat="server">
	<div>
	    <fieldset class="text" style="padding-left: 20; padding-bottom: 10; width: 100%; border-left-width: 0px; 
            border-right-width: 0px; border-top-style: solid; border-top-width: 2px; border-bottom-style: solid; 
            border-bottom-width: 2px; border-color: #FF6600">
			<legend><img src="Images/House.png" alt="Sayfada Hata Olu�tu" style="border-width: 0px;"/></legend>
			<p></p>
	            <asp:Label ID="lblHata" runat="server" Text="[HataMesaj�]" CssClass="tah11" Font-Bold="true"></asp:Label><br />
	            <asp:Label ID="lblTekrar" runat="server" Text="L�tfen tekrar deneyiniz..." CssClass="tah11"></asp:Label>
	        <p></p>
	    </fieldset>
	    <a href="JavaScript:history.go(-1);" class="text">&lt;&lt; Tekrar Dene</a>
	</div>
</form>

<form id="FormAddThumb" runat="server">
    <div>
    <fieldset class="text" style="padding-left: 20; padding-bottom: 10; width: 100%; border-left-width: 0px; 
        border-right-width: 0px; border-top-style: solid; border-top-width: 2px; border-bottom-style: solid; 
        border-bottom-width: 2px; border-color: #FF6600">
        <legend><img src="Images/House.png" alt="" style="border-width: 0px;"/></legend>
        <p></p>
            <table border="0" id="tblAddThumb">
            <tr>
                <td valign="top" style="width: 30%">K���k Resim Se�iniz:</td>
                <td style="width: 70%"><asp:FileUpload ID="FileUpload1" runat="server"/></td>
            </tr>
            <tr>
                <td></td>
                <td>
                <asp:ImageButton ID="imgThumbYukle" runat="server" ImageUrl="~/Applications/Images/yukle_button.gif" OnClick="btnThumbYukle_Click" />
                    <asp:RequiredFieldValidator ID="ReqFieldFoto" runat="server" ControlToValidate="FileUpload1"
                	    ErrorMessage="* Bir Fotograf se�melisiniz!" Font-Bold="True" ForeColor="#F03307">
                    </asp:RequiredFieldValidator>
                </td>
            </tr>
            </table>
        <p></p>
    </fieldset>
    </div>

    <p class="text">L�tfen y�kleyece�iniz Fotograf�n;</p>
    <ul class="text">
        <li><b>JPG</b>, <b>GIF</b>, <b>BMP</b> veya <b>PNG</b> dosya uzant�s�nda,</li>
        <li>Boyutunun <b>2 MByte</b> (2048 Kbyte)'tan k���k olmas�na dikkat ediniz.</li>
        <li><b>105x70px</b> geni�li�inde olmayan Fotograflar boyutland�r�lacakt�r.</li>
    </ul>
</form>

<div id="divShowThumb" runat="server" visible="false">
    <fieldset class="text" style="padding-left: 20; padding-bottom: 10; width: 100%; border-left-width: 0px; 
        border-right-width: 0px; border-top-style: solid; border-top-width: 2px; border-bottom-style: solid; 
        border-bottom-width: 2px; border-color: #FF6600">
		<legend><img src="Images/House.png" alt="" style="border-width: 0px;"/></legend>
		<p></p>
	    <table border="0" class="text">
	        <tr>
	            <td style="width: 30%;"><asp:Image ID="addedThumb" runat="server" BorderWidth="1px" /></td>
                <td style="width: 70%;" valign="top">
                    <asp:Label ID="lblFotoInfo0" runat="server" CssClass="tah11"></asp:Label><br />
                    <asp:Label ID="lblFotoInfo1" runat="server" CssClass="tah11"></asp:Label>
                </td>
	        </tr>
	        <tr>
	            <td colspan="2"><img src="Images/Ok.png" alt="" style="vertical-align:middle; border-width: 0px;" /> K���k resim ba�ar�yla y�klenmi�tir.</td>
	        </tr>
	    </table>
        <p></p>
    </fieldset>
    <a href="ChangeThumb.aspx?OtelKodu=<% =OtelKodu %>" class="text">&lt;&lt; K���k resimi de�i�tir</a>
</div>

</body>
</html>