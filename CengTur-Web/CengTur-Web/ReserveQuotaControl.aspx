<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ReserveQuotaControl.aspx.cs" Inherits="ReserveQuotaControl" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpDetail" Runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    <Scripts>
    <asp:ScriptReference  Path="~/Scripts/md5.js"/>
    </Scripts>
    </asp:ScriptManagerProxy>
   
    <script language="javascript" type="text/javascript">
        function AddPersonalInfo() {
        PageMethods.TotalPerson(ToplamKisiAlindi);
      }
      function ToplamKisiAlindi(sonuc) {
          
          for (var i = 1; i <= sonuc; i++) {
              $get('ctl00$cpDetail$txtTCKNoKisi' + i).className = 'textBox';
              $get('ctl00$cpDetail$txtAdiKisi' + i).className = 'textBox';
              $get('ctl00$cpDetail$txtSoyadiKisi' + i).className = 'textBox';
              $get('ctl00$cpDetail$txtDTarihiKisi' + i).className = 'textBox';
              $get('ctl00$cpDetail$txtTelNo').className = 'textBox';
              $get('ctl00$cpDetail$txtePosta').className = 'textBox';
          }
          Page_IsValid = true;
          
          if ($get('ctl00$cpDetail$txtTelNo').value == "") {
              $get('ctl00$cpDetail$txtTelNo').className = 'errStyle';
              Page_IsValid = false;
          }
          else if ($get('ctl00$cpDetail$txtTelNo').value.length != 11) {
              $get('ctl00$cpDetail$txtTelNo').className = 'errStyle';
              Page_IsValid = false;
          }
          if ($get('ctl00$cpDetail$txtePosta').value == "") {
              $get('ctl00$cpDetail$txtePosta').className = 'errStyle';
              Page_IsValid = false;
          }
          else if (!(ValidateEMail($get('ctl00$cpDetail$txtePosta').value))) {
              $get('ctl00$cpDetail$txtePosta').className = 'errStyle';
              Page_IsValid = false;
          }
          
          
          
          for (var i = 1; i <= sonuc; i++) {
              if ($get('ctl00$cpDetail$txtTCKNoKisi' + i).value == "") {
                  $get('ctl00$cpDetail$txtTCKNoKisi' + i).className = 'errStyle';
                  Page_IsValid = false;
              }
              else if ($get('ctl00$cpDetail$txtTCKNoKisi' + i).value.length != 11) {
              
              $get('ctl00$cpDetail$txtTCKNoKisi' + i).className = 'errStyle';
              Page_IsValid = false;
              }
              
              if ($get('ctl00$cpDetail$txtAdiKisi' + i).value == "") {
                  $get('ctl00$cpDetail$txtAdiKisi' + i).className = 'errStyle';
                  Page_IsValid = false;
              }
              if ($get('ctl00$cpDetail$txtAdiKisi' + i).value == "") {
                  $get('ctl00$cpDetail$txtAdiKisi' + i).className = 'errStyle';
                  Page_IsValid = false;
              }
              if ($get('ctl00$cpDetail$txtSoyadiKisi' + i).value == "") {
                  $get('ctl00$cpDetail$txtSoyadiKisi' + i).className = 'errStyle';
                  Page_IsValid = false;
              }
              if ($get('ctl00$cpDetail$txtDTarihiKisi' + i).value == "") {
                  $get('ctl00$cpDetail$txtDTarihiKisi' + i).className = 'errStyle';
                  Page_IsValid = false;
              }
             
          }
          if (Page_IsValid) {
          for (var i = 1; i <= sonuc; i++) {
              var TCKNo = $get('ctl00$cpDetail$txtTCKNoKisi' + i).value;
              var mAdi = $get('ctl00$cpDetail$txtAdiKisi' + i).value;
              var mSoyadi = $get('ctl00$cpDetail$txtSoyadiKisi' + i).value;
              var Cinsiyet = $get('ctl00$cpDetail$ddlCinsiyetKisi' + i).value;
              var DTarihi = $get('ctl00$cpDetail$txtDTarihiKisi' + i).value;
              var RbOdeme = $get('ctl00_cpDetail_rBtnOdemeKisi' + i).checked;

              
                  var Cins;
                  if (Cinsiyet == "E") {
                      Cins = 1
                  }
                  else {
                      Cins = 0
                  }
                  

                  PageMethods.AddPerson(TCKNo, mAdi, mSoyadi, DTarihi, Cins, 0, RbOdeme);
              }
              AddPersonalOk();              
          }
          else {
              alert('��aretli alanlar� do�ru olarak doldurunuz!');
          }
      }
      function AddPersonalOk() {
          AddContactInfo();
      }
      function AddContactInfo() {
          var TelNo = $get('ctl00$cpDetail$txtTelNo').value;
          var ePosta = $get('ctl00$cpDetail$txtePosta').value;
          PageMethods.AddSessionContactInfo(TelNo, ePosta, ContactInfoOk)
      }
      function ContactInfoOk() {
          PageMethods.SavePersonSesion(PersonalInfoSessionSaved)
      }
      function PersonalInfoSessionSaved() {
          PageMethods.SavePersonSesion(PersonalInfoSessionSaved)
      }
      function PersonalInfoSessionSaved() {

          window.location = "ReserveAccept.aspx";
      }
      function ValidateEMail(mail)
      {
          var filter=/^.+@.+\..{2,3}$/
          if(filter.test(mail)){
          return true;
          }
          else
          {
          return false;
          }
      
      } 
      </script>
    <table border="0" cellpadding="0" cellspacing="0" width="510px">
	<tr class="BaslikSatiri">
		<td colspan="2" align="right" style="height: 40px; padding-right:10px; background-image:url('Images/Tables/title_bar_turuncu_big.jpg');">
            <span style="font-size: 20px;">online sat��</span>
		</td>
	</tr>

    <tr style="height: 5px;"><td colspan="2"></td></tr>

	<tr class="BaslikSatiri" style="background-image:url('Images/Tables/title_bg_red.jpg');">
		<td colspan="2">
            <asp:Label ID="lblTurAdi" runat="server" Text="lblTurAdi"></asp:Label> / 
            <asp:Label ID="lblSehir" runat="server" Text="lblSehir"></asp:Label> /
            <asp:Label ID="lblBolge" runat="server" Text="lblBolge"></asp:Label>
		</td>
	</tr>

	<tr class="tdDetail" style="height: 40px;">
		<td colspan="2">
            <asp:Label ID="lblAdi" runat="server" Text="lblAdi" Font-Size="24px" ForeColor="#F03307"></asp:Label>
		</td>
	</tr>
	
	<tr class="tdDetail">
		<td colspan="2">
	        <table style="width: 510; background-image: url('Images/Tables/bg_box_noeffect_90x509.gif');">
                <tr>
                    <td style="width:15%;"><b>Oda Tipi</b></td>
                    <td style="width:2%;"><b>:</b></td>
                    <td style="width:33%;">
                        <asp:Label ID="lblOdaTipi" runat="server" Text="lblOdaTipi"></asp:Label>
                    </td>
                    
                    <td style="width:15%;"><b>Konaklama</b></td>
                    <td style="width:1%;"><b>:</b></td>
                    <td style="width:33%;">
                        <asp:Label ID="lblKonaklamaTipi" runat="server" Text="lblKonaklamaTipi"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td><b>Ki�i Say�s�</b></td>
                    <td><b>:</b></td>
                    <td>
                        <asp:Label ID="lblKisiSay" runat="server" Text="lblKisiSay"></asp:Label>
                    </td>
                    
                    <td><b>Konum</b></td>
                    <td style="width: 1%"><b>:</b></td>
                    <td>
                        <asp:Label ID="lblKonum" runat="server" Text="lblKonum"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td><b>Giri� Tarihi</b></td>
                    <td><b>:</b></td>
                    <td>
                        <asp:Label ID="lblGirisTarihi" runat="server" Text="lblGirisTarihi"></asp:Label>
                    </td>
                    
                    <td><b>��k�� Tarihi</b></td>
                    <td style="width: 1%"><b>:</b></td>
                    <td>
                        <asp:Label ID="lblCikisTarihi" runat="server" Text="lblCikisTarihi" 
                            ></asp:Label>
                    </td>
                </tr>
                  <tr>
                    <td valign="top" style="width:20%;"><b>Fiyat</b></td>
                    <td valign="top"><b>:</b></td>
                    <td colspan="4"><asp:Label ID="lblFiyat" runat="server" Text=""></asp:Label></td>
                </tr>
                <tr>
                    <td colspan="6">
                        <table border="1" width="100%" style="border-collapse:collapse;">
                            <tr style="background-color:#FF6600; color: White; font-weight:bold;">
		                        <td style="padding-left: 5px;">Tarih</td>
		                        <% if (Convert.ToInt32(Session["yetiskinSay"]) == 1) { %>
		                        <td style="padding-left: 5px;">Tek Ki�ilik Oda Ki�iba��</td>
		                        
		                        <% } else if (Convert.ToInt32(Session["yetiskinSay"]) > 1) { %>	                        
		                        <td style="padding-left: 5px;">�ki Ki�ilik Odada Ki�iba��</td>
		                        
		                        <% if ((Convert.ToInt32(Session["cocukSay"]) == 0) && (Convert.ToInt32(Session["yetiskinSay"]) >= 3)) { %>	
		                        <td style="padding-left: 5px;">�lave Ki�i</td>
		                        
		                        <% } else if ((Convert.ToInt32(Session["cocukSay"]) > 0) && (Convert.ToInt32(Session["yetiskinSay"]) == 3)) { %>
		                        <td style="padding-left: 5px;">�lave Ki�i</td>
		                        <td style="padding-left: 5px;">�ocuk</td>
		                        
		                        <% } else if (Convert.ToInt32(Session["cocukSay"]) > 0) { %>
		                        <td style="padding-left: 5px;">�ocuk</td>
		                        <% }} %>
	                        </tr>
                        <% for (int i = 0; i < ReserveGunSay; i++){ %>
	                        <tr>
		                        <td style="padding-left: 5px;"><% Response.Write(Convert.ToDateTime(Session["GirisTarihi"].ToString()).AddDays(i).ToLongDateString()); %></td>
		                        <% if (Convert.ToInt32(Session["yetiskinSay"]) == 1) { %>
		                        <td style="padding-left: 5px;"><% =Session["fiyat1P"].ToString() %> YTL</td>
		                        
		                        <% } else if (Convert.ToInt32(Session["yetiskinSay"]) > 1) { %>	                        
		                        <td style="padding-left: 5px;"><% =Session["fiyat2P"].ToString() %> YTL</td>
		                        
		                        <% if ((Convert.ToInt32(Session["cocukSay"]) == 0) && (Convert.ToInt32(Session["yetiskinSay"]) >= 3)) { %>	
		                        <td style="padding-left: 5px;"><% =Session["ilaveYatak"].ToString() %> YTL</td>
		                        
		                        <% } else if ((Convert.ToInt32(Session["cocukSay"]) > 0) && (Convert.ToInt32(Session["yetiskinSay"]) == 3)) { %>
		                        <td style="padding-left: 5px;"><% =Session["ilaveYatak"].ToString() %> YTL</td>
		                        <td style="padding-left: 5px;">
		                        <% foreach (string CocukFiyat in arrayCocukFiyat) {
                                       if (CocukFiyat != string.Empty)
                                           Response.Write(CocukFiyat);
                                } %>
		                        </td>
		                        
		                        <% } else if (Convert.ToInt32(Session["cocukSay"]) > 0) { %>
		                        <td style="padding-left: 5px;">
		                        <% foreach (string CocukFiyat in arrayCocukFiyat) {
                                       if (CocukFiyat != string.Empty)
                                           Response.Write(CocukFiyat);
                                } %>
		                        </td>
		                        <% }} %>
	                        </tr>
	                    <% } %>
                        </table>
                    </td>
                </tr>                
            </table>
         </td>
	</tr>
	<tr class="tdDetail">
		<td colspan="2">
		<fieldset class="Tah11" style=" padding:5px 0px 15px 10px; width: 100%; border-color: #FF6600; border-top-style: solid;
			border-left-width: 0px; border-right-width: 0px; border-top-width: 2px; border-bottom-width: 1px">
			<legend><img src="Images/Legend/Users.png" style="border: none;" alt="" /></legend>
            <asp:Table ID="TblPersonal" runat="server" border="0" width="98%" style="border-collapse: collapse;">
            <asp:TableRow style="background-color:#FF6600; color: White; font-weight:bold;">
            <asp:TableCell></asp:TableCell>
            <asp:TableCell><center>TC Kimlik No</center></asp:TableCell>
            <asp:TableCell><center>Ad�</center></asp:TableCell>
            <asp:TableCell><center>Soyad�</center></asp:TableCell>
            <asp:TableCell><center>Cinsiyet</center></asp:TableCell>
            <asp:TableCell><center>Do�um Tarihi</center></asp:TableCell>
            <asp:TableCell><center>�deme</center></asp:TableCell>
            </asp:TableRow>
            </asp:Table>
            </fieldset>                
            <p><span style="color: #F03307;">Yanl�� girilen bilgilerden kaynaklanabilecek problemlerden CengTur sorumlu tutulamaz.</span>
               L�tfen ileti�im i�in telefon numaran�z� ve e-Posta adresinizi giriniz.
            </p>
            <table border="0" width="98%" style="border-collapse: collapse;">
                <tr>
                    <td>Telefon No</td>
                    <td>:</td>
                    <td style="width: 80%">
                        <asp:TextBox ID="txtTelNo" runat="server" Width="200px" Height="14px" MaxLength="11" CssClass="textBox" onkeypress="var k = event.keyCode ? event.keyCode : event.charCode ? event.charCode : event.which; return /^(\d)$/.test( String.fromCharCode(k) );"></asp:TextBox>
                         <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" 
                            runat="server" Enabled="True" TargetControlID="txtTelNo" 
                            WatermarkCssClass="WaterMark" WatermarkText="Telefon numaran�z� giriniz...">
                        </cc1:TextBoxWatermarkExtender>
                        (�rn. 02462326182)
                    </td>
                </tr>
                <tr>
                    <td>e-Posta Adresi</td>
                    <td>:</td>
                    <td><asp:TextBox ID="txtePosta" runat="server" Width="200px" Height="14px" CssClass="textBox"></asp:TextBox>
                        <cc1:TextBoxWatermarkExtender ID="txtePosta_TextBoxWatermarkExtender" 
                            runat="server" Enabled="True" TargetControlID="txtePosta" 
                            WatermarkCssClass="WaterMark" WatermarkText="E-Posta adresinizi giriniz...">
                        </cc1:TextBoxWatermarkExtender>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3"><b>Not:</b> Telefon numaras�n�n, size ula�mam�z i�in mutlaka do�ru girilmesi gerekmektedir.</td>
                </tr>
                <tr>
	                <td colspan="3" align="right">
	                   
                        <img src="Images/Buttons/continue_button.gif" onclick="AddPersonalInfo()" />
                    </td>
                </tr>
            </table>
		</td>
	</tr>
</table>
</asp:Content>

