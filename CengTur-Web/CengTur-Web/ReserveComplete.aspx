<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ReserveComplete.aspx.cs" Inherits="Default5" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpDetail" Runat="Server">

    <script type="text/javascript" language="javascript">
    function PrintForm() {
        window.open("ReservePrint.aspx", "Window1", "menubar=no,width=520,height=360,toolbar=no scrollbars=1");

    }
</script>
    <table border="0" cellpadding="0" cellspacing="0" width="510px">
    <tr class="BaslikSatiri">
		<td colspan="2" align="right" style="height: 40px; padding-right:10px; background-image:url('Images/Tables/title_bar_turuncu_big.jpg');">
            <span style="font-size: 20px;">online sat��</span>
		</td>
	</tr>
	<tr>
	<td class="tdDetail" 
            style="background-image: url('Images/Tables/bg_box_noeffect_90x509.gif'); border-bottom-style: solid; border-bottom-color: #FF6600; border-bottom-width: 3px;">
	<br />
    <center><asp:Image ID="Image1" runat="server" ImageUrl="Images/Reserve/completed.gif" 
    Height="93px" Width="271px" />
    <br />
    <br />
    </center>
	</td>
	</tr>
	<tr class="BaslikSatiri" style="background-image:url('Images/Tables/title_bg_yesil.jpg');">
		<td colspan="4">
            Rezervasyon Bilgileri
		</td>
	</tr>
	<tr>
	<td align="center">
	<asp:Label ID="lblAdi" runat="server" Text="Otel Rezervasyon Formu" Font-Size="24px" ForeColor="#F03307"></asp:Label>
	</td>
	</tr>
	<tr>
	<td>
	<table style="width: 100%; background-image: url('Images/Tables/bg_box_noeffect_90x509.gif');">
                    <tr>
	<td>
	<b>Otel Ad�</b>
	</td>
	<td><b>:</b></td>
	<td colspan="4">
	<asp:Label ID="lblOtelAdi" runat="server" Text="lblOtelAdi" CssClass="text"></asp:Label>
	</td>
	</tr>
                <tr>
                    <td style="width:15%;"><b>Oda Tipi</b></td>
                    <td style="width:2%;"><b>:</b></td>
                    <td style="width:33%;">
                        <asp:Label ID="lblOdaTipi" runat="server" Text="lblOdaTipi"></asp:Label>
                    </td>
                    
                    <td style="width:15%;"><b>Konaklama</b></td>
                    <td style="width:2%;"><b>:</b></td>
                    <td style="width:33%;">
                        <asp:Label ID="lblKonaklamaTipi" runat="server" Text="lblKonaklamaTipi"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td><b>Ki�i Say�s�</b></td>
                    <td><b>:</b></td>
                    <td>
                        <asp:Label ID="lblKisiSay" runat="server" Text="lblKisiSay"></asp:Label>
                    </td>
                    
                    <td><b>Konum</b></td>
                    <td><b>:</b></td>
                    <td>
                        <asp:Label ID="lblKonum" runat="server" Text="lblKonum"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td><b>Giri� Tarihi</b></td>
                    <td><b>:</b></td>
                    <td>
                        <asp:Label ID="lblGirisTarihi" runat="server" Text="lblGirisTarihi"></asp:Label>
                    </td>
                    
                    <td><b>��k�� Tarihi</b></td>
                    <td><b>:</b></td>
                    <td>
                        <asp:Label ID="lblCikisTarihi" runat="server" Text="lblCikisTarihi"></asp:Label>
                    </td>
                </tr>
                
                  <tr>
                    <td valign="top" style="width:20%;"><b>Fiyat</b></td>
                    <td valign="top"><b>:</b></td>
                    <td colspan="4"><asp:Label ID="lblFiyat" runat="server" Text=""></asp:Label></td>
                </tr>
<%--	<tr>
	<td>
	<b>Adres</b>
	</td>
	<td>:</td>
	<td colspan="4"><%=Adres %></td>
	</tr>--%>
                
                <tr>
                    <td colspan="6">
             <fieldset class="Tah11" style=" padding:5px 0px 15px 10px; width: 100%; border-color: #FF6600; border-top-style: solid;
			border-left-width: 0px; border-right-width: 0px; border-top-width: 2px; border-bottom-width: 1px">
			<legend><img src="Images/Legend/Users.png" style="border: none;" alt="" /></legend>
            <asp:Table ID="TblPersonal" runat="server" border="0" width="98%" style="border-collapse: collapse;">
            <asp:TableRow style="background-color:#FF6600; color: White; font-weight:bold;">
            <asp:TableCell></asp:TableCell>
            <asp:TableCell><center>TC Kimlik No</center></asp:TableCell>
            <asp:TableCell><center>Ad�</center></asp:TableCell>
            <asp:TableCell><center>Soyad�</center></asp:TableCell>
            <asp:TableCell><center>Cinsiyet</center></asp:TableCell>
            <asp:TableCell><center>Do�um Tarihi</center></asp:TableCell>
            <asp:TableCell><center>�deme</center></asp:TableCell>
            </asp:TableRow>
            </asp:Table>
            </fieldset> 
            <br />
             <p><span style="color: #F03307;">Yukar�daki bilgileri kontrol ediniz e�er yanl�� bilgi giri�i varsa k�sa s�re i�inde bize bildiriniz.</span>
               <br />
               <br />
            </p>
                    </td>
                </tr>
                <tr>
                <td colspan="6">
                <table border="0" width="98%" style="border-collapse: collapse;">
                <tr>
                    <td><b>Telefon No</b></td>
                    <td>:</td>
                    <td style="width: 80%">
                        <asp:Label ID="lblTelefon" runat="server" Text="lblTelefon"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td><b>e-Posta Adresi</b></td>
                    <td>:</td>
                    <td>
                        <asp:Label ID="lblEPosta" runat="server" Text="lblEPosta"></asp:Label></td>
                </tr>
                <tr>
         
	                <td colspan="3" align="right">	                   
	                <br />
                        <img alt="Rezervasyon bilgilerini yazd�r." src="Images/Buttons/print_button.gif" onclick="PrintForm()"/>
                    </td>
                </tr>
            </table>
                </td>
                </tr>                
            </table>
	</td>
	</tr>
	</table>
</asp:Content>

