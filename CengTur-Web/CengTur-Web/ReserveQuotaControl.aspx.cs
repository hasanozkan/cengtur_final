#region Using Tan�mlamalar�
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.Services;
using eWorld.UI;
#endregion

public partial class ReserveQuotaControl : System.Web.UI.Page
{
    static int ToplamKisi;
    string OpenId;
    public int ReserveGunSay = 0;
    public DataTable DT_YetiskinFiyat, DT_CocukFiyat;
    public string[] arrayCocukFiyat;
    
    [WebMethod]
    public static int TotalPerson()
    {
        return ToplamKisi;
    }
    protected void Page_Load(object sender, EventArgs e)
    {

        /*No Caching---------------------------------------*/
        Response.ExpiresAbsolute = DateTime.Now.AddDays(-1d);
        Response.Expires = -1500;
        Response.CacheControl = "no-cache";
        /*-------------------------------------------------*/

        CreateSesionTable();

        try
        {
            OpenId = Session["OpenId"].ToString();
            lblTurAdi.Text = Session["TurAdi"].ToString();
            lblSehir.Text = Session["Sehir"].ToString();
            lblBolge.Text = Session["Bolge"].ToString();
            lblAdi.Text = Session["Adi"].ToString();

            lblOdaTipi.Text = Session["OdaTipAdi"].ToString();
            lblKonaklamaTipi.Text = Session["HizmetAdi"].ToString();

            if (Session["cocukSay"].ToString() == "0")
                lblKisiSay.Text = Session["yetiskinSay"].ToString() + " yeti�kin";
            else
                lblKisiSay.Text = Session["yetiskinSay"].ToString() + " yeti�kin + " + Session["cocukSay"].ToString() + " �ocuk";

            lblKonum.Text = Session["Sehir"].ToString() + " / " + Session["Bolge"].ToString();
            lblGirisTarihi.Text = Session["GirisTarihi"].ToString();
            lblCikisTarihi.Text = Session["CikisTarihi"].ToString();

            ReserveGunSay = (Convert.ToDateTime(lblCikisTarihi.Text) - Convert.ToDateTime(lblGirisTarihi.Text)).Days;
            
            ToplamKisi = Convert.ToInt32(Session["yetiskinSay"].ToString()) + Convert.ToInt32(Session["cocukSay"].ToString());

            try
            {
                if (Request.QueryString["Mode"] == "Edit")
                {
                    GenareteEditInfoTable();
                }
                else
                {
                    GeneratePersonalInfoTable();
                }
            }
            catch (Exception)
            {


            }
            

            /*Fiyat Listesi******/
            FiyatListesi();
            lblFiyat.Text = Session["PriceText"].ToString();
            Session["ToplamUcret"] = GetTotalPrice().ToString();
            
            
        }
        catch (Exception)
        {
            Response.Redirect("");
        }

        //D�zenleme Modu


    }

    #region Kimlik Bilgileri Duzenleme Tablosu Olustur
    private void GenareteEditInfoTable()
    {
        DataTable Persons = (DataTable)Session["KisiBilgileri"];
        for (int i = 1; i <= Persons.Rows.Count; i++)
        {
            TableRow satir = new TableRow();
            TableCell hucre = new TableCell();

            Label lblKisi = new Label();
            lblKisi.Text = i.ToString() + ".Ki�i";

            MaskedTextBox txtTCKNoKisi = new MaskedTextBox();
            txtTCKNoKisi.Width = 65;
            txtTCKNoKisi.Height = 14;
            txtTCKNoKisi.MaxLength = 11;
            txtTCKNoKisi.Mask = "99999999999";
            txtTCKNoKisi.CssClass = "textBox";
            txtTCKNoKisi.ID = "txtTCKNoKisi" + i.ToString();
            txtTCKNoKisi.Text = Persons.Rows[i-1][0].ToString();

            TextBox txtAdiKisi = new TextBox();
            txtAdiKisi.Width = 65;
            txtAdiKisi.Height = 14;
            txtAdiKisi.CssClass = "textBox";
            txtAdiKisi.ID = "txtAdiKisi" + i.ToString();
            txtAdiKisi.Text = Persons.Rows[i-1][1].ToString();

            TextBox txtSoyadiKisi = new TextBox();
            txtSoyadiKisi.Width = 65;
            txtSoyadiKisi.Height = 14;
            txtSoyadiKisi.CssClass = "textBox";
            txtSoyadiKisi.ID = "txtSoyadiKisi" + i.ToString();
            txtSoyadiKisi.Text = Persons.Rows[i-1][2].ToString();

            DropDownList ddlCinsiyetKisi = new DropDownList();
            ddlCinsiyetKisi.Items.Add("E");
            ddlCinsiyetKisi.Items.Add("K");
            ddlCinsiyetKisi.ID = "ddlCinsiyetKisi" + i.ToString();
            if ((Convert.ToByte(Persons.Rows[i-1][4].ToString())) == 1)
            {
                ddlCinsiyetKisi.SelectedIndex = 0;
            }
            else
            {
                ddlCinsiyetKisi.SelectedIndex = 1;
            }


            MaskedTextBox txtDTarihiKisi = new MaskedTextBox();
            txtDTarihiKisi.Width = 65;
            txtDTarihiKisi.Height = 14;
            txtDTarihiKisi.Mask = "99.99.9999";
            txtDTarihiKisi.CssClass = "textBox";
            txtDTarihiKisi.ID = "txtDTarihiKisi" + i.ToString();
            txtDTarihiKisi.Text = Persons.Rows[i-1][3].ToString();

            
            

            RadioButton rBtnOdemeKisi = new RadioButton();
            rBtnOdemeKisi.ID = "rBtnOdemeKisi" + i.ToString();

            if (Convert.ToBoolean(Persons.Rows[i-1][6].ToString()))
            {
                rBtnOdemeKisi.Checked = true;
            }
            else
            {
                rBtnOdemeKisi.Checked = false;
            }

            hucre.Controls.Add(lblKisi);
            hucre.CssClass = "Tablom";
            satir.Cells.Add(hucre);

            hucre = new TableCell();
            hucre.Controls.Add(txtTCKNoKisi);
            hucre.CssClass = "Tablom";
            satir.Cells.Add(hucre);

            hucre = new TableCell();
            hucre.Controls.Add(txtAdiKisi);
            hucre.CssClass = "Tablom";
            satir.Cells.Add(hucre);

            hucre = new TableCell();
            hucre.Controls.Add(txtSoyadiKisi);
            hucre.CssClass = "Tablom";
            satir.Cells.Add(hucre);

            hucre = new TableCell();
            hucre.Controls.Add(ddlCinsiyetKisi);
            hucre.CssClass = "Tablom";
            satir.Cells.Add(hucre);

            hucre = new TableCell();
            
            hucre.Controls.Add(txtDTarihiKisi);
            
            hucre.CssClass = "Tablom";
            satir.Cells.Add(hucre);

            hucre = new TableCell();
            rBtnOdemeKisi.GroupName = "BtnOdemeKisiGroup";
         
            hucre.Controls.Add(rBtnOdemeKisi);
            hucre.CssClass = "Tablom";
            satir.Cells.Add(hucre);

            TblPersonal.Rows.Add(satir);
        }
        txtTelNo.Text = Session["TelNo"].ToString();
        txtePosta.Text = Session["ePosta"].ToString();

    }
    #endregion

    #region Kimlik Kontrol Tablosu Olustur
    private void GeneratePersonalInfoTable()
    {

        //Tablo i�lemleri
        for (int i = 1; i <= ToplamKisi; i++)
        {
            TableRow satir = new TableRow();
            TableCell hucre = new TableCell();

            Label lblKisi = new Label();
            lblKisi.Text = i.ToString() + ".Ki�i";

            MaskedTextBox txtTCKNoKisi = new MaskedTextBox();
            txtTCKNoKisi.Width = 65;
            txtTCKNoKisi.Height = 14;
            txtTCKNoKisi.MaxLength = 11;
            txtTCKNoKisi.Mask = "99999999999";
            txtTCKNoKisi.CssClass = "textBox";
            txtTCKNoKisi.ID = "txtTCKNoKisi" + i.ToString();
            

            TextBox txtAdiKisi = new TextBox();
            txtAdiKisi.Width = 65;
            txtAdiKisi.Height = 14;
            txtAdiKisi.CssClass = "textBox";
            txtAdiKisi.ID = "txtAdiKisi" + i.ToString();

            TextBox txtSoyadiKisi = new TextBox();
            txtSoyadiKisi.Width = 65;
            txtSoyadiKisi.Height = 14;
            txtSoyadiKisi.CssClass = "textBox";
            txtSoyadiKisi.ID = "txtSoyadiKisi" + i.ToString();

            DropDownList ddlCinsiyetKisi = new DropDownList();
            ddlCinsiyetKisi.Items.Add("E");
            ddlCinsiyetKisi.Items.Add("K");
            ddlCinsiyetKisi.ID = "ddlCinsiyetKisi" + i.ToString();

            MaskedTextBox txtDTarihiKisi = new MaskedTextBox();
            txtDTarihiKisi.Width = 65;
            txtDTarihiKisi.Height = 14;
            txtDTarihiKisi.Mask = "99.99.9999";
            txtDTarihiKisi.CssClass = "textBox";
            txtDTarihiKisi.ID = "txtDTarihiKisi" + i.ToString();

            RadioButton rBtnOdemeKisi = new RadioButton();
            rBtnOdemeKisi.ID = "rBtnOdemeKisi" + i.ToString();



            hucre.Controls.Add(lblKisi);
            hucre.CssClass = "Tablom";
            satir.Cells.Add(hucre);

            hucre = new TableCell();
            hucre.Controls.Add(txtTCKNoKisi);
            hucre.CssClass = "Tablom";
            satir.Cells.Add(hucre);

            hucre = new TableCell();
            hucre.Controls.Add(txtAdiKisi);
            hucre.CssClass = "Tablom";
            satir.Cells.Add(hucre);

            hucre = new TableCell();
            hucre.Controls.Add(txtSoyadiKisi);
            hucre.CssClass = "Tablom";
            satir.Cells.Add(hucre);

            hucre = new TableCell();
            hucre.Controls.Add(ddlCinsiyetKisi);
            hucre.CssClass = "Tablom";
            satir.Cells.Add(hucre);

            hucre = new TableCell();
            hucre.Controls.Add(txtDTarihiKisi);
            hucre.CssClass = "Tablom";
            satir.Cells.Add(hucre);

            hucre = new TableCell();
            rBtnOdemeKisi.GroupName = "BtnOdemeKisiGroup";
            if (i == 1)
            {
                rBtnOdemeKisi.Checked = true;
            }
            else
            {
                rBtnOdemeKisi.Checked = false;
            }
            hucre.Controls.Add(rBtnOdemeKisi);
            hucre.CssClass = "Tablom";
            satir.Cells.Add(hucre);

            TblPersonal.Rows.Add(satir);
        }
    }
    #endregion

    protected void FiyatListesi()
    {
        /* Yeti�kin Fiyat Listesi******/
        string Sql = "SELECT oodId, fiyat1P, fiyat2P, ilaveYatak FROM OpenOtelDetails, OdaTipi "
                   + "WHERE (OpenOtelDetails.tipKodu = OdaTipi.tipKodu) "
                   + "AND (OpenOtelDetails.OpenId = '" + OpenId + "') "
                   + "AND (OpenOtelDetails.tipKodu = (SELECT tipKodu FROM OdaTipi WHERE tipAdi = '" + Session["OdaTipAdi"].ToString() + "'))";
        DT_YetiskinFiyat = CengTur.GetDataTable(Sql);

        Session["oodId"] = DT_YetiskinFiyat.Rows[0]["oodId"].ToString();
        Session["fiyat1P"] = DT_YetiskinFiyat.Rows[0]["fiyat1P"].ToString();
        Session["fiyat2P"] = DT_YetiskinFiyat.Rows[0]["fiyat2P"].ToString();
        Session["ilaveYatak"] = DT_YetiskinFiyat.Rows[0]["ilaveYatak"].ToString();

        /* �ocuk Fiyat Listesi******/
        Sql = "SELECT free1age, free2age, indirim2age FROM CocukIndirim, OpenOtelDetails "
            + "WHERE (CocukIndirim.OpenId = OpenOtelDetails.OpenId) "
            + "AND (CocukIndirim.OpenId = '" + OpenId + "')";
        DT_CocukFiyat = CengTur.GetDataTable(Sql);
        Session["free1age"] = DT_CocukFiyat.Rows[0]["free1age"].ToString();
        Session["free2age"] = DT_CocukFiyat.Rows[0]["free2age"].ToString();
        Session["indirim2age"] = DT_CocukFiyat.Rows[0]["indirim2age"].ToString();
        arrayCocukFiyat = new string[3];
        if (DT_CocukFiyat.Rows.Count > 0)
        {
            if (Convert.ToInt32(Session["cocukSay"]) == 1)
            {
                int Yas = CengTur.AgeSpan(Convert.ToDateTime(Session["DTCocuk1"].ToString()), DateTime.Now);
                if (Yas >= 0 && Yas <= Convert.ToInt32(Session["free1age"]))
                {
                    arrayCocukFiyat[0] = "1. �ocuk (" + Yas + " Ya��nda): 0 YTL";
                }

            }
            else if (Convert.ToInt32(Session["cocukSay"]) == 2)
            {
                if (CengTur.AgeSpan(Convert.ToDateTime(Session["DTCocuk1"].ToString()), DateTime.Now) < CengTur.AgeSpan(Convert.ToDateTime(Session["DTCocuk2"].ToString()), DateTime.Now))
                {
                    string DTCocuk1 = Session["DTCocuk1"].ToString();
                    string DTCocuk2 = Session["DTCocuk2"].ToString();
                    Session["DTCocuk1"] = DTCocuk2;
                    Session["DTCocuk2"] = DTCocuk1;
                }
                int Yas = CengTur.AgeSpan(Convert.ToDateTime(Session["DTCocuk1"].ToString()), DateTime.Now);
                arrayCocukFiyat[0] = "1. �ocuk (" + Yas + " Ya��nda): 0 YTL"; 
                int Yas2 = CengTur.AgeSpan(Convert.ToDateTime(Session["DTCocuk2"].ToString()), DateTime.Now);
                if (Yas2 >= 0 && Yas2 <= Convert.ToInt32(Session["free2age"]))
                {
                    arrayCocukFiyat[1] = "<br />2. �ocuk (" + Yas2 + " Ya��nda): 0 YTL";
                }
                else if (Yas2 >= (Convert.ToInt32(Session["free2age"]) + 1) && Yas2 <= Convert.ToInt32(Session["indirim2age"]))
                {
                    arrayCocukFiyat[1] = "<br />2. �ocuk (" + Yas2 + " Ya��nda): " + System.Math.Round(Convert.ToDouble(Session["fiyat2P"]) / 2, 1, MidpointRounding.AwayFromZero).ToString() + " YTL";
                }
            }
        }
    }
    private Double GetTotalPrice()
    {
        Double ToplamFiyat=0;
        if (Convert.ToInt32(Session["yetiskinSay"]) == 1) 
        {
            ToplamFiyat += Convert.ToDouble(Session["fiyat1P"]) * ReserveGunSay;
        } 
        else if (Convert.ToInt32(Session["yetiskinSay"]) > 1) 
        {
            if (Convert.ToInt32(Session["yetiskinSay"])<3)
            {
                ToplamFiyat += (2* Convert.ToDouble(Session["fiyat2P"]))*ReserveGunSay;
            } 
            else if ((Convert.ToInt32(Session["yetiskinSay"]) >= 3)) 
            {
                ToplamFiyat += (2 * Convert.ToDouble(Session["fiyat2P"])) * ReserveGunSay;
                ToplamFiyat += ((Convert.ToInt32(Session["yetiskinSay"])) - 2) * Convert.ToDouble(Session["ilaveYatak"]) * ReserveGunSay;
            }
        }
        if (Convert.ToInt32(Session["cocukSay"]) > 0)
        {
            if (Convert.ToInt32(Session["cocukSay"]) == 1)
            {
                int Yas = CengTur.AgeSpan(Convert.ToDateTime(Session["DTCocuk1"].ToString()), DateTime.Now);
                if (Yas >= 0 && Yas <= Convert.ToInt32(Session["free1age"]))
                {
                    ToplamFiyat += 0;
                }

            }
            else if (Convert.ToInt32(Session["cocukSay"]) == 2)
            {
                if (CengTur.AgeSpan(Convert.ToDateTime(Session["DTCocuk1"].ToString()), DateTime.Now) < CengTur.AgeSpan(Convert.ToDateTime(Session["DTCocuk2"].ToString()), DateTime.Now))
                {
                    string DTCocuk1 = Session["DTCocuk1"].ToString();
                    string DTCocuk2 = Session["DTCocuk2"].ToString();
                    Session["DTCocuk1"] = DTCocuk2;
                    Session["DTCocuk2"] = DTCocuk1;
                }
                int Yas = CengTur.AgeSpan(Convert.ToDateTime(Session["DTCocuk1"].ToString()), DateTime.Now);
                ToplamFiyat += 0;
                int Yas2 = CengTur.AgeSpan(Convert.ToDateTime(Session["DTCocuk2"].ToString()), DateTime.Now);
                if (Yas2 >= 0 && Yas2 <= Convert.ToInt32(Session["free2age"]))
                {
                    ToplamFiyat += 0;
                }
                else if (Yas2 >= (Convert.ToInt32(Session["free2age"]) + 1) && Yas2 <= Convert.ToInt32(Session["indirim2age"]))
                {
                    ToplamFiyat += System.Math.Round(Convert.ToDouble(Session["fiyat2P"]) / 2, 1, MidpointRounding.AwayFromZero) * ReserveGunSay;
                }
            }
        }
        
        return ToplamFiyat;
    }
    [WebMethod]
    public static void AddSessionContactInfo(string TelNo, string ePosta)
    {
        HttpContext.Current.Session["TelNo"] = TelNo;
        HttpContext.Current.Session["ePosta"] = ePosta;
    }

    #region Kisileri �nbellekleme

    static DataTable dt;

    private void CreateSesionTable()
    {
        dt = new DataTable();
        dt.Columns.Add("TCKimlik");
        dt.Columns.Add("Ad");
        dt.Columns.Add("Soyad");
        dt.Columns.Add("DTarih");
        dt.Columns.Add("Cins");
        dt.Columns.Add("Tur");
        dt.Columns.Add("Odeme");
        dt.Rows.Clear(); 
    }
    [WebMethod]
    public static void AddPerson(string TCKimlik, string Ad, string Soyad, string DTarih, byte cins, byte Tur, bool odeme)
    {
        DataRow dr = dt.NewRow();
        dr["TCKimlik"] = TCKimlik;
        dr["Ad"] = Ad;
        dr["Soyad"] = Soyad;
        dr["DTarih"] = DTarih;
        dr["Cins"] = cins;
        dr["Tur"] = Tur;
        dr["Odeme"] = odeme;
        dt.Rows.Add(dr);
    }
    [WebMethod]
    public static void SavePersonSesion()
    {
        HttpContext.Current.Session.Remove("KisiBilgileri");
        HttpContext.Current.Session["KisiBilgileri"] = dt;
    }
    #endregion
}
