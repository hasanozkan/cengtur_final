﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ReserveAccept.aspx.cs" Inherits="Default3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpDetail" Runat="Server">
    <table border="0" cellpadding="0" cellspacing="0" width="510px">
    <tr class="BaslikSatiri">
		<td colspan="2" align="right" style="height: 40px; padding-right:10px; background-image:url('Images/Tables/title_bar_turuncu_big.jpg');">
            <span style="font-size: 20px;">online satış</span>
		</td>
	</tr>
	 <tr style="height: 5px;"><td colspan="2"></td></tr>

	<tr class="BaslikSatiri" style="background-image:url('Images/Tables/title_bg_red.jpg');">
		<td colspan="2">
            Rezervasyon Onayı
		</td>
	</tr>
	<tr>
	<td align="center">
	<asp:Label ID="lblAdi" runat="server" Text="Rezervasyon Bilgileri" Font-Size="24px" ForeColor="#F03307"></asp:Label>
	</td>
	</tr>
	<tr>
	<td>
	<table style="width: 100%; background-image: url('Images/Tables/bg_box_noeffect_90x509.gif');">
	               <tr>
	<td>
	<b>Otel Adı</b>
	</td>
	<td><b>:</b></td>
	<td colspan="4">
	<asp:Label ID="lblOtelAdi" runat="server" Text="lblOtelAdi" CssClass="text"></asp:Label>
	</td>
	</tr>
     <tr>
                    <td style="width:15%;"><b>Oda Tipi</b></td>
                    <td style="width:2%;"><b>:</b></td>
                    <td style="width:33%;">
                        <asp:Label ID="lblOdaTipi" runat="server" Text="lblOdaTipi"></asp:Label>
                    </td>
                    
                    <td style="width:15%;"><b>Konaklama</b></td>
                    <td style="width:2%;"><b>:</b></td>
                    <td style="width:33%;">
                        <asp:Label ID="lblKonaklamaTipi" runat="server" Text="lblKonaklamaTipi"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td><b>Kişi Sayısı</b></td>
                    <td><b>:</b></td>
                    <td>
                        <asp:Label ID="lblKisiSay" runat="server" Text="lblKisiSay"></asp:Label>
                    </td>
                    
                    <td><b>Konum</b></td>
                    <td><b>:</b></td>
                    <td>
                        <asp:Label ID="lblKonum" runat="server" Text="lblKonum"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td><b>Giriş Tarihi</b></td>
                    <td><b>:</b></td>
                    <td>
                        <asp:Label ID="lblGirisTarihi" runat="server" Text="lblGirisTarihi"></asp:Label>
                    </td>
                    
                    <td><b>Çıkış Tarihi</b></td>
                    <td><b>:</b></td>
                    <td>
                        <asp:Label ID="lblCikisTarihi" runat="server" Text="lblCikisTarihi"></asp:Label>
                    </td>
                </tr>
                  <tr>
                    <td valign="top" style="width:20%;"><b>Fiyat</b></td>
                    <td valign="top"><b>:</b></td>
                    <td colspan="4"><asp:Label ID="lblFiyat" runat="server" Text=""></asp:Label></td>
                </tr>
                
                <tr>
                    <td colspan="6">
             <fieldset class="Tah11" style=" padding:5px 0px 15px 10px; width: 100%; border-color: #FF6600; border-top-style: solid;
			border-left-width: 0px; border-right-width: 0px; border-top-width: 2px; border-bottom-width: 1px">
			<legend><img src="Images/Legend/Users.png" style="border: none;" alt="" /></legend>
            <asp:Table ID="TblPersonal" runat="server" border="0" width="98%" style="border-collapse: collapse;">
            <asp:TableRow style="background-color:#FF6600; color: White; font-weight:bold;">
            <asp:TableCell></asp:TableCell>
            <asp:TableCell><center>TC Kimlik No</center></asp:TableCell>
            <asp:TableCell><center>Adı</center></asp:TableCell>
            <asp:TableCell><center>Soyadı</center></asp:TableCell>
            <asp:TableCell><center>Cinsiyet</center></asp:TableCell>
            <asp:TableCell><center>Doğum Tarihi</center></asp:TableCell>
            <asp:TableCell><center>Ödeme</center></asp:TableCell>
            </asp:TableRow>
            </asp:Table>
            </fieldset> 
            <br />
             <p><span style="color: #F03307;">Yukarıdaki bilgileri kontrol ediniz eğer yanlış bilgi girişi varsa kısa süre içinde bize bildiriniz.</span>
               <br />
               <br />
            </p>
                    </td>
                </tr>
                <tr>
                <td colspan="6">
                <table border="0" width="98%" style="border-collapse: collapse;">
                <tr>
                    <td><b>Telefon No</b></td>
                    <td>:</td>
                    <td style="width: 80%">
                        <asp:Label ID="lblTelefon" runat="server" Text="lblTelefon"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td><b>e-Posta Adresi</b></td>
                    <td>:</td>
                    <td>
                        <asp:Label ID="lblEPosta" runat="server" Text="lblEPosta"></asp:Label></td>
                </tr>
                <tr>
                 <td colspan="2" align="left">	                   
	                <br />
                     <asp:ImageButton ID="imgEdit" ImageUrl="Images/Buttons/edit_button.gif" 
                         runat="server" onclick="imgEdit_Click" />
                    
                    </td>
	                <td colspan="3" align="right">	                   
	                <br />
	                <asp:ImageButton ID="imgAccept" ImageUrl="Images/Buttons/accept_button.gif" 
                            runat="server" onclick="imgAccept_Click" />
                    </td>
                </tr>
            </table>
                </td>
                </tr>                
            </table>
	</td>
	</tr>
	</table>
</asp:Content>

