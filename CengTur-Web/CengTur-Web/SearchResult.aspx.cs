#region Using Tanýmlamalarý
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.Sql;
using System.Data.SqlClient;
#endregion

public partial class _SearchResult : System.Web.UI.Page
{
    public SqlDataReader DR_HitHotel;

    protected void Page_Load(object sender, EventArgs e)
    {
        //try
        //{
        //    GetResults();
        //}
        //catch (Exception)
        //{
        //    Response.Redirect("");
        //}
        GetResults();
    }
    public void GetResults()
    {
        string[] Params = CengTur.GetQueryParam(Request.QueryString[0]);

        lblTurAdi.Text = (CengTur.GetDataTable("SELECT TurAdi FROM Turlar WHERE TurKodu='" + Params[0] +"'").Rows[0][0].ToString());
        lblSehir.Text = (CengTur.GetDataTable("SELECT SAdi FROM Sehir WHERE SKodu='" + Params[1] + "'").Rows[0][0].ToString());
        lblBolge.Text = (CengTur.GetDataTable("SELECT BAdi FROM Bolge WHERE BKodu='" + Params[2] + "'").Rows[0][0].ToString());

        DR_HitHotel = CengTur.SearchHotel(Params[0], Params[1], Params[2]);
    }
}