<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="CustomerRelations.aspx.cs" Inherits="Default2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpDetail" Runat="Server">
<script type="text/javascript" language="javascript">
        function Detay(comment) {
        if (document.getElementById(comment+'Full').style.display == 'none')
        {
          
            document.getElementById(comment + 'Full').style.display = '';
            document.getElementById(comment + 'Devam').style.display = 'none';
            document.getElementById(comment + 'Daralt').style.display = '';
            document.getElementById(comment + 'Head').style.display = 'none';
        }
        else {
            
            document.getElementById(comment + 'Full').style.display = 'none';
            document.getElementById(comment + 'Devam').style.display = '';
            document.getElementById(comment + 'Daralt').style.display = 'none';
            document.getElementById(comment + 'Head').style.display = '';
            }
    }
</script>
   
<table width="500">
<tr>
<td>
    <div>
        <cc1:Accordion ID="MyAccordion" runat="server" SelectedIndex="1"
            HeaderCssClass="accordionHeader" HeaderSelectedCssClass="accordionHeaderSelected"
            ContentCssClass="accordionContent" FadeTransitions="false" FramesPerSecond="40" 
            TransitionDuration="250" AutoSize="None" RequireOpenedPane="false" SuppressHeaderPostbacks="true">
        <Panes>
        <cc1:AccordionPane runat="server">
<Header><table><tr><td><img alt="" src="Images/trackBack.png" /> </td><td>M��teri Te�ekk�r Mesajlar�</td></tr></table> </Header>
<Content>
<table width="480">
	<tr>
	<td>
	    <fieldset
        style=" border-top: 2px solid #FF6600; padding: 5px 0px 15px 10px; width: 480px; border-left-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-color: #FF6600; border-right-color: #FF6600; border-bottom-color: #FF6600;">
			<legend><img src="Images/Legend/Users.png" style="border: none;" alt="" /></legend>
	De�erli Misafirimiz,
Verdi�imiz t�m hizmetler de tek amac�m�z sizin memnuniyetiniz. Bizimle seyahat eden misafirlerimiz: 
            
            <br />
            <br />Hakl� Misafir Mesaj�ndan veya Kalite G�vence ve M��teri �li�kileri hatt�ndan (0246-232 31 61) t�m talep ve g�r��lerini iletebileceklerini, 
            <br />
            <br />Kalite g�vence ve M��teri �li�kileri yetkilileri taraf�ndan Seyahatleri sonras� telefonda aranarak memnuniyetlerinin �l��mlenece�ini, 
            <br />
            <br />Kullan�lan ileri teknoloji sayesinde t�m talep ve g�r��lerin gere�inin yerine getirilerek geri bilgilendirileceklerini, 
            <br />
            <br />www.otelpuan.com sitesinde konaklama hakk�nda ki g�r��lerini yay�nlayabileceklerini, 
            <br />
            <br />Misafir geri beslemelerinin hizmet kalitemizin artarak devam�n�n sa�lanmas�nda yol g�sterici oldu�unu, biliyorlar ve t�m seyahatlerinde bizi tercih ediyorlar. 
            <br />
            <br />Bu nedenle biz daha fazla a��klamayal�m misafirlerimiz anlats�n ve tamam�n� www.otelpuan.com sitesinde payla�al�m istedik.
T�m �al��ma arkada�lar�m�z ad�na olumlu g�r��lerinizi iletme nezaketini g�sterdi�iniz i�in te�ekk�r ederiz.
	</fieldset>
	</td>
	</tr>
</table>

<table>
<%while (DR_MusteriYorumlar�.Read()){%>
<tr>
<td>
 
 <div id="comment<%=DR_MusteriYorumlar�.GetValue(0).ToString()%>Head" style="display: inherit"><%=DR_MusteriYorumlar�.GetString(1)%></div>
 <div id="comment<%=DR_MusteriYorumlar�.GetValue(0).ToString()%>Full" style="display: none"><%=DR_MusteriYorumlar�.GetString(3)%></div>
 <br />
 <div style="text-align: right"><b style="color: #000000"><%=DR_MusteriYorumlar�.GetString(2)%></b></div>
 <div id="comment<%=DR_MusteriYorumlar�.GetValue(0).ToString()%>Devam" style="text-align: right"><a href="javascript:Detay('comment<%=DR_MusteriYorumlar�.GetValue(0).ToString()%>')">Devam�></a></div>
 <div id="comment<%=DR_MusteriYorumlar�.GetValue(0).ToString()%>Daralt" style="text-align: right; display: none;"><a id="A1" href="javascript:Detay('comment<%=DR_MusteriYorumlar�.GetValue(0).ToString()%>')">Daralt<</a></div>
 
 
 
<hr   style="width: 490px; color: #FF6600;" />
</td>
</tr>
<%}%>
</table>
</Content>
</cc1:AccordionPane>
<cc1:AccordionPane runat="server">
        <Header><table><tr><td><img alt="" src="Images/trackBack.png"/></td><td>Hakl� M��teri Mesaj�</td></tr></table></Header>
        <Content>
         <asp:UpdatePanel ID="UpdatePanel1" runat="server">
  <ContentTemplate>
        <table border="0" cellpadding="0" cellspacing="0" width="480">
	<tr>
	<td>
	<fieldset class="tah11"     
        style=" border-top: 2px solid #FF6600; padding: 5px 0px 15px 10px; width: 480px; border-left-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-color: #FF6600; border-right-color: #FF6600; border-bottom-color: #FF6600;">
			<legend><img src="Images/Legend/Users.png" style="border: none;" alt="" /></legend>
	De�erli Misafirimiz,
Amac�m�z; misafir memnuniyeti odakl� sat�� �ncesi ve sonras� hizmetler geli�tirerek, kaliteli ve hayal etti�iniz gibi tatiller sunmak. �al��malar�m�za ���k tutan dilek, �neri ve ele�tirileriniz bizim i�in �ok �nemli, bu y�zden mesaj�n�za "Hakl� Misafir Mesaj�" ad�n� veriyoruz. 

        <br />
        <br />
                A�a��da yer alan Mesaj Yaz buttonuna t�klayarak veya 0246 232 31 61 numaral� hattan Kalite G�vence ve M��teri ili�kileri yetkilileri ile irtibata ge�ebilirsiniz. �letece�iniz t�m g�r��leriniz; ilgili birimlerle payla��lacak, titizlikle ara�t�r�lacak ve neticesi Kalite G�vence ve M��teri �li�kileri yetkilimiz taraf�ndan en k�sa s�rede taraf�n�za bildirilecektir. 

        <br />
        <br />
        Te�ekk�r ederiz. 
        <br />
        <br />
        <center><asp:Image ID="btnMesajYaz" runat="server" ImageUrl="Images/nav6067970i.gif"/></center>
       
        <center><asp:Label ID="lblBasarili" runat="server" Text="Mesaj�n�z ba�ar�yla kaydedildi..." ForeColor="#669900" Visible="False"></asp:Label></center>
       </fieldset>    
        <cc1:ModalPopupExtender ID="programmaticModalPopup" runat="server" 
            DynamicServicePath="" Enabled="True" TargetControlID="btnMesajYaz" 
            PopupControlID="PanelModal" DropShadow="true" 
            RepositionMode="RepositionOnWindowScroll"
             BackgroundCssClass="modalBackground" CancelControlID="CancelButton">
             
        </cc1:ModalPopupExtender>
</td>
	</tr>
	
	</table>
     <asp:Panel ID="PanelModal" runat="server" Height="100%" Width="400" Style="display: none" 
            BackColor="#D3DEEF" BorderColor="#666666" BorderStyle="Outset">
            <center>
            <table cellpadding="5" cellspacing="2">
            <tr>
            <td colspan="2" align="center">
            <b class="Baslik">~Hakl� M��teri Mesaj�~</b>
            <hr />
            </td>
            </tr>
            <tr>
            <td>
            Ad:
            </td>
            <td align="left">
                <asp:TextBox ID="txtAd" runat="server" Width="150" CssClass="textBox"></asp:TextBox></td>
               
            </tr>
            <tr>
            <td>
            Soyad:
            </td>
            <td  align="left">
            <asp:TextBox ID="txtSoyad" runat="server" Width="150" CssClass="textBox"></asp:TextBox>
            </td>
            </tr>
            <tr>
            <td>
            E-Posta:
            </td>
            <td  align="left">
            <asp:TextBox ID="txtEposta" runat="server" Width="150" CssClass="textBox"></asp:TextBox>
            </td>
            </tr>
            <tr>
            <td>Telefon:</td>
            <td align="left"><asp:TextBox ID="txtTel" runat="server" Width="150" CssClass="textBox"></asp:TextBox></td>
            </tr>
            <tr>
            <td>Mesaj�n�z:</td>
            <td  align="left">
                <asp:TextBox ID="txtMesaj" runat="server" TextMode="MultiLine" Width="189px" 
                    Height="100px" CssClass="textBox"></asp:TextBox></td>
                
            </tr>
            <tr>
            <td align="center" colspan="2"> 
            <center><asp:Label ID="lblError" runat="server" Text="Bo� alan b�rakmay�n�z..." Visible="False" ForeColor="Maroon"></asp:Label></center>
            <hr /> 
            <asp:Button ID="SendButton" runat="server" Text="G�nder" Width="62"  Height="26" 
                    CssClass="textBox" OnClick="btnSend_Click"/>
               
            <asp:Button ID="CancelButton" runat="server" Text="�ptal" Width="62" Height="26" 
                    CssClass="textBox"/>
            </td>
            </tr>
            </table>
      </center>
        </asp:Panel>
        </ContentTemplate>
  </asp:UpdatePanel>
        </Content>
        </cc1:AccordionPane>
        </Panes>
        </cc1:Accordion>
</div>
</td>
</tr>
</table>

</asp:Content>

