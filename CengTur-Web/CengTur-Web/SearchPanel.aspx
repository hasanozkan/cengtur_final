<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SearchPanel.aspx.cs" Inherits="SearchPanel" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
    <link href="Style/CengTur.css" type="text/css" rel="stylesheet"/>
    
    <script language="javascript" type="text/javascript">
        function searchString()
	    {
	        var searchUrl;
    	    
	        if(document.frmSearchPanel.ddlSehAdi.value == "")
	            alert("L�tfen Tur Bilgisi se�iniz.");
            else if(document.frmSearchPanel.ddlSehAdi.value == 0)
                alert("L�tfen �ehir se�iniz.");
            else
            {
                searchUrl = "SearchResult.aspx?" + document.frmSearchPanel.ddlTurAdi.value + "," + document.frmSearchPanel.ddlSehAdi.value + "," + document.frmSearchPanel.ddlBolAdi.value;
                window.open(searchUrl, '_parent');
            }
	    }
    </script>
    
</head>
<body style="background-color: #E5EAF0">
    <form id="frmSearchPanel" name="frmSearchPanel" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
    <div>
	    <table class="tdDetail" border="0" cellpadding="2">
            <tr>
                <td>
                    Oteller ve Turlar
                    <asp:DropDownList ID="ddlTurAdi" runat="server" CssClass="ddList" AutoPostBack="true" OnSelectedIndexChanged="ddlTurAdi_SelectedIndexChanged"></asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:DropDownList ID="ddlSehAdi" runat="server" CssClass="ddList" AutoPostBack="true" OnSelectedIndexChanged="ddlSehAdi_SelectedIndexChanged"></asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:DropDownList ID="ddlBolAdi" runat="server" CssClass="ddList"></asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                <table>
                <tr>
                <td>
                <asp:UpdateProgress ID="UpdateProgress2" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                    <ProgressTemplate>
                    <img src="Images/Ajax/ajax-loader.gif" />
                    </ProgressTemplate>
                    </asp:UpdateProgress>
                </td>
                <td style="width: 100%">
                 <asp:ImageButton ImageAlign="Right" ID="btnSearch" runat="server" ImageUrl="~/Images/Buttons/ara_button.gif" OnClientClick="searchString();" />
                </td>
                </tr>
                </table>
                    
                   
                </td>
            </tr>
        </table>
    </div>
    </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>