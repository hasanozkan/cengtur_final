<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="CustomsInformations.aspx.cs" Inherits="Default2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpDetail" Runat="Server">

<table border="0" cellpadding="0" cellspacing="0" width="510">
    <tr>
    <td>
    <table>
    <tr>
    <td background="Images/Tables/title_bg_acikmavi.jpg">&nbsp;&nbsp; �<a href="#GOdenmeyenEsyalar">G�mr�k �denmeyen E�yalar</a>� �<a href="#GOdenenEsyalar">G�mr�k �denen E�yalar</a>� �<a href="#Doviz">D�viz Giri�-��k���</a>�</td>
    
    </tr>
    </table>
    <hr />
    </td>
    </tr>
    
	<tr class="BaslikSatiri">
	<a name="GKurallar�"></a>
		<td align="right" 
            style="height: 40px; padding-right:10px; background-image:url('Images/Tables/title_bar_turuncu_big.jpg');">
            <span style="font-size: 20px;">G�mr�k Kurallar�</span>
		</td>
	</tr>
	<tr>
	<td>
	
	<SPAN>Her �lkenin transit ge�i� 
                                amac�yla dahi olsa topraklar�na sokulacak mal, 
                                e�ya, t�ketim malzemesi ve parayla ilgili �zel 
                                kurallar� bulunmaktad�r. Bu kurallar �zellikle 
                                eski Do�u Bloku �yesi �lkelerde (Yugoslavya, 
                                Bulgaristan, Romanya) hayli kat�d�r. S�zkonusu 
                                kurallar�n bilinmesi ve bunlara uyulmas�, 
                                arzulanmayan durumlar�n �nlenmesi ve giri� 
                                yap�lan �lkeden rahatl�kla ��k�� yap�lmas� i�in 
                                gereklidir. Giri� g�mr���ndeki ihmal ve 
                                yanl��l�klar�n ��k�� g�mr���nde soruna ve daha 
                                b�y�k s�k�nt�lara yol a�abilece�i ak�lda 
                                tutulmal�d�r. G�mr�k formaliteleri genellikle 
                                �lkeye sokulmas� yasak ya da miktar olarak belli 
                                bir s�n�ra tabi veya s�zl� ya da yaz�l� olarak 
                                beyan edilmesi gerekli �eyler i�erir. Genelde 
                                her t�rl� silah, gaz tabancalar�, g�z ya�art�c� 
                                spreyler ve av malzemelerinin �lkeye sokulmas� 
                                yasakt�r. �ahsi ihtiyaca y�nelik e�yan�n (zati 
                                e�ya) tekrar ��kar�lmas� �art�yla �lkeye 
                                sokulmas� serbesttir. �ahsi ihtiyaca y�nelik 
                                e�ya ve malzemenin neler oldu�u ve hangi 
                                miktarlarda olmas� gerekti�i esasen uluslararas� 
                                anla�malarla belirlenmi�tir. A�a��da bir k�sm� 
                                liste halinde verilmekte olan bu e�ya ve 
                                malzemeler ile miktarlar�n�n belirlenmesinde 
                                g�zetilen husus, bunlar�n miktar, �e�it ve ama� 
                                olarak ki�isel kullan�ma uygun olmas�, ticari 
                                nitelikte bulunmamas�d�r.</SPAN>
        <br />
        <br />
                                </td>
	</tr>
	
		<tr class="BaslikSatiri">
		<a name="GOdenmeyenEsyalar"></a>
		<td align="right" 
            style="height: 40px; padding-right:10px; background-image:url('Images/Tables/title_bar_turuncu_big.jpg');">
            <span style="font-size: 20px;">G�mr�k �denmeyen E�yalar</span>
		</td>
	</tr>
		<td>
		
	<SPAN class="Baslik">Giyim ve Yolculuk 
                                E�yas�<STRONG>:</STRONG></SPAN><BR><SPAN 
                                class=vbl11acikgri>Yolcunun giyinip ku�anmas�na 
                                mahsus e�ya,<BR>Yolcunun ya�ant�s�na mahsus e�ya 
                                ile seyahat e�yas�,<BR>Yolcu i�in elbiselik 
                                kuma� (en fazla 5 metre),<BR>Naklihane hakk�n� 
                                haiz olan ve yurda kesin d�n�� yapan yolcuya ait 
                                bir adet k�rkten mamul giyim 
                                e�yas�.</SPAN><BR><BR>
            <STRONG 
                                class=Baslik>Elektronik 
                                E�ya:</STRONG><BR><SPAN class=vbl11acikgri>Bir 
                                adet renkli televizyon (55 ekrana kadar)<BR>Bir 
                                adet 16 ekrana kadar renkli cep televizyonu (16 
                                ekran dahil),<BR>Bir adet siyah beyaz 
                                televizyon-radyo-teyp (kombine),<BR>Bir adet 
                                video kamera ve 5 adet bo� video teyp 
                                kaseti,<BR>Sekiz mm.ye kadar al�c� veya 
                                g�sterici sinema makinas� (On adet bo� sinema 
                                filmi ile birlikte),<BR>Bir adet slayt g�sterme 
                                makinas�,<BR>Portatif compact disc 
                                player,<BR>Bir adet kendi ses verebilen portatif 
                                radyo ve portatif radyo-teyp (portatif 
                                radyo-teybin �zellikleri M�ste�arl�k�a 
                                belirlenir.)<BR>Bir adet volkmen veya cepte 
                                ta��nabilen k���k teyp,<BR>Kasetsiz ve kartu�suz 
                                elektronik oyun aletleri,<BR>Her birinden en �ok 
                                5 er adet olmak �zere plak, teyp kaseti veya 
                                compact disc.</SPAN><BR><BR>
            <STRONG 
                                class=Baslik>M�zik 
                                Aletleri:</STRONG><BR><SPAN 
                                class=vbl11acikgri>Armonika, Mandolin, Kaval, 
                                M�z�ka, Fl�t, Gitar, Akordeon, (Her birinden 
                                birer adet olmak �zere en �ok 3 
                                adet)</SPAN><BR><BR>
            <SPAN 
                                class=Baslik>Spor ve Oyun 
                                Aletleri:</SPAN><BR><SPAN class=vbl11acikgri>�ki 
                                b�lmeli bir adet kamp �ad�r�,<BR>Sualt� sporlar� 
                                i�in bir adet dalg�� tak�m� (nitelikleri, aksam, 
                                ve par�alar� M�ste�arl�k�a belirlenir.),<BR>U�an 
                                kanatlar (bir �ift),<BR>Motor tak�lmayan cinsten 
                                �i�irme bir adet bot,<BR>Su sporu i�in 
                                kullan�lan yelken tertibatl� bir adet s�rf, Ayak 
                                paleti (bir �ift),<BR>Yolcunun tek ba��na 
                                kullanabilece�i �zelli�e sahip birer adet olmak 
                                �zere spor yapmas�na mahsus di�er spor alet ve 
                                giysileri (deniz motosikleti ve motorlu deniz 
                                k�za�� hari�),<BR>Satran� tak�m�, Dama tak�m�, 
                                Be� adet oyun ka��d�.</SPAN><BR><BR>
            <STRONG 
                                class=Baslik>Sa�l�k 
                                Cihazlar�:</STRONG><BR><SPAN 
                                class=vbl11acikgri>Hasta yolcuya ait yataklar, 
                                Mal�ller i�in hareket ettirici tertibat� bulunan 
                                motorlu, motorsuz koltuklar, �ahsi tedavide 
                                kullan�lan ila�lar, Gaz maskesi ve benzeri 
                                koruyucu elbise (en �ok 2 
                                adet).</SPAN><BR><BR>
            <STRONG 
                                class=Baslik>Mutfak 
                                E�yas�:</STRONG><BR><SPAN 
                                class=vbl11acikgri>Yolculuk esnas�nda 
                                kullan�lacak kadar tabak, tencere, �atal, ka��k, 
                                b��ak gibi mutfak aletleri,<BR>Bir adet portatif 
                                piknik oca��,<BR>Bir adet semaver,<BR>�ki adet 
                                termos,<BR>Ak� ile �al��an kahve 
                                makinas�.</SPAN><BR><BR>
            <STRONG 
                                class=Baslik>T�ketim 
                                Maddeleri:</STRONG><BR><SPAN 
                                class=vbl11acikgri>Yerli ve yabanc� ayr�m� 
                                yapmaks�z�n;<BR>a) 200 adet sigara (1 karton) 
                                ile 50 adet yaprak sigara. Ayr�ca, 200 gr. 
                                k�y�lm�� t�t�n ve 200 yaprak sigara ka��d� veya 
                                200 gr. pipo t�t�n� veya 200 gr. a��z t�t�n� 
                                veya 200 gr. t�mbeki veya 50 gr. enfiye,<BR>b) 
                                G�mr�k hatt� d��� e�ya sat�� ma�azalar�ndan 
                                sat�n al�nm�� olmas� halinde (a) bendinde yer 
                                alanlara ek olarak 200 adet sigara (1 karton), 
                                100 adet yaprak sigara ve 500 gr. pipo 
                                t�t�n�,<BR>1,5 kg. kahve,<BR>1,5 kg. 
                                nescafe,<BR>500 gr. �ay,<BR>1 kg. �ikolata,<BR>1 
                                kg. �ekerden mam�l yiyecek,<BR>100 cl'lik bir 
                                (1) �i�e ya da 75 veya 70 cl'lik iki (2) �i�e 
                                alkoll� i�ki,<BR>En fazla 120 ml.lik �i�eler 
                                i�inde be� adet kolonya, lavanta, parf�m, esans 
                                veya losyon,<BR></SPAN><BR>
            <STRONG 
                                class=Baslik>Di�erleri:</STRONG><BR><SPAN 
                                class=vbl11acikgri>Bir adet portatif yaz� 
                                makinas�,<BR>Bir adet foto�raf makinas� (5 adet 
                                filmi ile birlikte),<BR>�ocuk yolculara mahsus 
                                bir adet �ocuk arabas�,<BR>�ocuk yolcuya mahsus 
                                ebad�na bak�lmaks�z�n bir adet 
                                bisiklet,<BR>�ocuk yolcuya mahsus oyuncaklar (en 
                                �ok 10 adet),<BR>Cepte ta��nabilen ve pille 
                                �al��an hesap makinas�,<BR>Bir adet �t� (buharl� 
                                veya buhars�z),<BR>1 x 1,5 metre ebad�nda 
                                seccade (y�nl�, pamuklu veya sentetik olmak 
                                kayd�yla),<BR>Bir adet el d�rb�n� (gece g�r�� 
                                d�rb�n ve g�zl�kleri ile gece g�r�� keskin 
                                ni�anc� d�rb�nleri hari�),<BR>Bir adet masa 
                                saati,<BR>Bir adet portatif gaz 
                                sobas�,<BR>Ara�la birlikte yolculuk halinde 
                                (sadece ara� sahibi veya s�r�c�s�ne uygulanmak 
                                kayd�yla)<BR>a) Tornavida tak�m� (ayr� 
                                b�y�kl�kte olmak �zere en fazla 5 adet),<BR>b) 
                                Lokma tak�m� (ara�ta kullan�lan tipte olmak 
                                �zere en fazla 10 par�a),<BR>c) Oto 
                                pompas�,<BR>d) Ak� �arj cihaz�,<BR>e) Ak� ile 
                                �al��an otomobil s�p�rgesi,<BR>f) Ak� ile 
                                �al��an buzluk,<BR>g) Stepne d���nda bir adet 
                                oto i� ve d�� lasti�i,</SPAN><BR><BR><SPAN 
                                class=vbl11acikgri>Yolcunun durumuna g�re G�mr�k 
                                M�ste�arl���n�n zati e�ya kapsam�nda m�talaa 
                                edilecek di�er e�ya,<BR><BR>Yolcunun beraberinde 
                                olmak kay�t ve �art�yla 1 adet kedi veya 1 adet 
                                k�pek veya 1 adet ku� veya 10 adet akvaryum 
                                bal��� (veteriner sa�l�k raporu, orijin ve a�� 
                                belgesi ile varsa kimlik ve e�gal belgesi ibraz� 
                                �art� ve veteriner kontrol� 
                                kayd�yla).<BR><BR>�te yandan �lkeye 
                                sokulabilecek veya �lkeden ��kar�labilecek 
                                d�vizle (o �lke paras� d���ndaki paralar) veya o 
                                �lke paras�yla ilgili �zel kurallar 
                                bulunmaktad�r. Genelde belli bir miktara kadar 
                                d�vizin veya yerel paran�n �lkelere sokulmas� 
                                veya ��kar�lmas� serbesttir. Ancak bu 
                                miktarlar�n �zerindeki d�viz veya yerel paran�n 
                                giri�te mutlaka beyan edilmesi 
                                gerekir.<BR><BR>Paran�n yan�s�ra baz� e�yan�n da 
                                �lkeye giri�te beyan edilmesi gerekir. Bunlar�n 
                                ba��nda ziynet e�yas� gelmektedir. Ayr�ca, TV, 
                                foto�raf makinas�, video kamera, video oynat�c�, 
                                cep telefonu, elektrikli aletler, kamping ve 
                                spor malzemeleri ile di�er k�ymetli e�yan�n da 
                                g�mr�kte beyan edilmesi gerekmektedir. Beyan 
                                yaz�l� ya da s�zl� olabilmektedir. Beyan�n nas�l 
                                olaca�� �lkeye ve e�yan�n tipine 
                                ba�l�d�r.<br />
            </SPAN><BR>
                                </td>
                                </tr>
                               	<tr class="BaslikSatiri">
                               	 <a name="GOdenenEsyalar"></a>
		                        <td align="right" 
                                style="height: 40px; padding-right:10px; background-image:url('Images/Tables/title_bar_turuncu_big.jpg');">
                                <span style="font-size: 20px;">G�mr�k �denen E�yalar</span>
		                        </td>
	                            </tr>
	                            <tr>
	                            <td>
	                           
                                    <SPAN class=vbl11acikgri>18 
                                ya��ndan b�y�k yolcuya, T�rkiye'ye geli�inden 2 
                                ay �nce ve 6 ay sonra posta veya h�zl� kargo 
                                yoluyla g�nderilen ve de�eri 1.500 Euro'yu 
                                ge�meyen ve ticaret ama�l� olmayan e�yadan y�zde 
                                10 sabit g�mr�k al�n�r (300 Euro'luk muafiyet 
                                d���l�r). Ya da 18 ya��ndaki yolcunun 
                                beraberinde getirece�i ve de�eri 1.500 Euro'yu 
                                ge�meyen ve ticaret ama�l� olmayan e�yadan y�zde 
                                10 sabit g�mr�k al�n�r (300 Euro'luk muafiyet 
                                d���l�r). Bu haktan yararlanabilmek i�in �u 
                                ko�ullar gereklidir:<BR><BR>T�rkiye'deki 
                                ki�ilere posta veya h�zl� kargo ta��mac�l��� 
                                yoluyla g�nderilen, yolcular beraberlerinde 
                                getirdikleri, hediye edilmek �zere getirilen ya 
                                da g�nderilen, ki�inin �ahs�na veya ailesinin 
                                kullan�m�na mahsus, kar��l���nda al�c� 
                                taraf�ndan herhangi bir �deme yap�lmam�� olan, 
                                ticari miktar ve mahiyet arz etmeyen,<BR><BR>Bu 
                                �er�evede getirilen e�yan�n toplam k�ymetinin 
                                1.500 EURO'yu a�mas� halinde, s�z konusu e�yan�n 
                                ithalinde �denmesi gereken vergiler, y�r�rl�kte 
                                bulunan esaslara g�re tahakkuk ve tahsil edilir. 
                                �thal konusu e�yan�n k�ymetinin tek ba��na 1.500 
                                EURO'yu ge�mesi halinde, bu e�yan�n g�mr�k 
                                vergileri �thalat Rejimi Karar� ile belirlenen 
                                oranlara g�re tahakkuk ve tahsil edilir. E�yaya 
                                sabit oranl� vergi uygulamadan �nce, beyan 
                                sahibi bunun yerine ithalat vergilerinin 
                                uygulanmas�n� talep edebilir. Bu durumda e�ya 
                                i�in ithalat vergileri tahsil edilmek suretiyle 
                                e�yan�n ithaline izin verilir. Bu �er�evede 
                                getirilen e�yan�n vergi matrah�n�n tespitinde 
                                esas al�nacak k�ymeti, ibraz edilen faturaya 
                                veya sat�� fi�ine g�re belirlenir. Bu t�r belge 
                                ibraz edilememesi veya ibraz edilen belgede 
                                kay�tl� k�ymetin d���k bulunmas� halinde, 
                                e�yan�n k�ymeti idarece tespit 
                                edilir.<br />
                                    </SPAN><BR>
                                </td>
                                </tr>
                                  	<tr class="BaslikSatiri">
		                        <td align="right" 
                                style="height: 40px; padding-right:10px; background-image:url('Images/Tables/title_bar_turuncu_big.jpg');">
                                <span style="font-size: 20px;">D�viz Giri�-��k���</span>
		                        </td>
	                            </tr>
	                            <tr>
	                            <td>
	                            
	                            <a name="Doviz"></a>
                                    <SPAN 
                                class=vbl11acikgri>Yolcular, s�n�rs�z miktarda 
                                T�rk paras�n� ve d�vizi beraberlerinde 
                                T�rkiye'ye getirebilirler. Yolcular, en �ok 
                                5.000 ABD dolar� kar��l��� T�rk paras�n� yurt 
                                d���na ��karabilirler. Yolcular, 5.000 ABD 
                                dolar� veya e�iti nakit di�er d�vizleri 
                                beraberlerinde yurt d���na ��karabilirler. 
                                D��ar�da yerle�ik ki�iler ile yurt d���nda 
                                �al��an T�rk uyruklu ki�iler, yurda giri�lerinde 
                                beyan etmi� olmak, T�rkiye'de yerle�ik ki�iler 
                                ise bankalardan d�viz sat�n ald�klar�n� 
                                belgelemek kayd�yla 5.000 ABD dolar� veya 
                                e�itini a�an miktardaki nakit d�vizi 
                                beraberlerinde yurt d���na ��karabilirler. 
                                Bankalar vas�tas� ile yurt d���na T�rk paras� ve 
                                d�viz transferi serbesttir. Yolcular, 
                                beraberlerindeki kendilerine ait de�eri 15.000 
                                ABD dolar�n� a�mayan ve ticari ama� ta��mayan 
                                ziynet e�yas� niteli�inde k�ymetli madenlerden 
                                ve ta�lardan yap�lm�� e�yay� yurda 
                                getirebilirler ve yurt d���na ��karabilirler. 
                                (Daha fazla de�erdeki ziynet e�yas�n�n yurt 
                                d���na ��kar�lmas�, giri�te beyan edilmi� 
                                olmas�na veya T�rkiye'den sat�n al�nm�� oldu�unu 
                                belgeleme �art�na ba�l�d�r.) T�rkiye'den 
                                yap�lacak bir ihracat�n bedeli olarak, pe�in 
                                �deme yapmak �zere efektifi beraberinde getiren 
                                yolcular�n, yurda giri�te bunu deklare etmeleri 
                                ve belgelendirmeleri 
                                gerekir.</SPAN><BR><BR>
                                </td>
	</tr>
	</table>
</asp:Content>

