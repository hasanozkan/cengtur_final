#region Using Tanýmlamalarý
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
#endregion

public partial class MasterPage : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        #region Count Visitor
        //Guncelleme
        if (!Convert.ToBoolean(Session["Kontrol"]))
        {
            string SQL = "UPDATE ZiyaretciSayisi SET Sayac=Sayac+1, sonZiyaretTrh=getdate()";
            CengTur.ExecQuery(SQL);
        }

        //Okuma
        DataTable zSayTB = CengTur.GetDataTable("Select * From ZiyaretciSayisi");
        lblCountVisitor.Text = zSayTB.Rows[0]["Sayac"].ToString();

        Session["Kontrol"] = true;
        #endregion
    }
}
