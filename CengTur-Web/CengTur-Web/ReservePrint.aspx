﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ReservePrint.aspx.cs" Inherits="ReservePrint" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Yazdır</title>
    <link href="Style/Master.css" rel="stylesheet" type="text/css" />
    <link href="Style/CengTur.css" rel="stylesheet" type="text/css" />
</head>
<body>
<table class="MasterText" border="0" cellpadding="0" cellspacing="0" width="510px">
<tr><td>
<center><asp:Label ID="lblAdi" runat="server" Text="Otel Rezervasyon Formu" Font-Size="24px" ForeColor="#F03307"></asp:Label></center>
   <br />
   <table style="width: 100%; background-image: url('Images/Tables/bg_box_noeffect_90x509.gif');">
	<tr>
	<td>
	<b>Otel Adı</b>
	</td>
	<td><b>:</b></td>
	<td colspan="5">
	<asp:Label ID="lblOtelAdi" runat="server" Text="lblOtelAdi" CssClass="text"></asp:Label>
	</td>
	</tr>

                <tr>
                    <td style="width:15%;"><b>Oda Tipi</b></td>
                    <td style="width:2%;"><b>:</b></td>
                    <td style="width:33%;">
                        <asp:Label ID="lblOdaTipi" runat="server" Text="lblOdaTipi"></asp:Label>
                    </td>
                    
                    <td style="width:15%;"><b>Konaklama</b></td>
                    <td style="width:2%;"><b>:</b></td>
                    <td style="width:33%;">
                        <asp:Label ID="lblKonaklamaTipi" runat="server" Text="lblKonaklamaTipi"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td><b>Kişi Sayısı</b></td>
                    <td><b>:</b></td>
                    <td>
                        <asp:Label ID="lblKisiSay" runat="server" Text="lblKisiSay"></asp:Label>
                    </td>
                    
                    <td><b>Konum</b></td>
                    <td><b>:</b></td>
                    <td>
                        <asp:Label ID="lblKonum" runat="server" Text="lblKonum"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td><b>Giriş Tarihi</b></td>
                    <td><b>:</b></td>
                    <td>
                        <asp:Label ID="lblGirisTarihi" runat="server" Text="lblGirisTarihi"></asp:Label>
                    </td>
                    
                    <td><b>Çıkış Tarihi</b></td>
                    <td><b>:</b></td>
                    <td>
                        <asp:Label ID="lblCikisTarihi" runat="server" Text="lblCikisTarihi"></asp:Label>
                    </td>
                </tr>
                
                  <tr>
                    <td valign="top" style="width:20%;"><b>Fiyat</b></td>
                    <td valign="top"><b>:</b></td>
                    <td colspan="4"><asp:Label ID="lblFiyat" runat="server" Text=""></asp:Label></td>
                </tr>
                	<%--<tr>
	<td>
	<b>Adres</b>
	</td>
	<td>:</td>
	<td colspan="5"><%=Adres %></td>
	</tr>--%>
                
                <tr>
                    <td colspan="6">
             <fieldset class="Tah11" style=" padding:5px 0px 15px 10px; width: 100%; border-color: #FF6600; border-top-style: solid;
			border-left-width: 0px; border-right-width: 0px; border-top-width: 2px; border-bottom-width: 1px">
			<legend><img src="Images/Legend/Users.png" style="border: none;" alt="" /></legend>
            <asp:Table ID="TblPersonal" runat="server" border="0" width="98%" style="border-collapse: collapse;">
            <asp:TableRow style="background-color:#FF6600; color: White; font-weight:bold;">
            <asp:TableCell></asp:TableCell>
            <asp:TableCell><center>TC Kimlik No</center></asp:TableCell>
            <asp:TableCell><center>Adı</center></asp:TableCell>
            <asp:TableCell><center>Soyadı</center></asp:TableCell>
            <asp:TableCell><center>Cinsiyet</center></asp:TableCell>
            <asp:TableCell><center>Doğum Tarihi</center></asp:TableCell>
            <asp:TableCell><center>Ödeme</center></asp:TableCell>
            </asp:TableRow>
            </asp:Table>
            </fieldset> 
            <br />
             <p><span style="color: #F03307;">Yukarıdaki bilgileri kontrol ediniz eğer yanlış bilgi girişi varsa kısa süre içinde bize bildiriniz.</span>
               <br />
               <br />
            </p>
                    </td>
                </tr>
                <tr>
                <td colspan="6">
                <table border="0" width="98%" style="border-collapse: collapse;">
                <tr>
                    <td><b>Telefon No</b></td>
                    <td>:</td>
                    <td style="width: 80%">
                        <asp:Label ID="lblTelefon" runat="server" Text="lblTelefon"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td><b>e-Posta Adresi</b></td>
                    <td>:</td>
                    <td>
                        <asp:Label ID="lblEPosta" runat="server" Text="lblEPosta"></asp:Label></td>
                </tr>
                <tr>
	                <td colspan="3" align="right">	                   
	                <br />
                       
                </tr>
            </table>
                </td>
                </tr>                
            </table>
    </td></tr>
    </table>
    <script type="text/javascript" language="javascript">
        window.print();
    </script>
</body>
</html>
