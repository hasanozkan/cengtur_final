#region Using Tanımlamaları
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
#endregion

public partial class SearchPanel : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (!IsPostBack)
        {
            ddlTurAdi.DataSource = CengTur.GetDataTable("SELECT * FROM Turlar ORDER BY Oncelik");
            ddlTurAdi.DataValueField = "TurKodu";
            ddlTurAdi.DataTextField = "TurAdi";
            ddlTurAdi.DataBind();

            ddlSehAdi.Enabled = false;
            ddlBolAdi.Enabled = false;
        }
        
    }
    protected void ddlTurAdi_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlSehAdi.Enabled = true;
        ddlSehAdi.DataSource = CengTur.GetDataTable("SELECT SKodu, SAdi FROM Sehir WHERE TurKodu = '" + ddlTurAdi.SelectedValue + "'");
        ddlSehAdi.DataValueField = "SKodu";
        ddlSehAdi.DataTextField = "SAdi";
        ddlSehAdi.DataBind();
        ddlSehAdi.Items.Insert(0, new ListItem("SEÇİNİZ", "0", true));

        ddlBolAdi.Items.Clear();
        ddlBolAdi.Enabled = false;
    }
    protected void ddlSehAdi_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlBolAdi.Enabled = true;
        ddlBolAdi.DataSource = CengTur.GetDataTable("SELECT * FROM Bolge WHERE SKodu=" + ddlSehAdi.SelectedValue);
        ddlBolAdi.DataValueField = "BKodu";
        ddlBolAdi.DataTextField = "BAdi";
        ddlBolAdi.DataBind();
        if (ddlBolAdi.Items.Count == 0)        
        ddlBolAdi.Items.Add(new ListItem(ddlSehAdi.SelectedItem.Text, ddlSehAdi.SelectedValue));

        
    }
}