<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Reserve.aspx.cs" Inherits="Reserve" %>
<%@ Register Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpDetail" Runat="Server">
<script type="text/javascript" language="javascript">
    document.getElementById('lblTurAdi');
</script>
<table border="0" cellpadding="0" cellspacing="0" width="510">
	<tr class="BaslikSatiri">
		<td colspan="2" align="right" style="height: 40px; padding-right:10px; background-image:url('Images/Tables/title_bar_turuncu_big.jpg');">
            <span style="font-size: 20px;">online sat��</span>
		</td>
	</tr>

    <tr style="height: 5px;"><td colspan="2"></td></tr>

	<tr class="BaslikSatiri" style="background-image:url('Images/Tables/title_bg_red.jpg');">
		<td colspan="2">
            <asp:Label ID="lblTurAdi" runat="server" Text="lblTurAdi"></asp:Label> / 
            <asp:Label ID="lblSehir" runat="server" Text="lblSehir"></asp:Label> /
            <asp:Label ID="lblBolge" runat="server" Text="lblBolge"></asp:Label>
		</td>
	</tr>

	<tr class="tdDetail" style="height: 40px;">
		<td colspan="2">
            <asp:Label ID="lblAdi" runat="server" Text="lblAdi" Font-Size="24px" ForeColor="#F03307"></asp:Label>
		</td>
	</tr>
	<tr class="tdDetail">
		<td colspan="2">
	        <fieldset class="tah11" style=" padding:15px 0px 15px 10px; width: 100%; border-color: #FF6600; border-top-style: solid;
			border-left-width: 0px; border-right-width: 0px; border-top-width: 2px; border-bottom-width: 0px">
			<legend><img src="Images/Legend/Calendar.png" style="border: none;" alt="" /></legend>
                <asp:UpdatePanel ID="UpdatePanelTop" runat="server">
                <ContentTemplate>
	            <table border="0" width="100%" cellspacing="0" class="TableData">
	                <tr>
		                <td rowspan="7" style="width: 35%;">
                            <asp:Calendar ID="Calendar1" runat="server" BackColor="White" BorderColor="#F03307" BorderWidth="1px" 
                                CellPadding="0" EnableTheming="False" EnableViewState="False" FirstDayOfWeek="Monday" 
                                Font-Names="Trebuchet MS" Font-Size="8pt" ForeColor="#0066CC" DayNameFormat="Shortest"  NextPrevFormat="ShortMonth" 
                                Width="150px" OnDayRender="Calendar1_DayRender" OnSelectionChanged="Calendar1_SelectionChanged">
                                <OtherMonthDayStyle ForeColor="#999999" />
                                <NextPrevStyle Font-Size="8pt" ForeColor="#FF6600" />
                                <DayHeaderStyle BackColor="#FFFFEC" ForeColor="#FF6600" Height="1px" />
                                <TitleStyle BackColor="#FFFFEC" ForeColor="#F03307" Font-Bold="True" Font-Size="10pt" Height="20px" />
                            </asp:Calendar>
		                </td>
		                <td style="width: 60%;" colspan="2">
		                    <asp:Image ID="imgOpenDate" runat="server" ImageUrl="~/Images/Calendar/openDate.gif" ImageAlign="AbsMiddle" />
		                    Ge�erli oldu�u tarih aral���
                        </td>
	                </tr>
	                <tr>
		                <td colspan="2">
		                    <asp:Image ID="imgSelectedDate" runat="server" ImageUrl="~/Images/Calendar/selectedDate.gif" ImageAlign="AbsMiddle" />
		                    Se�ilen tarih aral���
		                </td>
	                </tr>
	                <tr>
		                <td colspan="2">
		                    <asp:Image ID="imgToday" runat="server" ImageUrl="~/Images/Calendar/Today.gif" ImageAlign="AbsMiddle" />
		                    Bug�n
                        </td>
	                </tr>
	                <tr>
		                <td colspan="2"><hr style="color:#FF6600; height:1px;" /></td>
	                </tr>
	                <tr>
		                <td style="width: 17%"><b>Giri� Tarihi: </b></td>
		                <td><asp:Label ID="lblBasTarih" runat="server" Text="<< Takvimden Se�iniz"></asp:Label></td>
	                </tr>
	                <tr>
		                <td><b>Gece Say�s�: </b></td>
		                <td>
                            <asp:DropDownList ID="ddlGeceSayisi" runat="server" AutoPostBack="True" Enabled="false" Font-Size="8pt"
                                Width="100px" OnSelectedIndexChanged="ddlGeceSayisi_SelectedIndexChanged">
                            </asp:DropDownList>		    
                        </td>
	                </tr>
	                <tr>
		                <td><b>��k�� Tarihi: </b></td>
		                <td><asp:Label ID="lblBitTarih" runat="server"></asp:Label></td>
	                </tr>
	                <tr>
	                <td align="center" colspan="3">
                        <asp:UpdateProgress ID="UpdateProgressTop" runat="server" AssociatedUpdatePanelID="UpdatePanelTop">
                        <ProgressTemplate>
                        <br />
                            &nbsp;
                        <img src="Images/Ajax/ajax-loader.gif" /> Y�kleniyor...</ProgressTemplate>
                        </asp:UpdateProgress>
	                </td>
	                </tr>
                </table>
                </ContentTemplate>
                </asp:UpdatePanel>
			</fieldset>
		</td>
	</tr>
	<tr class="tdDetail" style="height: 40px;">
		<td colspan="2">
		    <fieldset class="tah11" style=" padding:5px 0px 15px 10px; width: 100%; border-color: #FF6600; border-top-style: solid;
			    border-left-width: 0px; border-right-width: 0px; border-top-width: 2px; border-bottom-width: 1px">
			    <legend><img src="Images/Legend/Users.png" style="border: none;" alt="" /></legend>
                <asp:UpdatePanel ID="UpdatePanelBottom" runat="server">
                    <ContentTemplate>
                    <table style="width: 100%; background-image: url('Images/Tables/bg_box_noeffect_90x509.gif');">
                    <tr>
                        <td><b>Oda ve Konaklama Tipi</b></td>
                        <td><b>:</b></td>
                        <td>
                            <asp:DropDownList ID="ddlOdaKonaklamaTipi" runat="server" Font-Size="8pt" 
                                Width="250px" onselectedindexchanged="ddlOdaKonaklamaTipi_SelectedIndexChanged"></asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td><b>Yeti�kin Say�s�</b></td>
                        <td><b>:</b></td>
                        <td>
                            <asp:DropDownList ID="ddlYetiskinSayisi" runat="server" AutoPostBack="true" Font-Size="8pt" Width="100px" OnSelectedIndexChanged="ddlYetiskinSayisi_SelectedIndexChanged">
                                <asp:ListItem Value="1"></asp:ListItem>
                                <asp:ListItem Value="2"></asp:ListItem>
                                <asp:ListItem Value="3"></asp:ListItem>
                                <asp:ListItem Value="4"></asp:ListItem>
                                <asp:ListItem Value="5"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td><b>�ocuk Say�s�</b></td>
                        <td><b>:</b></td>
                        <td>
                            <asp:DropDownList ID="ddlCocukSayisi" runat="server" Enabled="false" 
                                Font-Size="8pt" Width="100px" 
                                onselectedindexchanged="ddlCocukSayisi_SelectedIndexChanged1" 
                                AutoPostBack="True">
                                <asp:ListItem Value="Se�iniz"></asp:ListItem>
                                <asp:ListItem Value="1"></asp:ListItem>
                                <asp:ListItem Value="2"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                    <td colspan="3">
                  
                        <asp:Table ID="tblCocuk" runat="server" Visible="false" Width="350px">
                        
                        <asp:TableRow>
                        <asp:TableCell Width="162"><b>1.�ocuk Do�um Tarihi</b></asp:TableCell>
                        <asp:TableCell>:</asp:TableCell>
                        <asp:TableCell><asp:DropDownList ID="ddlGunCocuk1" runat="server" OnSelectedIndexChanged="ddlOdaDogumTarihi_SelectedIndexChanged" AutoPostBack="true"><asp:ListItem>G�n</asp:ListItem><asp:ListItem>1</asp:ListItem><asp:ListItem>2</asp:ListItem><asp:ListItem>3</asp:ListItem><asp:ListItem>4</asp:ListItem><asp:ListItem>5</asp:ListItem><asp:ListItem>6</asp:ListItem><asp:ListItem>7</asp:ListItem><asp:ListItem>8</asp:ListItem><asp:ListItem>9</asp:ListItem><asp:ListItem>10</asp:ListItem><asp:ListItem>11</asp:ListItem><asp:ListItem>12</asp:ListItem><asp:ListItem>13</asp:ListItem><asp:ListItem>14</asp:ListItem><asp:ListItem>15</asp:ListItem><asp:ListItem>16</asp:ListItem><asp:ListItem>17</asp:ListItem><asp:ListItem>18</asp:ListItem><asp:ListItem>19</asp:ListItem><asp:ListItem>20</asp:ListItem><asp:ListItem>21</asp:ListItem><asp:ListItem>22</asp:ListItem><asp:ListItem>23</asp:ListItem><asp:ListItem>24</asp:ListItem><asp:ListItem>25</asp:ListItem><asp:ListItem>26</asp:ListItem><asp:ListItem>27</asp:ListItem><asp:ListItem>28</asp:ListItem><asp:ListItem>29</asp:ListItem><asp:ListItem>30</asp:ListItem><asp:ListItem>31</asp:ListItem></asp:DropDownList></asp:TableCell>
                        <asp:TableCell> <asp:DropDownList ID="ddlAyCocuk1" runat="server" OnSelectedIndexChanged="ddlOdaDogumTarihi_SelectedIndexChanged" AutoPostBack="true"><asp:ListItem>Ay</asp:ListItem><asp:ListItem Value="1">Ocak</asp:ListItem><asp:ListItem Value="2">�ubat</asp:ListItem><asp:ListItem Value="3">Mart</asp:ListItem><asp:ListItem Value="4">Nisan</asp:ListItem><asp:ListItem Value="5">May�s</asp:ListItem><asp:ListItem Value="6">Haziran</asp:ListItem><asp:ListItem Value="7">Temmuz</asp:ListItem><asp:ListItem Value="8">A�ustos</asp:ListItem><asp:ListItem Value="9">Eyl�l</asp:ListItem><asp:ListItem Value="10">Ekim</asp:ListItem><asp:ListItem Value="11">Kas�m</asp:ListItem><asp:ListItem Value="12">Aral�k</asp:ListItem></asp:DropDownList></asp:TableCell>
                        <asp:TableCell> <asp:DropDownList ID="ddlYilCocuk1" runat="server" OnSelectedIndexChanged="ddlOdaDogumTarihi_SelectedIndexChanged" AutoPostBack="true"><asp:ListItem>Y�l</asp:ListItem><asp:ListItem>2009</asp:ListItem><asp:ListItem>2008</asp:ListItem><asp:ListItem>2007</asp:ListItem><asp:ListItem>2006</asp:ListItem><asp:ListItem>2005</asp:ListItem><asp:ListItem>2004</asp:ListItem><asp:ListItem>2003</asp:ListItem><asp:ListItem>2002</asp:ListItem><asp:ListItem>2001</asp:ListItem><asp:ListItem>2000</asp:ListItem><asp:ListItem>1999</asp:ListItem><asp:ListItem>1998</asp:ListItem><asp:ListItem>1997</asp:ListItem><asp:ListItem></asp:ListItem></asp:DropDownList></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                        <asp:TableCell><b>2.�ocuk Do�um Tarihi</b> </asp:TableCell>
                        <asp:TableCell>:</asp:TableCell>
                        <asp:TableCell><asp:DropDownList ID="ddlGunCocuk2" runat="server" OnSelectedIndexChanged="ddlOdaDogumTarihi_SelectedIndexChanged" AutoPostBack="true"><asp:ListItem>G�n</asp:ListItem><asp:ListItem>1</asp:ListItem><asp:ListItem>2</asp:ListItem><asp:ListItem>3</asp:ListItem><asp:ListItem>4</asp:ListItem><asp:ListItem>5</asp:ListItem><asp:ListItem>6</asp:ListItem><asp:ListItem>7</asp:ListItem><asp:ListItem>8</asp:ListItem><asp:ListItem>9</asp:ListItem><asp:ListItem>10</asp:ListItem><asp:ListItem>11</asp:ListItem><asp:ListItem>12</asp:ListItem><asp:ListItem>13</asp:ListItem><asp:ListItem>14</asp:ListItem><asp:ListItem>15</asp:ListItem><asp:ListItem>16</asp:ListItem><asp:ListItem>17</asp:ListItem><asp:ListItem>18</asp:ListItem><asp:ListItem>19</asp:ListItem><asp:ListItem>20</asp:ListItem><asp:ListItem>21</asp:ListItem><asp:ListItem>22</asp:ListItem><asp:ListItem>23</asp:ListItem><asp:ListItem>24</asp:ListItem><asp:ListItem>25</asp:ListItem><asp:ListItem>26</asp:ListItem><asp:ListItem>27</asp:ListItem><asp:ListItem>28</asp:ListItem><asp:ListItem>29</asp:ListItem><asp:ListItem>30</asp:ListItem><asp:ListItem>31</asp:ListItem></asp:DropDownList></asp:TableCell>
                        <asp:TableCell> <asp:DropDownList ID="ddlAyCocuk2" runat="server" OnSelectedIndexChanged="ddlOdaDogumTarihi_SelectedIndexChanged" AutoPostBack="true"><asp:ListItem>Ay</asp:ListItem><asp:ListItem>Ocak</asp:ListItem><asp:ListItem>�ubat</asp:ListItem><asp:ListItem>Mart</asp:ListItem><asp:ListItem>Nisan</asp:ListItem><asp:ListItem>May�s</asp:ListItem><asp:ListItem>Haziran</asp:ListItem><asp:ListItem>Temmuz</asp:ListItem><asp:ListItem>A�ustos</asp:ListItem><asp:ListItem>Eyl�l</asp:ListItem><asp:ListItem>Ekim</asp:ListItem><asp:ListItem>Kas�m</asp:ListItem><asp:ListItem>Aral�k</asp:ListItem></asp:DropDownList></asp:TableCell>
                        <asp:TableCell> <asp:DropDownList ID="ddlYilCocuk2" runat="server" OnSelectedIndexChanged="ddlOdaDogumTarihi_SelectedIndexChanged" AutoPostBack="true"><asp:ListItem>Y�l</asp:ListItem><asp:ListItem>2009</asp:ListItem><asp:ListItem>2008</asp:ListItem><asp:ListItem>2007</asp:ListItem><asp:ListItem>2006</asp:ListItem><asp:ListItem></asp:ListItem></asp:DropDownList></asp:TableCell>
                        </asp:TableRow>
                        </asp:Table>
                    </td>
                    </tr>
                    <tr>
                        <td colspan="3" style="color: #F03307; font-size:10px;">
                            T�m tesislerimizde, �zellikle �ocuk ya�lar� fiyatland�rmas� i�in kimlik kontrol� yap�lmaktad�r.
                        </td>
                    </tr>
                    <tr>
                    <td colspan="3"></td>
                    </tr>
                    <tr>
                    <td colspan="3">
                    <hr   style="width: 490px; color: #FF6600;" />
                    <b>�deme �eklini belirleyiniz:</b>
                    </td>
                    </tr>
                    <tr>
                    <td colspan="3" style="color: #F03307; font-size:10px;">
                    Logolardan kart�n�z� se�in ya da havale ile �demek i�in "HAVALE" se�ene�ini i�aretleyin.
                    </td>
                    </tr>
                    <tr>
                    <td colspan="3">
                    <table>
                    <tr>
                    
                    <td><asp:ImageButton ID="ccWorld" ImageUrl="Images/CreditCards/world.gif" 
                            runat="server" BorderColor="Black" BorderStyle="Solid" BorderWidth="1" 
                            onclick="ccWorld_Click" /></td>
                    <td><asp:ImageButton ID="ccAdvantage" ImageUrl="Images/CreditCards/advantage.gif" 
                            runat="server" BorderColor="Black" BorderStyle="Solid" BorderWidth="1" 
                            onclick="ccWorld_Click" /></td>
                    <td><asp:ImageButton ID="ccAxess" ImageUrl="Images/CreditCards/axess.gif" 
                            runat="server" BorderColor="Black" BorderStyle="Solid" BorderWidth="1" 
                            onclick="ccWorld_Click" /></td>
                    <td><asp:ImageButton ID="ccBonus" ImageUrl="Images/CreditCards/bonus.gif" 
                            runat="server" BorderColor="Black" BorderStyle="Solid" BorderWidth="1" 
                            onclick="ccWorld_Click" /></td>
	                <td><asp:ImageButton ID="ccCardFinans" ImageUrl="Images/CreditCards/cardfinans.gif" 
                            runat="server" BorderColor="Black" BorderStyle="Solid" BorderWidth="1" 
                            onclick="ccWorld_Click" /></td>
	                <td><asp:ImageButton ID="ccCiti" ImageUrl="Images/CreditCards/citi.gif" 
                            runat="server" BorderColor="Black" BorderStyle="Solid" BorderWidth="1" 
                            onclick="ccWorld_Click" /></td>
                    </tr>
                    <tr>
                    <td><asp:ImageButton ID="ccMaximum" ImageUrl="~/Images/CreditCards/maximum.gif" 
                            runat="server" BorderColor="Black" BorderStyle="Solid" BorderWidth="1" 
                            onclick="ccWorld_Click" /></td>
                    <td><asp:ImageButton ID="ccShopAndMiles" ImageUrl="~/Images/CreditCards/shopandmiles.gif" 
                            runat="server" BorderColor="Black" BorderStyle="Solid" BorderWidth="1" 
                            onclick="ccWorld_Click" /></td>
	                <td><asp:ImageButton ID="ccOtherCards" ImageUrl="~/Images/CreditCards/othercards.gif" 
                            runat="server" BorderColor="Black" BorderStyle="Solid" BorderWidth="1" 
                            onclick="ccWorld_Click" /></td>
	                <td><asp:ImageButton ID="ccOrder" ImageUrl="Images/CreditCards/order.gif" 
                            runat="server" BorderColor="Black" BorderStyle="Solid" BorderWidth="1" 
                            onclick="ccWorld_Click" /></td>	
                    </tr>
                    <tr>
                    <td colspan="6">&nbsp;</td>
                    </tr>
                    <tr>
                    <td colspan="6">
                        <asp:Panel ID="pnOdeme" runat="server" Height="100%" Width="100%" Visible="false">
                        <hr   style="width: 490px; color: #FF6600;" />
                        <b><asp:Label ID="lblKart" runat="server" Text="Banka �deme Se�enekleri:"></asp:Label></b>
                        <table width="100%">
                        <tr><td valign="top"> 
                        <hr style="width: 100%; color: #FF0000;" />   
                                               
                            <asp:Table ID="tblOdemeSecenekleri" runat="server">
                            <asp:TableRow>
                            <asp:TableCell> <asp:RadioButton ID="rbtTekCekim" runat="server" Checked="true" GroupName="rbOdeme" 
                                oncheckedchanged="rbtTekCekim_CheckedChanged" AutoPostBack="true" />
                            </asp:TableCell> 
                            <asp:TableCell> <asp:Label ID="lblTekCekim" runat="server" Text="Tek �ekim"></asp:Label></asp:TableCell>
                            <asp:TableCell></asp:TableCell>
                            <asp:TableCell HorizontalAlign="Right"> <asp:Label ID="lblTekCekimUcret" runat="server" Text="100 TL" Font-Bold="True" BorderColor="Black"></asp:Label></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                            <asp:TableCell> <asp:RadioButton ID="rbtTaksit1" runat="server" GroupName="rbOdeme" oncheckedchanged="rbtTekCekim_CheckedChanged" AutoPostBack="true"/></asp:TableCell>
                            <asp:TableCell> <asp:Label ID="lblTaksit1" runat="server" Text="6 taksit (6 x 100,80 TL)"></asp:Label></asp:TableCell>
                            <asp:TableCell></asp:TableCell>
                            <asp:TableCell HorizontalAlign="Right"> <asp:Label ID="lblTaksit1Ucret" runat="server" Text="100 TL" Font-Bold="True" BorderColor="Black"></asp:Label></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                            <asp:TableCell> <asp:RadioButton ID="rbtTaksit2" runat="server" GroupName="rbOdeme" oncheckedchanged="rbtTekCekim_CheckedChanged" AutoPostBack="true"/></asp:TableCell>
                            <asp:TableCell> <asp:Label ID="lblTaksit2" runat="server" Text="12 taksit (12 x 100,80 TL)"></asp:Label></asp:TableCell>
                            <asp:TableCell></asp:TableCell>
                            <asp:TableCell HorizontalAlign="Right"> <asp:Label ID="lblTaksit2Ucret" runat="server" Text="100 TL" Font-Bold="True" BorderColor="Black"></asp:Label></asp:TableCell>
                            </asp:TableRow>
                       
                            </asp:Table>
                         </td>
                         <td style="width: 14px">
                         
                         </td>
                         <td style="width: 120px; padding:20px; background-image: url('Images/Tables/detail_bg.jpg'); background-repeat:no-repeat;">
                         <center><span style="color: #FF0000; font-size: 16px;"><b>TOPLAM F�YAT</span></b></center>
                         <hr style="color: #FF6600;" />
                         
                             <center><asp:Label ID="lblTutar" runat="server" Text="250" Font-Size="X-Large" 
            ForeColor="#CC0000"></asp:Label> <span style="font-size: 12px">
                                 &nbsp;TL</span></center>
                             <hr style="color: #FF6600;" />
                             <asp:Label ID="lblOdemeTuru" runat="server" Text="Tek �ekim" Font-Size="8pt"></asp:Label>
                         </td>
                         </tr>
                        </table>
                        </asp:Panel>
                    </td>
                    </tr>
                    </table>
                    </td>
                    </tr>
                    <tr>
                      <td align="center" colspan="3">
                        <asp:UpdateProgress ID="UpdateProgressBottom" runat="server" AssociatedUpdatePanelID="UpdatePanelBottom">
                        <ProgressTemplate>
                        <br />
                            &nbsp;
                        <img src="Images/Ajax/ajax-loader.gif" /> Y�kleniyor...</ProgressTemplate>
                        </asp:UpdateProgress>
	                </td>
                    </tr>
                </table>
                </ContentTemplate>
                </asp:UpdatePanel>
			    
            </fieldset>
		</td>
	</tr>
	<tr class="tdDetail" style="padding-top: 15px;">
		<td valign="top">
            <asp:Label ID="lblError" runat="server" Text="" Font-Bold="True" 
                Font-Size="Medium" ForeColor="Red"></asp:Label>
		</td>
		<td align="right" valign="top">
		<br />
		    <asp:ImageButton ID="imgBtnContinue" runat="server" 
                ImageUrl="~/Images/Buttons/continue_button.gif" 
                onclick="imgBtnContinue_Click" />
		</td>
    </tr>		
</table>
    
</asp:Content>