<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" Debug="true" %>
<%@ Register Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<asp:Content ID="cpDetail" ContentPlaceHolderID="cpDetail" Runat="Server">
<table border="0" cellpadding="0" cellspacing="0" width="510">
	<tr class="BaslikSatiri" style="background-image:url('Images/Tables/title_bg_red.jpg');">
		<td colspan="4">
            YURT ��� TAT�L FIRSATLARI
		</td>
	</tr>
	<% while (DR_HitHotel.Read()) {	%>
    <tr>
        <td colspan="2">
            <table border="0" cellpadding="0" cellspacing="0" class="TableData" style="width:255px; height:128px; background-image: url('Images/Tables/bg_box.gif');">
                <tr>
                    <td rowspan="5" style="width: 105px;" valign="middle">
                        <img src="Images/Otels/Thumbs/<% =DR_HitHotel.GetString(4) %>" alt="" class="imgThumb" />
                    </td>
                    <td style="height: 20px; vertical-align: bottom;">
                        <b style="color: #FF6600;"><% =DR_HitHotel.GetString(0) %></b>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b><% =DR_HitHotel.GetString(2) %> / <% =DR_HitHotel.GetString(3) %></b>
                    </td>                
                </tr>
                <tr>
                    <td>
                        <% =DR_HitHotel.GetString(7) %>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span style="color: #F03307;"><% =CengTur.OtelEnDusukFiyat(DR_HitHotel.GetGuid(8).ToString()) %> YTL.</span>'den itibaren
                    </td>
                </tr>
                <tr>
                    <td>
                        (<% =DR_HitHotel.GetDateTime(5).ToShortDateString() %>-<% =DR_HitHotel.GetDateTime(6).ToShortDateString()%> aras�nda)
                        <a href="Detail.aspx?OpenId=<% =DR_HitHotel.GetGuid(8) %>">Detaylar >></a>
                    </td>
                </tr>
            </table>
		</td>
		<td colspan="2">
        <% if(DR_HitHotel.Read()) { %>
            <table border="0" cellpadding="0" cellspacing="0" class="TableData" style="width:255px; height:128px; background-image: url('Images/Tables/bg_box.gif');">
                <tr>
                    <td rowspan="5" style="width: 105px;" valign="middle">
                        <img src="Images/Otels/Thumbs/<% =DR_HitHotel.GetString(4) %>" alt="" class="imgThumb" />
                    </td>
                    <td style="height: 20px; vertical-align: bottom;">
                        <b style="color: #FF6600;"><% =DR_HitHotel.GetString(0) %></b>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b><% =DR_HitHotel.GetString(2) %> / <% =DR_HitHotel.GetString(3) %></b>
                    </td>                
                </tr>
                <tr>
                    <td>
                        <% =DR_HitHotel.GetString(7) %>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span style="color: #F03307;"><% =CengTur.OtelEnDusukFiyat(DR_HitHotel.GetGuid(8).ToString()) %> YTL.</span>'den itibaren
                    </td>
                </tr>
                <tr>
                    <td>
                        (<% =DR_HitHotel.GetDateTime(5).ToShortDateString() %>-<% =DR_HitHotel.GetDateTime(6).ToShortDateString()%> aras�nda)
                        <a href="Detail.aspx?OpenId=<% =DR_HitHotel.GetGuid(8) %>">Detaylar >></a>
                    </td>
                </tr>
            </table>
        <% } %>    
		</td>
	<% } %>
	</tr>
	<tr style="height: 20px;">
		<td colspan="4"></td>
	</tr>

	<tr class="BaslikSatiri" style="background-image:url('Images/Tables/title_bg_yesil.jpg');">
		<td colspan="4">
            YURT DI�I TAT�L FIRSATLARI
		</td>
	</tr>
	<% while (DR_HitHotelYD.Read())
    {	%>
    <tr>
        <td colspan="2">
            <table border="0" cellpadding="0" cellspacing="0" class="TableData" style="width:255px; height:128px; background-image: url('Images/Tables/bg_box.gif');">
                <tr>
                    <td rowspan="5" style="width: 105px;" valign="middle">
                        <img src="Images/Otels/Thumbs/<% =DR_HitHotelYD.GetString(4) %>" alt="" class="imgThumb" />
                    </td>
                    <td style="height: 20px; vertical-align: bottom;">
                        <b style="color: #FF6600;"><% =DR_HitHotelYD.GetString(0)%></b>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b><% =DR_HitHotelYD.GetString(2)%> / <% =DR_HitHotelYD.GetString(3)%></b>
                    </td>                
                </tr>
                <tr>
                    <td>
                        <% =DR_HitHotelYD.GetString(7)%>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span style="color: #F03307;"><% =CengTur.OtelEnDusukFiyat(DR_HitHotelYD.GetGuid(8).ToString())%> YTL.</span>'den itibaren
                    </td>
                </tr>
                <tr>
                    <td>
                        (<% =DR_HitHotelYD.GetDateTime(5).ToShortDateString()%>-<% =DR_HitHotelYD.GetDateTime(6).ToShortDateString()%> aras�nda)
                        <a href="Detail.aspx?OpenId=<% =DR_HitHotelYD.GetGuid(8) %>">Detaylar >></a>
                    </td>
                </tr>
            </table>
		</td>
		<td colspan="2">
        <% if (DR_HitHotelYD.Read())
           { %>
            <table border="0" cellpadding="0" cellspacing="0" class="TableData" style="width:255px; height:128px; background-image: url('Images/Tables/bg_box.gif');">
                <tr>
                    <td rowspan="5" style="width: 105px;" valign="middle">
                        <img src="Images/Otels/Thumbs/<% =DR_HitHotelYD.GetString(4) %>" alt="" class="imgThumb" />
                    </td>
                    <td style="height: 20px; vertical-align: bottom;">
                        <b style="color: #FF6600;"><% =DR_HitHotelYD.GetString(0)%></b>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b><% =DR_HitHotelYD.GetString(2)%> / <% =DR_HitHotelYD.GetString(3)%></b>
                    </td>                
                </tr>
                <tr>
                    <td>
                        <% =DR_HitHotelYD.GetString(7)%>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span style="color: #F03307;"><% =CengTur.OtelEnDusukFiyat(DR_HitHotelYD.GetGuid(8).ToString())%> YTL.</span>'den itibaren
                    </td>
                </tr>
                <tr>
                    <td>
                        (<% =DR_HitHotelYD.GetDateTime(5).ToShortDateString()%>-<% =DR_HitHotelYD.GetDateTime(6).ToShortDateString()%> aras�nda)
                        <a href="Detail.aspx?OpenId=<% =DR_HitHotelYD.GetGuid(8) %>">Detaylar >></a>
                    </td>
                </tr>
            </table>
        <% } %>    
		</td>
	<% } %>
	</tr>

	<tr style="height: 20px;">
		<td colspan="4"></td>
	</tr>
</table>

</asp:Content>