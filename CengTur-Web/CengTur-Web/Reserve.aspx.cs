#region Using Tan�mlamalar�
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Web.Services;
using System.Drawing;
#endregion

public partial class Reserve : System.Web.UI.Page
{
    #region Global degisken tanimlari
    string OpenId;
    DateTime BasTarih, BitTarih;
    public static string selectedCard;
    SqlDataReader DR_HotelDetail, DR_OdaKonaklamaTipi;
    bool IsTaksitShown = false;
    public DataTable DT_YetiskinFiyat, DT_CocukFiyat;
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        /*No Caching---------------------------------------*/
        Response.ExpiresAbsolute = DateTime.Now.AddDays(-1d);
        Response.Expires = -1500;
        /*Response.CacheControl = "no-cache";
        /*-------------------------------------------------*/
        
        try
        {
            
            OpenId = Session["OpenId"].ToString();
            lblTurAdi.Text = Session["TurAdi"].ToString();
            lblSehir.Text = Session["Sehir"].ToString();
            lblBolge.Text = Session["Bolge"].ToString();
            lblAdi.Text = Session["Adi"].ToString();

            #region DR_HotelDetail
            DR_HotelDetail = CengTur.HotelDetail(OpenId);
            DR_HotelDetail.Read();
            BasTarih = DR_HotelDetail.GetDateTime(4);
            BitTarih = DR_HotelDetail.GetDateTime(5);
            #endregion

            if (!IsPostBack)
            {
                getOdaKonaklamaTipi();
                #region Y�l Aral��� Belirleme
                ddlYilCocuk1.Items.Clear();
                ddlYilCocuk2.Items.Clear();
                int LastYear = DateTime.Now.Year;
                int FirstYear = LastYear - 13;
                ddlYilCocuk1.Items.Add("Y�l");
                ddlYilCocuk2.Items.Add("Y�l");
                for (int i = LastYear; i >= FirstYear; i--)
                {
                    ddlYilCocuk1.Items.Add(i.ToString());
                    ddlYilCocuk2.Items.Add(i.ToString());

                }
                #endregion
                Session["ccSecilen"] = null;
                
            }


        }
        catch (Exception)
        {
            Response.Redirect("");
        }
        finally
        {
            DR_HotelDetail.Close();
        }
        
    }

    #region Tarih Aral��� Belirleme ve Calendar ��lemleri
    protected void Calendar1_DayRender(object sender, DayRenderEventArgs e)
    {
        e.Day.IsSelectable = false;

        Style gecerliAralikStyle = new Style();
        gecerliAralikStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFFEC");
        gecerliAralikStyle.BorderColor = System.Drawing.ColorTranslator.FromHtml("#6098CF");
        gecerliAralikStyle.BorderWidth = 1;
        gecerliAralikStyle.Font.Bold = true;

        Style secilenAralikStyle = new Style();
        secilenAralikStyle.ForeColor = System.Drawing.ColorTranslator.FromHtml("#FFFFFF");
        secilenAralikStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#6098CF");
        secilenAralikStyle.BorderColor = System.Drawing.ColorTranslator.FromHtml("#6098CF");
        secilenAralikStyle.BorderWidth = 1;

        Style weekendStyle = new Style();
        weekendStyle.BackColor = System.Drawing.Color.White;

        Style todayStyle = new Style();
        todayStyle.ForeColor = System.Drawing.ColorTranslator.FromHtml("#F03307");
        todayStyle.Font.Bold = true;


        if ((e.Day.Date >= BasTarih) && (e.Day.Date <= BitTarih))
        {
            e.Cell.ApplyStyle(gecerliAralikStyle);
            e.Day.IsSelectable = (e.Day.Date >= DateTime.Today);

            //Label aLabel = new Label();
            //aLabel.Text = " <br>" + "se�";
            //e.Cell.Controls.Add(aLabel);

            //e.Cell.Text = "<a href=" + e.SelectUrl + ">Merhaba</a>";
        }
        else if (e.Day.IsWeekend)
            e.Cell.ApplyStyle(weekendStyle);

        if (e.Day.IsSelected)
            e.Cell.ApplyStyle(secilenAralikStyle);
        else if (e.Day.IsToday)
            e.Cell.ApplyStyle(todayStyle);

        if (lblBitTarih.Text != string.Empty)
            if ((e.Day.Date >= Convert.ToDateTime(lblBasTarih.Text)) && (e.Day.Date <= Convert.ToDateTime(lblBitTarih.Text)))
                e.Cell.ApplyStyle(secilenAralikStyle);
    }

    protected void Calendar1_SelectionChanged(object sender, EventArgs e)
    {
        lblBasTarih.Text = Calendar1.SelectedDate.ToLongDateString();
        lblBitTarih.Text = string.Empty;
        GeceSayisiBelirle(Calendar1.SelectedDate, BitTarih);
        ddlGeceSayisi.Enabled = true;
        OdemeTablosu();
       
    }

    void GeceSayisiBelirle(DateTime Giris, DateTime maxCikis)
    {
        ddlGeceSayisi.Items.Clear();
        ddlGeceSayisi.Items.Add(new ListItem("Se�iniz", "0"));
        TimeSpan GeceSayisi = maxCikis - Giris;
        for (int i = 1; i <= GeceSayisi.Days; i++)
            ddlGeceSayisi.Items.Add(new ListItem(i.ToString(), i.ToString()));
    }

    protected void ddlGeceSayisi_SelectedIndexChanged(object sender, EventArgs e)
    {
        lblBitTarih.Text = Convert.ToDateTime(lblBasTarih.Text).AddDays(Convert.ToDouble(ddlGeceSayisi.SelectedValue)).ToLongDateString();
        OdemeTablosu();
    }
    #endregion

    private void getOdaKonaklamaTipi()
    {
        DR_OdaKonaklamaTipi = CengTur.OdaKonaklamaTipi(OpenId);
        while (DR_OdaKonaklamaTipi.Read())
            ddlOdaKonaklamaTipi.Items.Add(new ListItem(DR_OdaKonaklamaTipi.GetString(0)));
    }

    protected void ddlYetiskinSayisi_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToInt32(ddlYetiskinSayisi.SelectedValue) > 1)
            ddlCocukSayisi.Enabled = true;
        else
        {
            ddlCocukSayisi.SelectedIndex = 0;
            ddlCocukSayisi.Enabled = false;
        }
        OdemeTablosu();
    }

    

    #region �nbellekleme islemleri
    private void createSession()
    {
        string[] DelimiterChars = { " - " };
        Session["OdaTipAdi"] = ddlOdaKonaklamaTipi.SelectedValue.Split(DelimiterChars, StringSplitOptions.None)[0];
        Session["HizmetAdi"] = ddlOdaKonaklamaTipi.SelectedValue.Split(DelimiterChars, StringSplitOptions.None)[1];
        Session["oodId"] = null;
        Session["yetiskinSay"] = ddlYetiskinSayisi.SelectedValue;
        if (ddlCocukSayisi.SelectedIndex != 0)
            Session["cocukSay"] = ddlCocukSayisi.SelectedValue;
        else
            Session["cocukSay"] = 0;
        Session["GirisTarihi"] = lblBasTarih.Text;
        Session["CikisTarihi"] = lblBitTarih.Text;

        if (ddlCocukSayisi.SelectedIndex != 0)
        {
            if (Convert.ToInt32(ddlCocukSayisi.SelectedValue) == 1)
            {
                Session["DTCocuk1"] = ddlGunCocuk1.SelectedValue + "." + ddlAyCocuk1.SelectedValue + "." + ddlYilCocuk1.SelectedValue;
            }
            else if (Convert.ToInt32(ddlCocukSayisi.SelectedValue) == 2)
            {
                Session["DTCocuk1"] = ddlGunCocuk1.SelectedValue + "." + ddlAyCocuk1.SelectedValue + "." + ddlYilCocuk1.SelectedValue;
                Session["DTCocuk2"] = ddlGunCocuk2.SelectedValue + "." + ddlAyCocuk2.SelectedValue + "." + ddlYilCocuk2.SelectedValue;
            }
        }
    }
    #endregion

    #region Cocuk Yas Hesab� ve Ucret Hesab�
    protected void ddlCocukSayisi_SelectedIndexChanged1(object sender, EventArgs e)
    {
        if (ddlCocukSayisi.SelectedIndex != 0)
        {
            if (Convert.ToInt32(ddlCocukSayisi.SelectedValue) == 1)
            {
                tblCocuk.Visible = true;
                tblCocuk.Rows[0].Visible = true;
                tblCocuk.Rows[1].Visible = false;
            }
            else if (Convert.ToInt32(ddlCocukSayisi.SelectedValue) == 2)
            {
                tblCocuk.Visible = true;
                tblCocuk.Rows[0].Visible = true;
                tblCocuk.Rows[1].Visible = true;
            }
            else
            {
                tblCocuk.Visible = false;
            }
        }
        else
        {
            tblCocuk.Visible = false;
        }
        OdemeTablosu();

    }
    #endregion

    #region Kart Se�imim ��lemleri
    protected void ccWorld_Click(object sender, ImageClickEventArgs e)
    {
        string CCSecilen = "";
        ccWorld.BorderColor = Color.Black;
        ccWorld.BorderStyle = BorderStyle.Solid;
        ccWorld.BorderWidth = 1;

        ccAdvantage.BorderColor = Color.Black;
        ccAdvantage.BorderStyle = BorderStyle.Solid;
        ccAdvantage.BorderWidth = 1;

        ccAxess.BorderColor = Color.Black;
        ccAxess.BorderStyle = BorderStyle.Solid;
        ccAxess.BorderWidth = 1;

        ccBonus.BorderColor = Color.Black;
        ccBonus.BorderStyle = BorderStyle.Solid;
        ccBonus.BorderWidth = 1;

        ccCardFinans.BorderColor = Color.Black;
        ccCardFinans.BorderStyle = BorderStyle.Solid;
        ccCardFinans.BorderWidth = 1;
        
        ccCiti.BorderColor = Color.Black;
        ccCiti.BorderStyle = BorderStyle.Solid;
        ccCiti.BorderWidth = 1;

        ccMaximum.BorderColor = Color.Black;
        ccMaximum.BorderStyle = BorderStyle.Solid;
        ccMaximum.BorderWidth = 1;

        ccShopAndMiles.BorderColor = Color.Black;
        ccShopAndMiles.BorderStyle = BorderStyle.Solid;
        ccShopAndMiles.BorderWidth = 1;

        ccOtherCards.BorderColor = Color.Black;
        ccOtherCards.BorderStyle = BorderStyle.Solid;
        ccOtherCards.BorderWidth = 1;

        ccOrder.BorderColor = Color.Black;
        ccOrder.BorderStyle = BorderStyle.Solid;
        ccOrder.BorderWidth = 1;

        if (sender == ccWorld)
        {
            ((ImageButton)sender).BorderColor = Color.Red;
            ((ImageButton)sender).BorderStyle = BorderStyle.Solid;
            ((ImageButton)sender).BorderWidth = 2;
            CCSecilen = "ccWorld";
        }
        else if (sender == ccAdvantage)
        {
            ((ImageButton)sender).BorderColor = Color.Red;
            ((ImageButton)sender).BorderStyle = BorderStyle.Solid;
            ((ImageButton)sender).BorderWidth = 2;
            CCSecilen = "ccAdvantage";
        }
        else if (sender == ccAxess)
        {
            ((ImageButton)sender).BorderColor = Color.Red;
            ((ImageButton)sender).BorderStyle = BorderStyle.Solid;
            ((ImageButton)sender).BorderWidth = 2;
            CCSecilen = "ccAxess";
        }
        else if (sender == ccBonus)
        {
            ((ImageButton)sender).BorderColor = Color.Red;
            ((ImageButton)sender).BorderStyle = BorderStyle.Solid;
            ((ImageButton)sender).BorderWidth = 2;
            CCSecilen = "ccBonus";
        }
        else if (sender == ccCardFinans)
        {
            ((ImageButton)sender).BorderColor = Color.Red;
            ((ImageButton)sender).BorderStyle = BorderStyle.Solid;
            ((ImageButton)sender).BorderWidth = 2;
            CCSecilen = "ccCardFinans";
        }
        else if (sender == ccCiti)
        {
            ((ImageButton)sender).BorderColor = Color.Red;
            ((ImageButton)sender).BorderStyle = BorderStyle.Solid;
            ((ImageButton)sender).BorderWidth = 2;
            CCSecilen = "CCSecilen";
        }
        else if (sender == ccMaximum)
        {
            ((ImageButton)sender).BorderColor = Color.Red;
            ((ImageButton)sender).BorderStyle = BorderStyle.Solid;
            ((ImageButton)sender).BorderWidth = 2;
            CCSecilen = "ccMaximum";
        }
        else if (sender == ccShopAndMiles)
        {
            ((ImageButton)sender).BorderColor = Color.Red;
            ((ImageButton)sender).BorderStyle = BorderStyle.Solid;
            ((ImageButton)sender).BorderWidth = 2;
            CCSecilen = "ccShopAndMiles";
        }
        else if (sender == ccOtherCards)
        {
            ((ImageButton)sender).BorderColor = Color.Red;
            ((ImageButton)sender).BorderStyle = BorderStyle.Solid;
            ((ImageButton)sender).BorderWidth = 2;
            CCSecilen = "ccOtherCards";
        }
        else if (sender == ccOrder)
        {
            ((ImageButton)sender).BorderColor = Color.Red;
            ((ImageButton)sender).BorderStyle = BorderStyle.Solid;
            ((ImageButton)sender).BorderWidth = 2;
            CCSecilen = "ccOrder";
        }
        Session["ccSecilenImage"] = ((ImageButton)sender).ImageUrl;
        Session["ccSecilen"] = CCSecilen;
        OdemeTablosu();
    }

    #endregion

    #region Tarih Giri�leri Do�rulama
    private void OdemeTablosu()
    {
        bool mIsValid = true;
        try
        {
            // Rezervasyon i�in giri� tarihinin ge�erli bir tarih olup olmad��� kontrol ediliyor...
            if (Convert.ToDateTime(lblBasTarih.Text) == null)
                throw new FormatException();
            else if (ddlGeceSayisi.SelectedIndex == 0)
                throw new ArgumentNullException();
            else
            {
                if (ddlCocukSayisi.SelectedIndex != 0)
                {
                    if (Convert.ToInt32(ddlYetiskinSayisi.SelectedValue) + Convert.ToInt32(ddlCocukSayisi.SelectedValue) > 4)
                        throw new OverflowException();
                }
                else if (Convert.ToInt32(ddlYetiskinSayisi.SelectedValue) > 4)
                    throw new OverflowException();
            }
            if (ddlCocukSayisi.SelectedIndex != 0)
            {
                if (Convert.ToInt32(Convert.ToInt32(ddlCocukSayisi.SelectedValue)) == 1)
                {
                    if (ddlGunCocuk1.SelectedItem.Value != "G�n" && ddlAyCocuk1.SelectedValue != "Ay" && ddlYilCocuk1.SelectedValue != "Y�l")
                    {
                        DateTime tarihFormati;
                        string Tarih = ddlGunCocuk1.SelectedValue + "." + ddlAyCocuk1.SelectedValue + "." + ddlYilCocuk1.SelectedValue;
                        if (!DateTime.TryParse(Tarih, out tarihFormati))
                        {
                            
                            mIsValid = false;
                        }

                    }
                    else
                    {
                        
                        mIsValid = false;
                    }
                }
                else
                {
                    if (ddlGunCocuk1.SelectedItem.Value != "G�n" && ddlGunCocuk2.SelectedItem.Value != "G�n"
                    && ddlAyCocuk1.SelectedValue != "Ay" && ddlAyCocuk2.SelectedValue != "Ay" && ddlYilCocuk1.SelectedValue != "Y�l" && ddlYilCocuk2.SelectedValue != "Y�l")
                    {
                        DateTime tarihFormati;
                        string Tarih1 = ddlGunCocuk1.SelectedValue + "." + ddlAyCocuk1.SelectedValue + "." + ddlYilCocuk1.SelectedValue;
                        string Tarih2 = ddlGunCocuk2.SelectedValue + "." + ddlAyCocuk2.SelectedValue + "." + ddlYilCocuk2.SelectedValue;
                        if (!DateTime.TryParse(Tarih1, out tarihFormati))
                        {
                            
                            mIsValid = false;
                        }
                        else if (!DateTime.TryParse(Tarih2, out tarihFormati))
                        {
                            
                            mIsValid = false;
                        }
                    }
                    else
                    {
                        
                        mIsValid = false;
                    }

                }


            }



            if (Session["ccSecilen"]==null)
            {
                mIsValid = false;
            }
        }
        catch (FormatException)
        {
            
            mIsValid = false;
        }
        catch (ArgumentNullException)
        {
            
            mIsValid = false;
        }
        catch (OverflowException)
        {
            
            mIsValid = false;
        }
        if (mIsValid)
        {
            

            lblTekCekimUcret.Text = GetTotalPrice().ToString()+" TL";
            lblTutar.Text = GetTotalPrice().ToString();
            lblOdemeTuru.Text = "Tek �ekim";
            TaksitSecenekleri();
            string Toplam = GetTotalPrice().ToString();
            Session["PriceText"] = Toplam + " TL";
            
            Session["Price"] = Toplam;
            Session["SecilenTaksit"] = 0;

            IsTaksitShown = true;
            pnOdeme.Visible = true;
           
        }
        else
        {
            IsTaksitShown = false;
            pnOdeme.Visible = false;
            
        }

    }
#endregion

    # region ToplamUcretHesab�
    private Double GetTotalPrice()
    {
        string[] DelimiterChars = { " - " };
        Session["OdaTipAdi"] = ddlOdaKonaklamaTipi.SelectedValue.Split(DelimiterChars, StringSplitOptions.None)[0];
        Session["HizmetAdi"] = ddlOdaKonaklamaTipi.SelectedValue.Split(DelimiterChars, StringSplitOptions.None)[1];
        Session["yetiskinSay"] = ddlYetiskinSayisi.SelectedValue;
        if (ddlCocukSayisi.SelectedIndex != 0)
            Session["cocukSay"] = ddlCocukSayisi.SelectedValue;
        else
            Session["cocukSay"] = 0;
    

        if (ddlCocukSayisi.SelectedIndex != 0)
        {
            if (Convert.ToInt32(ddlCocukSayisi.SelectedValue) == 1)
            {
                Session["DTCocuk1"] = ddlGunCocuk1.SelectedValue + "." + ddlAyCocuk1.SelectedValue + "." + ddlYilCocuk1.SelectedValue;
            }
            else if (Convert.ToInt32(ddlCocukSayisi.SelectedValue) == 2)
            {
                Session["DTCocuk1"] = ddlGunCocuk1.SelectedValue + "." + ddlAyCocuk1.SelectedValue + "." + ddlYilCocuk1.SelectedValue;
                Session["DTCocuk2"] = ddlGunCocuk2.SelectedValue + "." + ddlAyCocuk2.SelectedValue + "." + ddlYilCocuk2.SelectedValue;
            }
        }

        string Sql = "SELECT oodId, fiyat1P, fiyat2P, ilaveYatak FROM OpenOtelDetails, OdaTipi "
                 + "WHERE (OpenOtelDetails.tipKodu = OdaTipi.tipKodu) "
                 + "AND (OpenOtelDetails.OpenId = '" + OpenId + "') "
                 + "AND (OpenOtelDetails.tipKodu = (SELECT tipKodu FROM OdaTipi WHERE tipAdi = '" + Session["OdaTipAdi"].ToString() + "'))";
        DT_YetiskinFiyat = CengTur.GetDataTable(Sql);

        Session["oodId"] = DT_YetiskinFiyat.Rows[0]["oodId"].ToString();
        Session["fiyat1P"] = DT_YetiskinFiyat.Rows[0]["fiyat1P"].ToString();
        Session["fiyat2P"] = DT_YetiskinFiyat.Rows[0]["fiyat2P"].ToString();
        Session["ilaveYatak"] = DT_YetiskinFiyat.Rows[0]["ilaveYatak"].ToString();

        /* �ocuk Fiyat Listesi******/
        Sql = "SELECT free1age, free2age, indirim2age FROM CocukIndirim, OpenOtelDetails "
            + "WHERE (CocukIndirim.OpenId = OpenOtelDetails.OpenId) "
            + "AND (CocukIndirim.OpenId = '" + OpenId + "')";
        DT_CocukFiyat = CengTur.GetDataTable(Sql);
        Session["free1age"] = DT_CocukFiyat.Rows[0]["free1age"].ToString();
        Session["free2age"] = DT_CocukFiyat.Rows[0]["free2age"].ToString();
        Session["indirim2age"] = DT_CocukFiyat.Rows[0]["indirim2age"].ToString();
        int ReserveGunSay = Convert.ToInt32(ddlGeceSayisi.SelectedValue);
        Double ToplamFiyat = 0;
        if (Convert.ToInt32(Session["yetiskinSay"]) == 1)
        {
            ToplamFiyat += Convert.ToDouble(Session["fiyat1P"]) * ReserveGunSay;
        }
        else if (Convert.ToInt32(Session["yetiskinSay"]) > 1)
        {
            if (Convert.ToInt32(Session["yetiskinSay"]) < 3)
            {
                ToplamFiyat += (2 * Convert.ToDouble(Session["fiyat2P"])) * ReserveGunSay;
            }
            else if ((Convert.ToInt32(Session["yetiskinSay"]) >= 3))
            {
                ToplamFiyat += (2 * Convert.ToDouble(Session["fiyat2P"])) * ReserveGunSay;
                ToplamFiyat += ((Convert.ToInt32(Session["yetiskinSay"])) - 2) * Convert.ToDouble(Session["ilaveYatak"]) * ReserveGunSay;
            }
        }
        if (Convert.ToInt32(Session["cocukSay"]) > 0)
        {
            if (Convert.ToInt32(Session["cocukSay"]) == 1)
            {
                int Yas = CengTur.AgeSpan(Convert.ToDateTime(Session["DTCocuk1"].ToString()), DateTime.Now);
                if (Yas >= 0 && Yas <= Convert.ToInt32(Session["free1age"]))
                {
                    ToplamFiyat += 0;
                }

            }
            else if (Convert.ToInt32(Session["cocukSay"]) == 2)
            {
                if (CengTur.AgeSpan(Convert.ToDateTime(Session["DTCocuk1"].ToString()), DateTime.Now) < CengTur.AgeSpan(Convert.ToDateTime(Session["DTCocuk2"].ToString()), DateTime.Now))
                {
                    string DTCocuk1 = Session["DTCocuk1"].ToString();
                    string DTCocuk2 = Session["DTCocuk2"].ToString();
                    Session["DTCocuk1"] = DTCocuk2;
                    Session["DTCocuk2"] = DTCocuk1;
                }
                int Yas = CengTur.AgeSpan(Convert.ToDateTime(Session["DTCocuk1"].ToString()), DateTime.Now);
                ToplamFiyat += 0;
                int Yas2 = CengTur.AgeSpan(Convert.ToDateTime(Session["DTCocuk2"].ToString()), DateTime.Now);
                if (Yas2 >= 0 && Yas2 <= Convert.ToInt32(Session["free2age"]))
                {
                    ToplamFiyat += 0;
                }
                else if (Yas2 >= (Convert.ToInt32(Session["free2age"]) + 1) && Yas2 <= Convert.ToInt32(Session["indirim2age"]))
                {
                    ToplamFiyat += System.Math.Round(Convert.ToDouble(Session["fiyat2P"]) / 2, 1, MidpointRounding.AwayFromZero) * ReserveGunSay;
                }
            }
        }

        return ToplamFiyat;
    }
    #endregion

    void rbtOdeme_CheckedChanged(object sender, EventArgs e)
    {
        OdemeTablosu();
    }
    protected void ddlOdaKonaklamaTipi_SelectedIndexChanged(object sender, EventArgs e)
    {
        OdemeTablosu();
    }
    protected void ddlOdaDogumTarihi_SelectedIndexChanged(object sender, EventArgs e)
    {
        OdemeTablosu();
    }

    #region OdemeSecimi ve Taksitli Fiyat �slemleri
    protected void rbtTekCekim_CheckedChanged(object sender, EventArgs e)
    {
        if (sender==rbtTekCekim)
        {
        string Toplam=GetTotalPrice().ToString();
        lblTutar.Text = Toplam;
        lblOdemeTuru.Text = "Tek �ekim";
        Session["PriceText"] = Toplam;
        Session["Price"] = Toplam;
        Session["SecilenTaksit"] = 0;
        }
        else if (sender == rbtTaksit1)
        {
            lblTutar.Text = Session["Taksit1Price"].ToString();
            lblOdemeTuru.Text = Session["Taksit1Text"].ToString();
            Session["PriceText"] = Session["Taksit1Price"].ToString() + " TL" + " - " + Session["Taksit1Text"].ToString();
            Session["SecilenTaksit"] = 1;
        }
        else if (sender==rbtTaksit2)
        {
            lblTutar.Text = Session["Taksit2Price"].ToString();
            lblOdemeTuru.Text = Session["Taksit2Text"].ToString();
            Session["PriceText"] = Session["Taksit2Price"].ToString() + " TL" + " - " + Session["Taksit2Text"].ToString();
            Session["SecilenTaksit"] = 2;
        }
    }
    # endregion

    #region KullaniciUyarilari
    protected void imgBtnContinue_Click(object sender, ImageClickEventArgs e)
    {
        bool mIsValid = true;

        try
        {
            // Rezervasyon i�in giri� tarihinin ge�erli bir tarih olup olmad��� kontrol ediliyor...
            if (Convert.ToDateTime(lblBasTarih.Text) == null)
                throw new FormatException();
            else if (ddlGeceSayisi.SelectedIndex == 0)
                throw new ArgumentNullException();
            else
            {
                if (ddlCocukSayisi.SelectedIndex != 0)
                {
                    if (Convert.ToInt32(ddlYetiskinSayisi.SelectedValue) + Convert.ToInt32(ddlCocukSayisi.SelectedValue) > 4)
                        throw new OverflowException();
                }
                else if (Convert.ToInt32(ddlYetiskinSayisi.SelectedValue) > 4)
                    throw new OverflowException();
            }
            if (ddlCocukSayisi.SelectedIndex != 0)
            {
                if (Convert.ToInt32(Convert.ToInt32(ddlCocukSayisi.SelectedValue)) == 1)
                {
                    if (ddlGunCocuk1.SelectedItem.Value != "G�n" && ddlAyCocuk1.SelectedValue != "Ay" && ddlYilCocuk1.SelectedValue != "Y�l")
                    {
                        DateTime tarihFormati;
                        string Tarih = ddlGunCocuk1.SelectedValue + "." + ddlAyCocuk1.SelectedValue + "." + ddlYilCocuk1.SelectedValue;
                        if (!DateTime.TryParse(Tarih, out tarihFormati))
                        {
                            lblError.Text = "* Ge�ersiz �ocuk Do�um Tarihi Se�imi !";
                            mIsValid = false;
                        }

                    }
                    else
                    {
                        lblError.Text = "* �ocuk Do�um Tarihini se�melisiniz!";
                        mIsValid = false;
                    }


                }
                else
                {
                    if (ddlGunCocuk1.SelectedItem.Value != "G�n" && ddlGunCocuk2.SelectedItem.Value != "G�n"
                    && ddlAyCocuk1.SelectedValue != "Ay" && ddlAyCocuk2.SelectedValue != "Ay" && ddlYilCocuk1.SelectedValue != "Y�l" && ddlYilCocuk2.SelectedValue != "Y�l")
                    {
                        DateTime tarihFormati;
                        string Tarih1 = ddlGunCocuk1.SelectedValue + "." + ddlAyCocuk1.SelectedValue + "." + ddlYilCocuk1.SelectedValue;
                        string Tarih2 = ddlGunCocuk2.SelectedValue + "." + ddlAyCocuk2.SelectedValue + "." + ddlYilCocuk2.SelectedValue;
                        if (!DateTime.TryParse(Tarih1, out tarihFormati))
                        {
                            lblError.Text = "* Ge�ersiz �ocuk Do�um Tarihi Se�imi !";
                            mIsValid = false;
                        }
                        else if (!DateTime.TryParse(Tarih2, out tarihFormati))
                        {
                            lblError.Text = "* Ge�ersiz �ocuk Do�um Tarihi Se�imi !";
                            mIsValid = false;
                        }

                    }
                    else
                    {
                        lblError.Text = "* �ocuk Do�um Tarihlerini se�melisiniz!";
                        mIsValid = false;
                    }

                }

            }
            if (IsTaksitShown)
            {
                mIsValid = false;
            }
            if (Session["ccSecilen"] == null)
            {
                mIsValid = false;
                lblError.Text = "* �deme �eklini se�iniz!";
            }
            if (mIsValid)
            {
                lblError.Text = "";
                createSession();
                Response.Redirect("~/ReserveQuotaControl.aspx");
            }


        }
        catch (FormatException)
        {
            lblError.Text = "* Giri� Tarihini se�melisiniz!";
            mIsValid = false;
        }
        catch (ArgumentNullException)
        {
            lblError.Text = "* Gece Say�s�n� se�melisiniz!";
            mIsValid = false;
        }
        catch (OverflowException)
        {
            lblError.Text = "* Kontenjan yeterli de�il!<br />Yeti�kin + �ocuk say�s� en fazla 4 olmal�...";
            mIsValid = false;
        }
    }
    #endregion

    #region TaksitSecenekleriniG�ster
    DataTable DTTaksit;
       
    private void TaksitSecenekleri()
    {
        
        string Sql = "SELECT ccTaksit,ccTaksitSayisi,ccKomisyon FROM CreditCards,CreditCardInstalment "
                 + "WHERE (CreditCards.ccNo = CreditCardInstalment.ccNo)"
                 + "AND (CreditCards.ccName= '" + Session["ccSecilen"].ToString() + "') ";
        DTTaksit = CengTur.GetDataTable(Sql);

        rbtTekCekim.Checked = true;
        rbtTaksit1.Checked = false;
        rbtTaksit2.Checked = false;
        if (DTTaksit.Rows.Count==1)
        {

            tblOdemeSecenekleri.Rows[1].Visible = true;
            tblOdemeSecenekleri.Rows[2].Visible = false;
            Session["Taksit1"] = DTTaksit.Rows[0];
            double TaksitsizFiyat = GetTotalPrice();
            double TaksitliFiyat = TaksitsizFiyat + TaksitsizFiyat * Convert.ToDouble(DTTaksit.Rows[0][2].ToString());
            
            Session["Taksit1Text"] = DTTaksit.Rows[0][0].ToString()+  "(" + DTTaksit.Rows[0][1].ToString() + " X " + Math.Round(TaksitliFiyat / Convert.ToInt32(DTTaksit.Rows[0][1].ToString()),2) + " TL)";
            
            lblTaksit1.Text = DTTaksit.Rows[0][0].ToString() + " (" + DTTaksit.Rows[0][1].ToString() + " X " + Math.Round(TaksitliFiyat/Convert.ToInt32(DTTaksit.Rows[0][1].ToString()),2)+" TL)";
            lblTaksit1Ucret.Text = TaksitliFiyat.ToString();
            Session["Taksit1Price"] = TaksitliFiyat.ToString();

            
        }
        else if (DTTaksit.Rows.Count==2)
        {
            tblOdemeSecenekleri.Rows[1].Visible = true;
            tblOdemeSecenekleri.Rows[2].Visible = true;
            Session["Taksit1"] = DTTaksit.Rows[0];
            Session["Taksit2"] = DTTaksit.Rows[1];
            double TaksitsizFiyat = GetTotalPrice();
            double TaksitliFiyat1 = TaksitsizFiyat + TaksitsizFiyat * Convert.ToDouble(DTTaksit.Rows[0][2].ToString());
            double TaksitliFiyat2 = TaksitsizFiyat + TaksitsizFiyat * Convert.ToDouble(DTTaksit.Rows[1][2].ToString());

            Session["Taksit1Price"] = TaksitliFiyat1.ToString();
            Session["Taksit2Price"] = TaksitliFiyat2.ToString();

            Session["Taksit1Text"] = DTTaksit.Rows[0][0].ToString() + "(" + DTTaksit.Rows[0][1].ToString() + " X " + Math.Round(TaksitliFiyat1 / Convert.ToInt32(DTTaksit.Rows[0][1].ToString()),2) + " TL)";
            Session["Taksit2Text"] = DTTaksit.Rows[1][0].ToString() + "(" + DTTaksit.Rows[1][1].ToString() + " X " + Math.Round(TaksitliFiyat1 / Convert.ToInt32(DTTaksit.Rows[1][1].ToString()),2) + " TL)";

            lblTaksit1.Text = DTTaksit.Rows[0][0].ToString() + " (" + DTTaksit.Rows[0][1].ToString() + " X " + Math.Round(TaksitliFiyat1 / Convert.ToInt32(DTTaksit.Rows[0][1].ToString()),2) + " TL)";
            lblTaksit1Ucret.Text = TaksitliFiyat1.ToString()+" TL";

            lblTaksit2.Text = DTTaksit.Rows[1][0].ToString() + " (" + DTTaksit.Rows[1][1].ToString() + " X " + Math.Round(TaksitliFiyat1 / Convert.ToInt32(DTTaksit.Rows[1][1].ToString()),2) + " TL)";
            lblTaksit2Ucret.Text = TaksitliFiyat2.ToString()+ " TL";
        }
        else
        {
            tblOdemeSecenekleri.Rows[1].Visible = false;
            tblOdemeSecenekleri.Rows[2].Visible = false;
        }

    }
    #endregion
}