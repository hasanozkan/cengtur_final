﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

public partial class Default2 : System.Web.UI.Page
{
    public SqlDataReader DR_MusteriYorumları;
    protected void Page_Load(object sender, EventArgs e)
    {
        lblError.Visible = false;
        DR_MusteriYorumları = CengTur.GetCustemerComment();
    }
    protected void btnSend_Click(object sender, EventArgs e)
    {
        bool mIs_Valid=true;
        if(txtAd.Text=="")
        {
            mIs_Valid = false; 
        }
        else if(txtSoyad.Text=="")
        {
            mIs_Valid = false; 
        }
        else if (txtEposta.Text == "")
        {
            mIs_Valid = false;
        }
        else if (txtMesaj.Text == "")
        {
            mIs_Valid = false;
        }
        else if (txtTel.Text == "")
        {
            mIs_Valid = false;
        }
        if (mIs_Valid)
        {
            string InsertMessage = "INSERT INTO MusteriMesajlari(mAd,mSoyad,mEposta,mTelefon,mMesaj)"
            + "VALUES('" + txtAd.Text + "','" + txtSoyad.Text + "','" + txtEposta.Text + "','" + txtTel.Text + "','" + txtMesaj.Text + "') ";
            CengTur.ExecQuery(InsertMessage);
            lblError.Visible = false;
            lblBasarili.Visible = true;
            txtAd.Text = "";
            txtSoyad.Text = "";
            txtEposta.Text = "";
            txtTel.Text = "";
            txtMesaj.Text = "";
        }
        else
        {
            programmaticModalPopup.Show();
            lblError.Visible = true;
            lblBasarili.Visible = false;
        }
    }
    protected void btnMesajYaz_Click(object sender, EventArgs e)
    {
        programmaticModalPopup.Show();
        lblError.Visible = false;
        lblBasarili.Visible = false;
    }
    public void HideError()
    {
        lblError.Visible = false;
    }

    [System.Web.Services.WebMethodAttribute(), System.Web.Script.Services.ScriptMethodAttribute()]
    public static string GetDynamicContent(string contextKey)
    {
        return default(string);
    }
}

